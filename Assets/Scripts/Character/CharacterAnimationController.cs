using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationController : MonoBehaviour
{
	public event EventHandler onFootstep;

    [BoxGroup("References")] [SerializeField] private Animator _animator;
	[BoxGroup("References")] [SerializeField] private Cloth _dress;
	[BoxGroup("References")] [SerializeField] private MouseMovement _characterMovement;

	private void Awake()
	{
		_characterMovement.onWrap += (sender, data) => _dress.ClearTransformMotion();
	}
	
	private void Update()
    {
        _animator.SetFloat("forward", _characterMovement.CurrentSpeed);
    }

	public void Footstep()
	{
		onFootstep?.Invoke(this, EventArgs.Empty);
	}
}

using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class MouseMovement : MonoBehaviour
{
	public event EventHandler onWrap;

	public float MoveSpeed;
	public float StartAimDistance = 1.0f;

    [BoxGroup("References")] [SerializeField] private NavMeshAgent _agent;

	public float CurrentSpeed { get; private set; }

    private InteractiveElement _currentTargetElement;

	private void Awake()
	{
		_agent.speed = MoveSpeed;
	}

	public void SetTargetPoint(Vector3 point)
	{
		_agent.isStopped = false;
		_currentTargetElement = null;
		_agent.updateRotation = true;
		_agent.SetDestination(point);
	}

	public void SetTargetElement(InteractiveElement element)
	{
		_agent.isStopped = false;
		_currentTargetElement = element;
		_agent.updateRotation = true;
		_agent.SetDestination(element.GetUseSpot());
	}

	public void CancelTarget()
	{
		_agent.isStopped = true;
		_currentTargetElement = null;
	}

	/**
	 * Teleports the entity somewhere
	 */
	public void Warp(Vector3 position)
	{
		_agent.Warp(position);
		onWrap?.Invoke(this, EventArgs.Empty);
	}

	private void Update()
	{
		CurrentSpeed = _agent.velocity.sqrMagnitude;

		if (_currentTargetElement == null) return;
		if (_agent.pathPending) return;

		if (_agent.remainingDistance < StartAimDistance)
		{
			_agent.updateRotation = false;

			float remainingPercent = _agent.remainingDistance / StartAimDistance;
			Vector3 targetPosition = _currentTargetElement.transform.position;

			Quaternion moveRotation = Quaternion.LookRotation(_agent.velocity, Vector3.up);
			Quaternion targetRotation = Quaternion.LookRotation(targetPosition - transform.position, Vector3.up);

			transform.rotation = Quaternion.Lerp(targetRotation, moveRotation, remainingPercent);
		/*var targetPosition = _agent.pathEndPosition;
			var targetPoint = new Vector3(targetPosition.x, transform.position.y, targetPosition.z);
			var _direction = (targetPoint - transform.position).normalized;
			var _lookRotation = Quaternion.LookRotation(_direction);
		*/

		}

		if (_agent.remainingDistance <= _agent.stoppingDistance)
		{
			_currentTargetElement.Reached();
			_currentTargetElement = null;
		}
	}
}

using Sirenix.OdinInspector;
using System;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

public class CursorController : SerializedMonoBehaviour
{
    public static event EventHandler onTargetingBlocked;
    public static event EventHandler onTargetingAllowed;

    public event EventHandler<Vector3> onGroundClick;
    public event EventHandler<InteractiveElement> onInteractiveElementClick;
    public event EventHandler onToggleHerbariumAction;
    public event EventHandler onTargetCancel;

    [BoxGroup("Layers")] [SerializeField] private LayerMask _groundLayers;
    [BoxGroup("Layers")] [SerializeField] private LayerMask _interactiveLayers;

    [BoxGroup("References")] [SerializeField] private Camera _camera;

    public MultiLock TargetingLock { get; private set; }

    private Ray _mouseRay;
    private InteractiveElement _currentInteractiveTarget;

    private void Awake()
	{
        TargetingLock = new MultiLock();
        TargetingLock.onLocked += (sender, data) =>
        {
            onTargetCancel?.Invoke(this, EventArgs.Empty);
            onTargetingBlocked?.Invoke(this, EventArgs.Empty);
        };

        TargetingLock.onUnlocked += (sender, data) =>
        {
            onTargetingAllowed?.Invoke(this, EventArgs.Empty);
        };
	}

	private void Update()
	{
        if (!TargetingLock.IsFree()) return;

        _mouseRay = _camera.ScreenPointToRay(Mouse.current.position.ReadValue());

        UpdateInteractiveTarget();
	}

	private void UpdateInteractiveTarget()
	{
        InteractiveElement target = null;
        RaycastHit hit;

        if (Physics.Raycast(_mouseRay, out hit, 100, _interactiveLayers))
        {
            target = hit.collider.GetComponent<InteractiveElement>();
        }

        if (target != _currentInteractiveTarget)
        {
            _currentInteractiveTarget?.CursorExit();
            _currentInteractiveTarget = target;
            _currentInteractiveTarget?.CursorEnter();
        }
    }
    
    public void OnMove(CallbackContext ctx)
	{
        if (!TargetingLock.IsFree() || !ctx.performed) return;
        if (_currentInteractiveTarget != null) return;

        RaycastHit hit;
        if (Physics.Raycast(_mouseRay, out hit, 100, _groundLayers))
		{
            Vector3 target = hit.point;
            onGroundClick?.Invoke(this, target);
		}
	}

    public void OnInteract(CallbackContext ctx)
    {
        if (!TargetingLock.IsFree() || !ctx.performed) return;
        if (_currentInteractiveTarget == null) return;

        _currentInteractiveTarget.Click();
        onInteractiveElementClick?.Invoke(this, _currentInteractiveTarget);
    }

    public void OnToggleHerbarium(CallbackContext ctx)
	{
        if (!ctx.performed) return;

        onToggleHerbariumAction?.Invoke(this, EventArgs.Empty);
	}
}

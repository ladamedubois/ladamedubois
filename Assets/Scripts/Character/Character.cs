using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
	[BoxGroup("References")] [SerializeField] private Bed _bed;
	[BoxGroup("References")] public MouseMovement Movement;

	public void InitDay()
	{
		transform.rotation = _bed.GetWakeTransform().rotation;
		Movement.Warp(transform.position = _bed.GetWakeTransform().position);
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITutorialLock
{
	public void TutorialLock(TutorialManager tutorial);
	public void TutorialUnlock(TutorialManager tutorial);
}

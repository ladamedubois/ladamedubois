using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour, IDailyComponent
{
	[BoxGroup("References")] [SerializeField] private TutorialMessage _messageFrame;
	[SerializeField] private List<TutorialStep> _steps;

	public TutorialMessage MessageFrame { get => _messageFrame; }

	private Queue<TutorialStep> _stepQueue;
	private TutorialStep _currentStep;

	private IEnumerator _waitBeforeShowCoroutine;

	public void Reset()
	{
	}

	public void LoadDay(DaysManager.DayData dayData)
	{
		if (!dayData.DailyMission.EnableTutorial) return;

		_stepQueue = new Queue<TutorialStep>(_steps);
		NextStep();
	}

	public void NextStep()
	{
		if (_currentStep != null)
		{
			_currentStep.onFinished -= OnStepFinished;
		}

		if (_stepQueue.Count <= 0) return;

_currentStep = _stepQueue.Dequeue();
		_currentStep.StartTask(this);

		if (_currentStep != null)
		{
			_currentStep.onFinished += OnStepFinished;
		}
	}

	public void OnStepFinished(object sender, EventArgs data)
	{
		NextStep();
	}

	internal void WaitBeforeShow(TutorialStep tutorialStep, float delay)
	{
		_waitBeforeShowCoroutine = WaitBeforeShowCoroutine(tutorialStep, delay);
		StartCoroutine(_waitBeforeShowCoroutine);
	}

	internal void CancelWaitBeforeShow()
	{
		if (_waitBeforeShowCoroutine == null) return;
		StopCoroutine(_waitBeforeShowCoroutine);
		_waitBeforeShowCoroutine = null;
	}

	private IEnumerator WaitBeforeShowCoroutine(TutorialStep tutorialStep, float delay)
	{
		yield return new WaitForSeconds(delay);
		tutorialStep.OnTaskShow<object>(null, null);
	}
}

using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskOpenHerbarium : TutorialStep
{
	[BoxGroup("Completion References")] [SerializeField] private Herbarium _herbarium;
	[SerializeField] private float _taskDelay = 1f;

	protected override void BindTaskShowCondition()
	{
		WaitBeforeShow(_taskDelay);
	}

	protected override void UnbindTaskShowCondition()
	{
		CancelWaitBeforeShow();
	}

	protected override void BindTaskCompleteCondition()
	{
		_herbarium.onOpen += OnTaskCompleted;
	}

	protected override void UnbindTaskCompleteCondition()
	{
		_herbarium.onOpen -= OnTaskCompleted;
	}
}

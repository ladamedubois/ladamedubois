using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskGoBackHome : TutorialStep
{
	[BoxGroup("Completion References")] [SerializeField] private CraftingStation _crafting;

	protected override void BindTaskShowCondition()
	{
		onStarted += OnTaskShow;
	}

	protected override void UnbindTaskShowCondition()
	{
		onStarted -= OnTaskShow;
	}

	protected override void BindTaskCompleteCondition()
	{
		_crafting.onRemedyCrafted += OnTaskCompleted;
	}

	protected override void UnbindTaskCompleteCondition()
	{
		_crafting.onRemedyCrafted -= OnTaskCompleted;
	}
}

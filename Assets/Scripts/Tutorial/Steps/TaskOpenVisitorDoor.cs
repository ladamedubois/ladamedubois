using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskOpenVisitorDoor : TutorialStep
{
	[BoxGroup("Completion References")] [SerializeField] private VisitorDoor _visitorDoor;
	[SerializeField] private float _taskDelay = 3f;

	protected override void BindTaskShowCondition()
	{
		WaitBeforeShow(_taskDelay);
	}

	protected override void UnbindTaskShowCondition()
	{
		CancelWaitBeforeShow();
	}

	protected override void BindTaskCompleteCondition()
	{
		_visitorDoor.onOpen += OnTaskCompleted;
	}

	protected override void UnbindTaskCompleteCondition()
	{
		_visitorDoor.onOpen -= OnTaskCompleted;
	}
}

using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskGoToSleep : TutorialStep
{
	[BoxGroup("Completion References")] [SerializeField] private DialogManager _dialogManager;
	[BoxGroup("Completion References")] [SerializeField] private Bed _bed;

	protected override void BindTaskShowCondition()
	{
		_dialogManager.onDialogClose += OnTaskShow;
	}

	protected override void UnbindTaskShowCondition()
	{
		_dialogManager.onDialogClose -= OnTaskShow;
	}

	protected override void BindTaskCompleteCondition()
	{
		_bed.onSleep += OnTaskCompleted;
	}

	protected override void UnbindTaskCompleteCondition()
	{
		_bed.onSleep -= OnTaskCompleted;
	}
}

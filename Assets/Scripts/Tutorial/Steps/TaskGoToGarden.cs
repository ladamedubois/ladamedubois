using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskGoToGarden : TutorialStep
{
	[BoxGroup("Completion References")] [SerializeField] private DialogManager _dialogManager;
	[BoxGroup("Completion References")] [SerializeField] private GardenDoor _gardenDoor;

	protected override void BindTaskShowCondition()
	{
		_dialogManager.onDialogClose += OnTaskShow;
	}

	protected override void UnbindTaskShowCondition()
	{
		_dialogManager.onDialogClose -= OnTaskShow;
	}

	protected override void BindTaskCompleteCondition()
	{
		_gardenDoor.onStartTravelDoor += OnTaskCompleted;
	}

	protected override void UnbindTaskCompleteCondition()
	{
		_gardenDoor.onStartTravelDoor -= OnTaskCompleted;
	}
}

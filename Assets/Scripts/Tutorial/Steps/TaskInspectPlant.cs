using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskInspectPlant : TutorialStep
{
	[BoxGroup("Completion References")] [SerializeField] private Herbarium _herbarium;

	protected override void BindTaskShowCondition()
	{
		onStarted += OnTaskShow;
	}

	protected override void UnbindTaskShowCondition()
	{
		onStarted -= OnTaskShow;
	}

	protected override void BindTaskCompleteCondition()
	{
		_herbarium.onClose += OnTaskCompleted;
		_herbarium.onPlantSelected += OnTaskCompleted;
	}

	protected override void UnbindTaskCompleteCondition()
	{
		_herbarium.onClose -= OnTaskCompleted;
		_herbarium.onPlantSelected -= OnTaskCompleted;
	}
}

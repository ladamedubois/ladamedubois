using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskGiveRemedy : TutorialStep
{
	[BoxGroup("Completion References")] [SerializeField] private CraftingStation _crafting;
	[BoxGroup("Completion References")] [SerializeField] private DialogManager _dialog;

	protected override void BindTaskShowCondition()
	{
		onStarted += OnTaskShow;
	}

	protected override void UnbindTaskShowCondition()
	{
		onStarted -= OnTaskShow;
	}

	protected override void BindTaskCompleteCondition()
	{
		_dialog.onRemedyGiven += OnTaskCompleted;
	}

	protected override void UnbindTaskCompleteCondition()
	{
		_dialog.onRemedyGiven -= OnTaskCompleted;
	}
}

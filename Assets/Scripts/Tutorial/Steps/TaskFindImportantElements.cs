using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskFindImportantElements : TutorialStep
{
	[BoxGroup("Completion References")] [SerializeField] private DialogManager _dialogManager;
	[BoxGroup("Completion References")] [SerializeField] private KeywordManager _keywordManager;

	protected override void BindTaskShowCondition()
	{
		_dialogManager.onEnterSymptomPhase += OnTaskShow;
	}

	protected override void UnbindTaskShowCondition()
	{
		_dialogManager.onEnterSymptomPhase -= OnTaskShow;
	}

	protected override void BindTaskCompleteCondition()
	{
		_keywordManager.onAllHintsFound += OnTaskCompleted;
	}

	protected override void UnbindTaskCompleteCondition()
	{
		_keywordManager.onAllHintsFound -= OnTaskCompleted;
	}
}

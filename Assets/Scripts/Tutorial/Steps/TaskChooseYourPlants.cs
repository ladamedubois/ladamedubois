using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskChooseYourPlants : TutorialStep
{
	[BoxGroup("Completion References")] [SerializeField] private InventoryManager _inventory;
	[BoxGroup("Completion References")] [SerializeField] private Herbarium _herbarium;

	protected override void BindTaskShowCondition()
	{
		_herbarium.onClose += OnTaskShow;
		if (!_herbarium.IsOpened)
		{
			onStarted += OnTaskShow;
		}
	}

	protected override void UnbindTaskShowCondition()
	{
		_herbarium.onClose -= OnTaskShow;
		onStarted -= OnTaskShow;
	}

	protected override void BindTaskCompleteCondition()
	{
		_inventory.onFull += OnTaskCompleted;
	}

	protected override void UnbindTaskCompleteCondition()
	{
		_inventory.onFull -= OnTaskCompleted;
	}
}

using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class TutorialMessage : InfoMessage
{
	protected override void DoAppearAnimation(InfoMessageData message)
	{
		_canvasGroup.DOFade(1f, _appearTime).SetDelay(message.Delay).From(0f);

		_rectTransform.DOScaleX(1f, _appearTime / 2f).SetEase(Ease.OutBack).SetDelay(message.Delay).From(0f).OnStart(() =>
		{
			OnNotificationAppeared(message);
		});
	}
}

using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class TutorialStep : MonoBehaviour
{
    public event EventHandler onFinished;
	public event EventHandler onStarted;

    [SerializeField] protected List<GameObject> _lockedObjects;
    [SerializeField] protected List<GameObject> _highlightedObjects;
	[InlineEditor(InlineEditorObjectFieldModes.Boxed)] [SerializeField] protected InfoMessageData _taskMessage;

	private TutorialManager _manager;
	
	public void StartTask(TutorialManager manager)
	{
		_manager = manager;
		BindTaskShowCondition();
		BindTaskCompleteCondition();

		LockInteractions();

		onStarted?.Invoke(this, EventArgs.Empty);
	}

	public void ShowTask()
	{
		_manager.MessageFrame.Show(_taskMessage);
		HighlightObjects();
		UnbindTaskShowCondition();
	}

	public void FinishTask()
	{
		_manager.MessageFrame.Hide(_taskMessage);
		LockInteractions(false);
		HighlightObjects(false);
		UnbindTaskCompleteCondition();
		UnbindTaskShowCondition();
		onFinished?.Invoke(this, EventArgs.Empty);
	}

	protected abstract void BindTaskShowCondition();
	protected abstract void UnbindTaskShowCondition();

	protected abstract void BindTaskCompleteCondition();
	protected abstract void UnbindTaskCompleteCondition();

	protected void OnTaskCompleted<T>(object sender, T data) => FinishTask();
	public void OnTaskShow<T>(object sender, T data) => ShowTask();

	private void LockInteractions(bool locked = true)
	{
		foreach (GameObject obj in _lockedObjects)
		{
			ITutorialLock[] locks = obj.GetComponents<ITutorialLock>();
			foreach (ITutorialLock lck in locks)
			{
				if (locked) lck.TutorialLock(_manager);
				else lck.TutorialUnlock(_manager);
			}
		}
	}

	private void HighlightObjects(bool highlighted = true)
	{
		foreach (GameObject obj in _highlightedObjects)
		{
			InteractiveOutline outline = obj.GetComponent<InteractiveOutline>();
			outline.Highlighted = highlighted;
		}
	}

	protected void WaitBeforeShow(float delay)
	{
		_manager.WaitBeforeShow(this, delay);
	}

	protected void CancelWaitBeforeShow()
	{
		_manager.CancelWaitBeforeShow();
	}
}

// GENERATED AUTOMATICALLY FROM 'Assets/Settings/Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Controls"",
    ""maps"": [
        {
            ""name"": ""Character"",
            ""id"": ""250f72b1-02e2-46e5-a43f-273a4555fdc5"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Button"",
                    ""id"": ""41b64d1f-0faf-453e-9700-ecda87865eda"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""897d5f27-712c-48b9-a15a-930ba415295a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ToggleHerbarium"",
                    ""type"": ""Button"",
                    ""id"": ""1c87f78f-8545-47a1-8e1a-a61bf262a0ed"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""d06a385c-11dc-4ff6-a59f-5f31a0925251"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3efaee0d-18d3-4fb8-ba27-406bbc3cc147"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""da53b443-9eb9-4222-8da1-8fb834e9af6c"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ToggleHerbarium"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""199c1ded-348d-448d-a742-3598f6856500"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ToggleHerbarium"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Herbarium"",
            ""id"": ""a8f26e5c-65af-4a5e-b6a6-3df52be8189f"",
            ""actions"": [
                {
                    ""name"": ""TogglePlantDetails"",
                    ""type"": ""Button"",
                    ""id"": ""43e200ed-55dd-4705-9821-9841ae09b321"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""7791df29-c916-45cc-91c7-ac277951ac83"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TogglePlantDetails"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Menus"",
            ""id"": ""7ee75573-aa7a-429b-93b4-07564659e55b"",
            ""actions"": [
                {
                    ""name"": ""TogglePause"",
                    ""type"": ""Button"",
                    ""id"": ""9688be16-d09b-4ea2-8f83-7f72e9016a12"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""ef551161-0f35-4653-84e1-e25cd828e474"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TogglePause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Character
        m_Character = asset.FindActionMap("Character", throwIfNotFound: true);
        m_Character_Move = m_Character.FindAction("Move", throwIfNotFound: true);
        m_Character_Interact = m_Character.FindAction("Interact", throwIfNotFound: true);
        m_Character_ToggleHerbarium = m_Character.FindAction("ToggleHerbarium", throwIfNotFound: true);
        // Herbarium
        m_Herbarium = asset.FindActionMap("Herbarium", throwIfNotFound: true);
        m_Herbarium_TogglePlantDetails = m_Herbarium.FindAction("TogglePlantDetails", throwIfNotFound: true);
        // Menus
        m_Menus = asset.FindActionMap("Menus", throwIfNotFound: true);
        m_Menus_TogglePause = m_Menus.FindAction("TogglePause", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Character
    private readonly InputActionMap m_Character;
    private ICharacterActions m_CharacterActionsCallbackInterface;
    private readonly InputAction m_Character_Move;
    private readonly InputAction m_Character_Interact;
    private readonly InputAction m_Character_ToggleHerbarium;
    public struct CharacterActions
    {
        private @PlayerControls m_Wrapper;
        public CharacterActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Character_Move;
        public InputAction @Interact => m_Wrapper.m_Character_Interact;
        public InputAction @ToggleHerbarium => m_Wrapper.m_Character_ToggleHerbarium;
        public InputActionMap Get() { return m_Wrapper.m_Character; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CharacterActions set) { return set.Get(); }
        public void SetCallbacks(ICharacterActions instance)
        {
            if (m_Wrapper.m_CharacterActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_CharacterActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_CharacterActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_CharacterActionsCallbackInterface.OnMove;
                @Interact.started -= m_Wrapper.m_CharacterActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_CharacterActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_CharacterActionsCallbackInterface.OnInteract;
                @ToggleHerbarium.started -= m_Wrapper.m_CharacterActionsCallbackInterface.OnToggleHerbarium;
                @ToggleHerbarium.performed -= m_Wrapper.m_CharacterActionsCallbackInterface.OnToggleHerbarium;
                @ToggleHerbarium.canceled -= m_Wrapper.m_CharacterActionsCallbackInterface.OnToggleHerbarium;
            }
            m_Wrapper.m_CharacterActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
                @ToggleHerbarium.started += instance.OnToggleHerbarium;
                @ToggleHerbarium.performed += instance.OnToggleHerbarium;
                @ToggleHerbarium.canceled += instance.OnToggleHerbarium;
            }
        }
    }
    public CharacterActions @Character => new CharacterActions(this);

    // Herbarium
    private readonly InputActionMap m_Herbarium;
    private IHerbariumActions m_HerbariumActionsCallbackInterface;
    private readonly InputAction m_Herbarium_TogglePlantDetails;
    public struct HerbariumActions
    {
        private @PlayerControls m_Wrapper;
        public HerbariumActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @TogglePlantDetails => m_Wrapper.m_Herbarium_TogglePlantDetails;
        public InputActionMap Get() { return m_Wrapper.m_Herbarium; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(HerbariumActions set) { return set.Get(); }
        public void SetCallbacks(IHerbariumActions instance)
        {
            if (m_Wrapper.m_HerbariumActionsCallbackInterface != null)
            {
                @TogglePlantDetails.started -= m_Wrapper.m_HerbariumActionsCallbackInterface.OnTogglePlantDetails;
                @TogglePlantDetails.performed -= m_Wrapper.m_HerbariumActionsCallbackInterface.OnTogglePlantDetails;
                @TogglePlantDetails.canceled -= m_Wrapper.m_HerbariumActionsCallbackInterface.OnTogglePlantDetails;
            }
            m_Wrapper.m_HerbariumActionsCallbackInterface = instance;
            if (instance != null)
            {
                @TogglePlantDetails.started += instance.OnTogglePlantDetails;
                @TogglePlantDetails.performed += instance.OnTogglePlantDetails;
                @TogglePlantDetails.canceled += instance.OnTogglePlantDetails;
            }
        }
    }
    public HerbariumActions @Herbarium => new HerbariumActions(this);

    // Menus
    private readonly InputActionMap m_Menus;
    private IMenusActions m_MenusActionsCallbackInterface;
    private readonly InputAction m_Menus_TogglePause;
    public struct MenusActions
    {
        private @PlayerControls m_Wrapper;
        public MenusActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @TogglePause => m_Wrapper.m_Menus_TogglePause;
        public InputActionMap Get() { return m_Wrapper.m_Menus; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MenusActions set) { return set.Get(); }
        public void SetCallbacks(IMenusActions instance)
        {
            if (m_Wrapper.m_MenusActionsCallbackInterface != null)
            {
                @TogglePause.started -= m_Wrapper.m_MenusActionsCallbackInterface.OnTogglePause;
                @TogglePause.performed -= m_Wrapper.m_MenusActionsCallbackInterface.OnTogglePause;
                @TogglePause.canceled -= m_Wrapper.m_MenusActionsCallbackInterface.OnTogglePause;
            }
            m_Wrapper.m_MenusActionsCallbackInterface = instance;
            if (instance != null)
            {
                @TogglePause.started += instance.OnTogglePause;
                @TogglePause.performed += instance.OnTogglePause;
                @TogglePause.canceled += instance.OnTogglePause;
            }
        }
    }
    public MenusActions @Menus => new MenusActions(this);
    public interface ICharacterActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnInteract(InputAction.CallbackContext context);
        void OnToggleHerbarium(InputAction.CallbackContext context);
    }
    public interface IHerbariumActions
    {
        void OnTogglePlantDetails(InputAction.CallbackContext context);
    }
    public interface IMenusActions
    {
        void OnTogglePause(InputAction.CallbackContext context);
    }
}

using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameButton : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    public event EventHandler onClicked;
	public event EventHandler onCursorEnter;
	public event EventHandler onCursorExit;

	[BoxGroup("References")] [SerializeField] private Image _image;
	[BoxGroup("References")] [SerializeField] protected TextMeshProUGUI _label;

	[BoxGroup("Images")] [OnValueChanged("UpdateImage")] [SerializeField] private Sprite _baseImage;
    [BoxGroup("Images")] [SerializeField] private Sprite _hoverImage;
    [BoxGroup("Images")] [SerializeField] private Sprite _clickedImage;

    [SerializeField] [OnValueChanged("UpdateLabel")] private string _text;

	private bool _mouseOver;

	public bool Interactible { get; set; }

	private void Awake()
	{
		Interactible = true;
		UpdateImage();
		UpdateLabel();
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!Interactible) return;
		onClicked?.Invoke(this, EventArgs.Empty);
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		_mouseOver = true;
		_image.sprite = _hoverImage;
		onCursorEnter?.Invoke(this, EventArgs.Empty);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		_mouseOver = false;
		_image.sprite = _baseImage;
		onCursorExit?.Invoke(this, EventArgs.Empty);
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		_image.sprite = _clickedImage;
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		_image.sprite =_mouseOver ? _hoverImage : _baseImage;
	}

	private void UpdateImage()
	{
		_image.sprite = _baseImage;
	}

	private void UpdateLabel()
	{
		_label.text = _text;
	}
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting;
using UnityEngine.UIElements;

public class UISymptomsBlock : VisualElement
{
	public event EventHandler<Label> onBlockHovered;
	public event EventHandler<Label> onBlockSelected;
	public event EventHandler<Label> onBlockSelectionCancelled;

	private Label _currentTarget;

    public UISymptomsBlock()
	{
		RegisterCallback<MouseMoveEvent>(ev =>
		{
			if (_currentTarget == null) return;

			if (!_currentTarget.worldBound.Contains(ev.mousePosition))
			{
				onBlockSelectionCancelled?.Invoke(this, _currentTarget);
				_currentTarget = null;
			}
		});

		AddToClassList("dialog-block");
		AddToClassList("symptoms");
	}

	public Dictionary<Label, VisitorData.KeywordData> SetText(List<VisitorData.KeywordData> text)
	{
		Dictionary<Label, VisitorData.KeywordData> labelsData = new Dictionary<Label, VisitorData.KeywordData>();

		foreach (VisitorData.KeywordData textBlock in text)
		{
			Label _textLabel = new Label();
			_textLabel.text = textBlock.Text;
			_textLabel.AddToClassList((textBlock.Validity ? "" : "in") + "valid");
			_textLabel.AddToClassList("block-text");
			_textLabel.AddToClassList("symptom");

			labelsData.Add(_textLabel, textBlock);

			Add(_textLabel);

			RegisterCallbacksFor(_textLabel);
		}

		return labelsData;
	}

	private void RegisterCallbacksFor(Label label)
	{
		label.RegisterCallback<MouseDownEvent>(ev =>
		{
			_currentTarget = label;
			onBlockSelected?.Invoke(this, label);
		});

		label.RegisterCallback<MouseOutEvent>(ev =>
		{
			
		});

		label.RegisterCallback<MouseEnterEvent>(ev =>
		{
			onBlockHovered?.Invoke(this, label);
		});

		label.RegisterCallback<MouseUpEvent>(ev =>
		{
			onBlockSelectionCancelled?.Invoke(this, label);
		});
	}

	#region UXML
	[Preserve]
	public new class UxmlFactory : UxmlFactory<UISymptomsBlock, UxmlTraits> { }
	[Preserve]
	public new class UxmlTraits : VisualElement.UxmlTraits { }
	#endregion
}


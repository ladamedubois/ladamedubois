using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class ProgressDot : MonoBehaviour
{
    [BoxGroup("References")] [SerializeField] private Image _image;

    [BoxGroup("Color")] public Color SimpleBlockColor;
    [BoxGroup("Color")] public Color SymptomsBlockColor;

    public float SelectedScale;

    private RectTransform _rectTransform;

	private void Awake()
	{
		_rectTransform = GetComponent<RectTransform>();
	}

	public void SetIsSymptomsBlock(bool symptoms = true)
	{
        _image.color = symptoms ? SymptomsBlockColor : SimpleBlockColor;
	}

    public void SetCurrent(bool current = true)
	{
		float scale = current ? SelectedScale : 1f;
		_rectTransform.DOScale(scale, 0.2f);
	}
}

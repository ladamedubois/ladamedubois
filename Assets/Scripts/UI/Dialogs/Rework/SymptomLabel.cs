using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SymptomLabel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    public event EventHandler onCursorEnter;
    public event EventHandler onCursorExit;
	public event EventHandler onSelectionStarted;
	public event EventHandler onSelectionCancelled;
	public event EventHandler onSelected;

	[BoxGroup("References")] [SerializeField] private Image _image;
	[BoxGroup("References")] [SerializeField] private TextMeshProUGUI _text;
	[BoxGroup("References")] [SerializeField] private RectTransform _loadMask;

	[BoxGroup("Parameters")] [SerializeField] private Sprite _outlineImage;
	[BoxGroup("Parameters")] [SerializeField] private Sprite _filledImage;
	[BoxGroup("Parameters")] [SerializeField] private Color _validColor;
	[BoxGroup("Parameters")] [SerializeField] private Color _invalidColor;
	[BoxGroup("Parameters")] [SerializeField] private float _selectionTime;

	public bool Selected { get; private set; }

	public VisitorData.KeywordData Data { get; private set; }

	private bool _selecting;

	private void Awake()
	{
		_selecting = false;
		_loadMask.anchorMax = Vector2.up;
	}

	public void SetData(VisitorData.KeywordData data)
	{
		_text.text = data.Text;
		Data = data;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		onCursorEnter?.Invoke(this, EventArgs.Empty);

		if (Selected) return;

		_image.sprite = _filledImage;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		onCursorExit?.Invoke(this, EventArgs.Empty);

		if (Selected) return;
		
		_image.sprite = _outlineImage;

		if (_selecting)
			StopSelection();
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		if (_selecting || Selected) return;

		StartSelection();
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if (!_selecting || Selected) return;

		StopSelection();
	}

	private void StartSelection()
	{
		_selecting = true;

		onSelectionStarted?.Invoke(this, EventArgs.Empty);
		_loadMask.DOAnchorMax(Vector2.one, _selectionTime)
			.SetEase(Ease.OutSine)
			.SetId("SymptomLoadingBar")
			.OnComplete(() =>
		{
			Select();
			StopSelection(true);
		});
	}

	private void Select()
	{
		Selected = true;
		_image.color = Data.Validity ? _validColor : _invalidColor;
	}

	private void StopSelection(bool finished = false)
	{
		_selecting = false;
		DOTween.Kill("SymptomLoadingBar");
		_loadMask.anchorMax = Vector2.up;

		if (finished)
		{
			onSelected?.Invoke(this, EventArgs.Empty);
		}
		else
		{
			onSelectionCancelled?.Invoke(this, EventArgs.Empty);
		}
	}
}

using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressDotsManager : MonoBehaviour
{
    [BoxGroup("Prefabs")] [SerializeField] private GameObject _progressDotPrefab;

	[BoxGroup("References")] [SerializeField] private GameObject _progress;
	[BoxGroup("References")] [SerializeField] private Transform _dotsPool;


	List<ProgressDot> _dots;
	ProgressDot _currentDot;

	public int Count { get => _dots.Count; }

	public ProgressDotsManager()
	{
		_dots = new List<ProgressDot>();
	}

	public void Clear()
	{
        foreach (ProgressDot dot in _dots)
		{
			Destroy(dot.gameObject);
		}

		_dots.Clear();
	}

	public void AddDot(bool symptomsDot = false)
	{
		ProgressDot dot = Instantiate(_progressDotPrefab, _dotsPool).GetComponent<ProgressDot>();
		if (!dot) return;

		dot.SetIsSymptomsBlock(symptomsDot);
		_dots.Add(dot);
	}

	public void SetCurrent(int index)
	{
		if (index < 0 || index >= _dots.Count) return;

		_currentDot?.SetCurrent(false);
		_currentDot = _dots[index];
		_currentDot?.SetCurrent(true);
	}

	public void SetDisplay(bool show = true)
	{
		_progress.SetActive(show);
	}

	public bool IsFirstSelected()
	{
		return _currentDot == _dots[0];
	}

	public bool IsLastSelected()
	{
		return _currentDot == _dots[_dots.Count - 1];
	}

}

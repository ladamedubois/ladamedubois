using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(CanvasGroup))]
public class UIDialogBox : MonoBehaviour
{
	public event EventHandler onArrowPrevClicked;
	public event EventHandler onArrowNextClicked;

	public event EventHandler onSymptomBlockHovered;
	public event EventHandler onSymptomBlockSelectionStarted;
	public event EventHandler onSymptomBlockSelectionCancelled;
	public event EventHandler<VisitorData.KeywordData> onSymptomBlockSelected;

	public event EventHandler onTextAnimationStart;
	public event EventHandler<bool> onTextAnimationEnd; // true if symptom text block
	public event EventHandler onTextAnimationStartAppear;
	public event EventHandler onOpenAnimationStarted;
	public event EventHandler onCloseAnimationStarted;
	public event EventHandler onCloseAnimationFinished;

	[BoxGroup("References")] [SerializeField] private GameObject _textPool;
	[BoxGroup("References")] [SerializeField] private ProgressDotsManager _dots;
	[BoxGroup("References")] [SerializeField] private Button _prevArrowButton;
	[BoxGroup("References")] [SerializeField] private Button _nextArrowButton;
	[BoxGroup("References")] [SerializeField] private Image _profileImage;
	[BoxGroup("References")] [SerializeField] private Image _tutorialOutline;
	[BoxGroup("References")] [SerializeField] private TextMeshProUGUI _name;
	[BoxGroup("References")] [SerializeField] private RectTransform _nameLayout;
	[BoxGroup("References")] [SerializeField] private Image _transitionMask;


	[BoxGroup("Prefabs")] [SerializeField] private GameObject _simpleTextBlock;
	[BoxGroup("Prefabs")] [SerializeField] private GameObject _symptomsTextBlock;

	public float AppearTransitionTime = 0.5f;
	public float TextTransitionTime = 0.5f;
	public int TextBlocksCount { get => _textBlocks.Count; }
	public MultiLock TextTransitionLock { get; private set; }
	public bool IsOpen { get; private set; }

	private List<ITextBlock> _textBlocks;
	private List<GameObject> _textBlocksObjects;

	private RectTransform _rectTransform;
	private CanvasGroup _canvasGroup;
	private ITextBlock _currentTextBlock;
	private bool _firstTransitionDone = false;

	public UIDialogBox()
	{
		_textBlocks = new List<ITextBlock>();
		_textBlocksObjects = new List<GameObject>();
		TextTransitionLock = new MultiLock();
	}

	private void Awake()
	{
		_rectTransform = GetComponent<RectTransform>();
		_canvasGroup = GetComponent<CanvasGroup>();

		_prevArrowButton.onClick.AddListener(() =>
		{
			onArrowPrevClicked?.Invoke(this, EventArgs.Empty);
		});

		_nextArrowButton.onClick.AddListener(() =>
		{
			onArrowNextClicked?.Invoke(this, EventArgs.Empty);
		});
	}

	public void SetProfileImage(Sprite sprite)
	{
		_profileImage.sprite = sprite;
	}

	public void SetName(string name)
	{
		_name.SetText(name);
	}

	public SimpleTextBlock AddSimpleTextBlock(string text, bool addDot = true)
	{
		SimpleTextBlock block = Instantiate(_simpleTextBlock, _textPool.transform).GetComponent<SimpleTextBlock>();

		if (block != null)
		{
			block.SetContent(text);
			block.SetVisible(false);
			_textBlocks.Add(block);
		}

		_textBlocksObjects.Add(block.gameObject);

		if (_dots != null && addDot) _dots.AddDot(false);

		return block;
	}

	public SimpleTextBlock AddSimpleTextBlock(SimpleTextBlock block, bool addDot = true)
	{
		block.transform.parent = _textPool.transform;

		if (block != null)
		{
			block.SetVisible(false);
			_textBlocks.Add(block);
		}

		_textBlocksObjects.Add(block.gameObject);

		if (_dots != null && addDot) _dots.AddDot(false);

		return block;
	}

	public SymptomTextBlock AddSymptomTextBlock(List<VisitorData.KeywordData> blocksData)
	{
		SymptomTextBlock block = Instantiate(_symptomsTextBlock, _textPool.transform).GetComponent<SymptomTextBlock>();

		if (block != null)
		{
			block.SetContent(blocksData);
			block.SetVisible(false);
			_textBlocks.Add(block);
		}

		_textBlocksObjects.Add(block.gameObject);

		block.onBlockSelectionStarted += (sender, data) => onSymptomBlockSelectionStarted?.Invoke(this, EventArgs.Empty);
		block.onBlockSelectionCancelled += (sender, data) => onSymptomBlockSelectionCancelled?.Invoke(this, EventArgs.Empty);
		block.onBlockSelected += (sender, label) => onSymptomBlockSelected?.Invoke(this, label.Data);
		block.onBlockHovered += (sender, data) => onSymptomBlockHovered?.Invoke(this, EventArgs.Empty);

		if (_dots != null) _dots.AddDot(true);

		return block;
	}

	public void ClearTextBlocks()
	{
		_currentTextBlock = null;
		
		foreach (GameObject block in _textBlocksObjects)
		{
			Destroy(block);
		}

		_textBlocks.Clear();
		_textBlocksObjects.Clear();

		if (_dots != null) _dots.Clear();
	}

	public bool SetDialogIndex(int index)
	{
		if (!TextTransitionLock.IsFree()) return false;

		if (index >= _textBlocks.Count || index < 0) return false;

		TextTransitionLock.Set(this);
		onTextAnimationStart?.Invoke(this, EventArgs.Empty);

		_transitionMask.gameObject.SetActive(true);
		_transitionMask.material.SetFloat("_ProgressHide", _firstTransitionDone ? 0f : 1f);
		_transitionMask.material.SetFloat("_ProgressShow", 0f);

		_transitionMask.material.DOFloat(1.0f, "_ProgressHide", TextTransitionTime).OnComplete(() =>
		{
			_currentTextBlock?.Hide();
			_currentTextBlock = _textBlocks[index];
			_currentTextBlock?.Show();

			onTextAnimationStartAppear?.Invoke(this, EventArgs.Empty);

			_transitionMask.material.DOFloat(1.0f, "_ProgressShow", TextTransitionTime * 2f).OnComplete(() =>
			{
				_transitionMask.gameObject.SetActive(false);
				_firstTransitionDone = true;
				TextTransitionLock.Unset(this);
				onTextAnimationEnd?.Invoke(this, _currentTextBlock is SymptomTextBlock);
			});
		});

		if (_dots != null)
		{
			_dots.SetDisplay(index < _dots.Count);

			_dots.SetCurrent(index);
			_prevArrowButton.gameObject.SetActive(!_dots.IsFirstSelected());
			_nextArrowButton.gameObject.SetActive(!_dots.IsLastSelected());
		}

		return true;
	}

	public void Show()
	{
		gameObject.SetActive(true);
		_canvasGroup.alpha = 1.0f;

		LayoutRebuilder.ForceRebuildLayoutImmediate(_nameLayout);
		TextTransitionLock.Unlock();
		_firstTransitionDone = false;
		IsOpen = true;

		onOpenAnimationStarted?.Invoke(this, EventArgs.Empty);
		_rectTransform.DOAnchorMin(Vector2.zero, AppearTransitionTime).SetEase(Ease.OutBack, 1.1f).From(Vector2.left * 0.5f);
	}

	public void Hide(bool animate = true)
	{
		TextTransitionLock.Unlock();
		IsOpen = false;

		if (animate)
		{
			onCloseAnimationStarted?.Invoke(this, EventArgs.Empty);

			_canvasGroup.DOFade(0f, AppearTransitionTime).From(1f).SetEase(Ease.OutExpo);
			_rectTransform.DOAnchorMin(Vector2.left * 0.6f, AppearTransitionTime).SetEase(Ease.OutQuad).OnComplete(() =>
			{
				gameObject.SetActive(false);
				onCloseAnimationFinished?.Invoke(this, EventArgs.Empty);
			});
		}
		else
		{
			gameObject.SetActive(false);
			onCloseAnimationFinished?.Invoke(this, EventArgs.Empty);
		}
	}

	public void TutorialNextHighlight()
	{
		_tutorialOutline.DOKill();
		_tutorialOutline.DOFade(0.8f, 0.3f).OnComplete(() =>
		{
			_tutorialOutline.DOFade(0.2f, 1)
					.From(0.8f)
					.SetEase(Ease.InOutSine)
					.SetLoops(-1, LoopType.Yoyo);
		});
	}

	public void HideTutorialNextHighlight()
	{
		_tutorialOutline.DOKill();
		_tutorialOutline.DOFade(0f, 0.3f);
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITextBlock
{
	public void Show();
	public void Hide();
	public void SetVisible(bool visible);
}

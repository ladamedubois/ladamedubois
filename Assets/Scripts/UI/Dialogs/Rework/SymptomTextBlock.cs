using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SymptomTextBlock : MonoBehaviour, ITextBlock
{
	public event EventHandler<SymptomLabel> onBlockSelected;
	public event EventHandler<SymptomLabel> onBlockSelectionCancelled;
	public event EventHandler<SymptomLabel> onBlockSelectionStarted;
	public event EventHandler<SymptomLabel> onBlockHovered;

	[BoxGroup("Prefabs")] [SerializeField] private GameObject _symptomLabelPrefab;
	[BoxGroup("References")] [SerializeField] private RectTransform _blockLayout;

	public void SetContent(List<VisitorData.KeywordData> text)
	{
		foreach (VisitorData.KeywordData textBlock in text)
		{
			SymptomLabel label = Instantiate(_symptomLabelPrefab, transform).GetComponent<SymptomLabel>();
			label.SetData(textBlock);

			RegisterCallbacksFor(label);
		}
	}

	private void RegisterCallbacksFor(SymptomLabel label)
	{
		label.onSelected += (sender, data) =>
		{
			onBlockSelected?.Invoke(this, label);
		};

		label.onSelectionStarted += (sender, data) =>
		{
			onBlockSelectionStarted?.Invoke(this, label);
		};

		label.onSelectionCancelled += (sender, data) =>
		{
			onBlockSelectionCancelled?.Invoke(this, label);
		};

		label.onCursorEnter += (sender, data) =>
		{
			if (label.Selected) return;
			onBlockHovered?.Invoke(this, label);
		};
	}

	public void Show()
	{
		gameObject.SetActive(true);
		LayoutRebuilder.ForceRebuildLayoutImmediate(_blockLayout);
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}

	public void SetVisible(bool visible)
	{
		gameObject.SetActive(visible);
	}
}

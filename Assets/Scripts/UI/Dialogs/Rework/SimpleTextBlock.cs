using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SimpleTextBlock : MonoBehaviour, ITextBlock
{
	[BoxGroup("References")] [SerializeField] private TextMeshProUGUI _textField;

    public void SetContent(string text)
	{
		_textField.text = text;
	}

	public void Show()
	{
		gameObject.SetActive(true);
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}

	public void SetVisible(bool visible)
	{
		gameObject.SetActive(visible);
	}
}

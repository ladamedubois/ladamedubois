using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class KeywordsContainerSlot : MonoBehaviour
{
	[SerializeField] private TextMeshProUGUI _text;
	[SerializeField] private Image _slotMarker;
	[SerializeField] private float _markerOpacity = 0.2f;

	private bool _firstHidden = false;
	private Vector3 _startPosition;
	private RectTransform _rectTransform;

	public void Reset()
	{
		_text.text = "";
		_slotMarker.transform.localScale = Vector3.one;
		
		Color color = Color.white;
		color.a = 0;
		_slotMarker.color = color;

		_rectTransform = transform as RectTransform;
	}

	public void Show()
	{
		_slotMarker.DOFade(_markerOpacity, 1f).From(0f)
			.OnComplete(() =>
			{
				_rectTransform.DOLocalMoveX(4f, 3f + Random.value * 0.5f).SetRelative(true).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
				_rectTransform.DOLocalMoveY(2f, 2.24f + Random.value * 0.5f).SetRelative(true).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
			});
	}

	public void Hide()
	{
		//transform.DORewind();
	}

	public void SetKeyword(string keyword)
	{
		_text.text = keyword;
		_text.alpha = 0f;

		_text.DOFade(1f, 1f).From(0f);
		_slotMarker.DOFade(0f, 0.5f);
		_slotMarker.transform.DOScaleX(10f, 0.5f).SetEase(Ease.OutExpo);
	}
}

using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class UIDialog : MonoBehaviour, IDailyComponent
{
	public event EventHandler onOpen;
	public event EventHandler onClose;

	public event EventHandler onArrowPrevClicked;
	public event EventHandler onArrowNextClicked;
	public event EventHandler onExitDialogClicked;
	public event EventHandler onGiveRemedyClicked;
	public event EventHandler onTakeRewardClicked;

	public event EventHandler onSymptomBlockHover;
	public event EventHandler<VisitorData.KeywordData> onSymptomBlockStartSelect;
	public event EventHandler<VisitorData.KeywordData> onSymptomBlockCancelSelect;
	public event EventHandler<VisitorData.KeywordData> onSymptomBlockSelected;

	public event EventHandler<VisitorData.KeywordData> onHintFound;


	[BoxGroup("References")] [SerializeField] private UIDialogBox _dialogBox;
	//[BoxGroup("References")] [SerializeField] private DialogCursor _dialogCursor;
	[BoxGroup("References")] [SerializeField] private KeywordsContainer _keywordsContainer;
	[BoxGroup("References")] [SerializeField] private GameButton _exitDialogButton;
	[BoxGroup("References")] [SerializeField] private ImageGameButton _giveRemedyButton;
	[BoxGroup("References")] [SerializeField] private ImageGameButton _takeRewardButton;

	public UIDialogBox DialogBox { get => _dialogBox; }

	private VisualElement _root;

	private VisualElement _overlay;

	private Label _name;
	private VisualElement _image;

	private VisualElement _textBlocksContainer;
	private List<VisualElement> _textBlocks;
	private VisualElement _progress;

	private VisualElement _progressDotsContainer;
	private List<VisualElement> _progressDots;

	private VisualElement _currentDot;
	private VisualElement _currentBlock;

	private Dictionary<Label, VisitorData.KeywordData> _labelsData;
	private Label _currentSymptomSelected;

	private bool _isTutorial;
	private bool _firstSymptomPassed;
	private bool _tutorialHighlight;
	private int _indexBalance = 0;

	private void Awake()
	{
		_dialogBox.onCloseAnimationFinished += (sender, data) =>
		{
			gameObject.SetActive(false);
		};

		_dialogBox.onArrowPrevClicked += (sender, data) =>
		{
			if (_isTutorial && _firstSymptomPassed) _indexBalance--;
			onArrowPrevClicked?.Invoke(this, EventArgs.Empty);
		};

		_dialogBox.onArrowNextClicked += (sender, data) =>
		{
			if (_isTutorial && _firstSymptomPassed) _indexBalance++;

			if (_tutorialHighlight)
			{
				if (_firstSymptomPassed)
				{
					if (_indexBalance >= 1)
					{
						_tutorialHighlight = false;
						DialogBox.HideTutorialNextHighlight();
					}
				}
				else
				{
					_tutorialHighlight = false;
					DialogBox.HideTutorialNextHighlight();
				}
			}

			onArrowNextClicked?.Invoke(this, EventArgs.Empty);
		};

		_exitDialogButton.onClicked += (sender, data) =>
		{
			onExitDialogClicked?.Invoke(this, EventArgs.Empty);
		};

		_giveRemedyButton.onClicked += (sender, data) =>
		{
			_giveRemedyButton.GiveAnimation();
			_giveRemedyButton.onAnimationEnd += OnGiveAnimationEnd;
		};

		// Tutorial
		_dialogBox.onSymptomBlockSelected += (sender, data) =>
		{
			if (!_isTutorial) return;
			if (_indexBalance > 0) return;
			if (!data.Validity) return;

			_tutorialHighlight = true;
			DialogBox.TutorialNextHighlight();
		};

		_dialogBox.onTextAnimationEnd += (sender, symptom) =>
		{
			if (!_isTutorial) return;
			if (symptom && _indexBalance > 0)
			{
				_isTutorial = false; // Disable tutorial for the rest of the day
				_tutorialHighlight = false;
				DialogBox.HideTutorialNextHighlight();
			}

			if (symptom && !_firstSymptomPassed) {
				_firstSymptomPassed = true;
				_indexBalance = 0;
			}
		};
	}

	private void OnGiveAnimationEnd(object sender, EventArgs data)
	{
		onGiveRemedyClicked?.Invoke(this, EventArgs.Empty);
		//_takeRewardButton.gameObject.SetActive(true);

		_giveRemedyButton.Hide();
		_giveRemedyButton.onAnimationEnd -= OnGiveAnimationEnd;

		_takeRewardButton.Show();
		_takeRewardButton.ReceiveAnimation();
		_takeRewardButton.onClicked += OnTakeRewardClicked;
	}

	private void OnTakeRewardClicked(object sender, EventArgs data)
	{
		_takeRewardButton.onClicked -= OnTakeRewardClicked;
		onTakeRewardClicked?.Invoke(this, EventArgs.Empty);
	}

	public void LoadDay(DaysManager.DayData dayData)
	{
		_dialogBox.SetName(dayData.DailyMission.Visitor.Name);
		_dialogBox.SetProfileImage(dayData.DailyMission.Visitor.NeutralDialogImage);

		CreateTextBlocks(dayData);
		//_dialogBox.SetDialogIndex(0, false);

		_giveRemedyButton.SetContentImage(dayData.DailyMission.RemedyImage);
		_takeRewardButton.SetContentImage(dayData.DailyMission.RewardImage, true);

		_isTutorial = dayData.DailyMission.EnableTutorial;
		_tutorialHighlight = _isTutorial;
		_firstSymptomPassed = false;
		_indexBalance = 0;
		if (_tutorialHighlight) DialogBox.TutorialNextHighlight();

		Close(false);
	}

	private void CreateTextBlocks(DaysManager.DayData dayData)
	{
		foreach (VisitorData.DialogText text in dayData.DailyMission.Visitor.LoreText)
		{
			_dialogBox.AddSimpleTextBlock(text.Text);
		}

		for (int i = 0; i < dayData.DailyMission.Visitor.SymptomsText.Count; i++)
		{
			List<VisitorData.KeywordData> keywords = dayData.DailyMission.Visitor.ParseSymptoms(i);

			SymptomTextBlock block = _dialogBox.AddSymptomTextBlock(keywords);

			block.onBlockSelected += OnBlockSelected;
		}

		_dialogBox.AddSimpleTextBlock(dayData.DailyMission.Visitor.WaitingText.Text, false);
		_dialogBox.AddSimpleTextBlock(dayData.DailyMission.Visitor.RewardText.Text, false);
	}

	public void Open(DialogManager.State state)
	{
		gameObject.SetActive(true);
		_dialogBox.Show();

		if (state == DialogManager.State.WaitForRemedy)
			AllowQuitDialog(true);

		if (state == DialogManager.State.GiveRemedy)
		{
			_giveRemedyButton.Show();
		}

		onOpen?.Invoke(this, EventArgs.Empty);
	}

	public void Close(bool animate = true)
	{
		onClose?.Invoke(this, EventArgs.Empty);

		_keywordsContainer.HideBubble();

		_dialogBox.Hide(animate);

		_exitDialogButton.gameObject.SetActive(false);
		_giveRemedyButton.gameObject.SetActive(false);
		_takeRewardButton.gameObject.SetActive(false);
	}

	/**
	 * Show the "quit dialog" button on the interface
	 */
	public void AllowQuitDialog(bool allow = true)
	{
		_exitDialogButton.gameObject.SetActive(allow);
	}

	public void OnBlockSelected(object sender, SymptomLabel symptom)
	{
		if (symptom.Data.Validity)
			onHintFound?.Invoke(this, symptom.Data);
	}

	/**
	 * Adds a new keyword in the container
	 */
	public void ValidateHint(string hint)
	{
		_keywordsContainer.AddKeyword(hint);
	}

	public void Reset()
	{
		_dialogBox.ClearTextBlocks();
		_keywordsContainer.Reset();
	}
}

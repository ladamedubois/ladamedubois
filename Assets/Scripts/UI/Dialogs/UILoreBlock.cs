using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting;
using UnityEngine.UIElements;

public class UILoreBlock : VisualElement
{
    private Label _textLabel;

    public UILoreBlock()
	{
		_textLabel = new Label();

		_textLabel.AddToClassList("block-text");
		AddToClassList("dialog-block");
		AddToClassList("lore");
		
		Add(_textLabel);
	}

	public void SetText(string text)
	{
		_textLabel.text = text;
	}

	#region UXML
	[Preserve]
	public new class UxmlFactory : UxmlFactory<UILoreBlock, UxmlTraits> { }
	[Preserve]
	public new class UxmlTraits : VisualElement.UxmlTraits { }
	#endregion
}


using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class KeywordsContainer : MonoBehaviour, IDailyComponent
{
	public event EventHandler<string> onKeywordAdded;
	public event EventHandler onAllSlotsFilled;
	public event EventHandler<string> onFirstKeywordAdded;

    [BoxGroup("References")] [SerializeField] private List<KeywordsContainerSlot> _slots;
	[BoxGroup("References")] [SerializeField] private Image _bubble;

	private int _currentSlotIndex;

	private void Awake()
	{
		onFirstKeywordAdded += (sender, keyword) => ShowBubble(keyword);
	}

	/**
	 * Tries to add a new keyword, if the bubble is not visible, displays the bubble instead
	 */
	public void AddKeyword(string keyword)
	{
		if (_currentSlotIndex >= _slots.Count) return;

		if (_currentSlotIndex == -1)
		{
			onFirstKeywordAdded?.Invoke(this, keyword);
		} 
		else
		{
			KeywordsContainerSlot slot = _slots[_currentSlotIndex];
			slot.SetKeyword(keyword);
			onKeywordAdded?.Invoke(this, keyword);
		}

		_currentSlotIndex++;

		if (_currentSlotIndex >= _slots.Count) onAllSlotsFilled?.Invoke(this, EventArgs.Empty);
	}

    public void Reset()
	{
		_currentSlotIndex = -1;
		foreach (KeywordsContainerSlot slot in _slots)
		{
			slot.Reset();
		}

		_bubble.rectTransform.localScale = Vector3.zero;
	}

	public void LoadDay(DaysManager.DayData dayData)
	{
	}

	/**
	 * Spawns the bubble container then shows the first keyword
	 */
	private void ShowBubble(string firstKeyword)
	{
		foreach (KeywordsContainerSlot slot in _slots)
		{
			slot.Show();
		}

		_bubble.rectTransform.DOKill();
		_bubble.rectTransform.DOScale(1.0f, 0.8f).SetEase(Ease.OutBack).From(Vector3.zero)
			.OnComplete(() =>
			{
				AddKeyword(firstKeyword);
				_bubble.rectTransform.DOLocalMoveY(-5, 2).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
			});

		_bubble.DOFade(1f, 1.5f).From(0f);
	}

	public void HideBubble()
	{
		foreach (KeywordsContainerSlot slot in _slots)
		{
			slot.Hide();
		}

		_bubble.rectTransform.DORewind();
		_bubble.rectTransform.DOKill();
		_bubble.rectTransform.DOScale(0.0f, 0.4f).SetEase(Ease.OutExpo);
	}
}

using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageGameButton : GameButton
{
	public event EventHandler onAnimationEnd;

    [BoxGroup("References")] [SerializeField] private Image _contentImage;
	[BoxGroup("References")] [SerializeField] private Image _labelImage;

	[BoxGroup("References")] [SerializeField] private RectTransform _characterTransform;

	[SerializeField] private float _animationTime;

	private Vector3 _initialPosition;

	public void Show()
	{
		gameObject.SetActive(true);
		_initialPosition = _contentImage.rectTransform.localPosition + Vector3.zero;
		_contentImage.transform.localScale = Vector3.one;
		_labelImage.color = Color.white;
		Interactible = true;
	}

	public void Hide()
	{
		_contentImage.rectTransform.localPosition = _initialPosition;
		gameObject.SetActive(false);
	}

	public void GiveAnimation()
	{
		Interactible = false;
		_labelImage.DOFade(0f, 0.2f);
		_contentImage.transform.DOLocalRotate(Vector3.forward * 50, _animationTime).SetEase(Ease.InOutFlash, 2, 0);
		_contentImage.transform.DOScale(0.5f, _animationTime);

		Vector3 target = _characterTransform.InverseTransformPoint(_contentImage.transform.position);
		target.x *= -1;

		DOTween.Sequence()
			.Append(
				_contentImage.rectTransform.DOLocalJump(target, 250f, 1, _animationTime)
			)
			.AppendInterval(0.5f)
			.OnComplete(() =>
			{
				onAnimationEnd?.Invoke(this, EventArgs.Empty);
			});
	}

	public void ReceiveAnimation()
	{
		Color color = Color.white;
		color.a = 0f;
		_labelImage.color = color;

		Interactible = false;

		_contentImage.rectTransform.position = _characterTransform.transform.position;
		_contentImage.transform.DOLocalRotate(Vector3.forward * -30, _animationTime).SetEase(Ease.InOutFlash, 2, 0);
		_contentImage.transform.DOScale(1f, _animationTime).From(0.5f);

		_contentImage.rectTransform.DOLocalJump(_initialPosition, 250f, 1, _animationTime)
			.OnComplete(() =>
			{
				_labelImage.DOFade(1f, 0.2f);
				Interactible = true;
				onAnimationEnd?.Invoke(this, EventArgs.Empty);
			});
	}

	public void SetContentImage(Sprite image, bool adaptText = false)
	{
		if (image == null) 
		{
			_contentImage.enabled = false;
			if (adaptText) _label.text = "Quitter";
		}
		else
		{
			_contentImage.enabled = true;
			_contentImage.sprite = image;
			if (adaptText) _label.text = "Accepter et quitter";
		}
	}
}

using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class DialogCursor : MonoBehaviour
{
    public event EventHandler onLoadingStarted;
    public event EventHandler onLoadingCancelled;
    public event EventHandler onLoadingFinished;

    public float LoadingTime = 2f;

    [BoxGroup("References")] [SerializeField] private UIDocument _document;
    [BoxGroup("References")] [SerializeField] private List<Sprite> _loaderFrames;
    private VisualElement _image;

    private float _startLoadingTime = -1;

    public float Progress { get; private set; }

	private void Awake()
	{
        _image = _document.rootVisualElement.Q<VisualElement>("Cursor");
        _image.style.display = DisplayStyle.None;
    }

	private void Update()
	{        
        MoveToCusor();
        if (_startLoadingTime < 0) return;


        Progress = Mathf.Clamp01((Time.time - _startLoadingTime) / LoadingTime);

        int index = (int)((_loaderFrames.Count - 1) * Progress);

        _image.style.backgroundImage = _loaderFrames[index].texture;

        if (Progress >= 1)
		{
            StopLoading();
		}
	}

    private void MoveToCusor()
	{
        Vector2 mousePos = Mouse.current.position.ReadValue();

        _image.style.bottom = mousePos.y - _image.layout.height / 2;
        _image.style.left = mousePos.x - _image.layout.width / 2;
    }

    public void StartLoading()
	{
        _startLoadingTime = Time.time;
        Progress = 0;

        _image.style.display = DisplayStyle.Flex;
        _image.style.backgroundImage = _loaderFrames[0].texture;

        onLoadingStarted?.Invoke(this, EventArgs.Empty);
    }

    public void StopLoading()
	{
        _startLoadingTime = -1;

        _image.style.display = DisplayStyle.None;

        if (Progress >= 1)
		{
            onLoadingFinished?.Invoke(this, EventArgs.Empty);
		}
		else
		{
            onLoadingCancelled?.Invoke(this, EventArgs.Empty);
		}

        Progress = 0;
    }
}

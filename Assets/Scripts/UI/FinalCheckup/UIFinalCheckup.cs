using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIFinalCheckup : MonoBehaviour, IPointerClickHandler
{
	public class FinalCheckupData
	{
		public bool IsFabre;
		public bool IsGoodEnding;
	}

	public event EventHandler<FinalCheckupData> onOpen;
	public event EventHandler onClose;
	public event EventHandler onStartClose;

	[BoxGroup("References")] [SerializeField] private Image _visitorImage;
	[BoxGroup("References")] [SerializeField] private Image _backgroundImage;
	[BoxGroup("References")] [SerializeField] private UIDialogBox _dialogBox;
	[BoxGroup("References")] [SerializeField] private TransitionFader _transition;

	public UIDialogBox DialogBox { get => _dialogBox; }

	private int _currentDialogIndex;
	private bool _canSkip;

	private bool _fabre;
	private bool _goodEnding;

	private void Awake()
	{/*
		_dialogBox.onTextAnimationEnd += (sender, data) => _bindGroups.SetBindGroup("FabreDialogPass");
		_dialogBox.onTextAnimationStart += (sender, data) => 
	*/}

	public void FillData(MissionData mission, bool goodEnding, Remedy remedy)
	{
		_backgroundImage.color = mission.Visitor.CheckupColor;
		_dialogBox.SetProfileImage(mission.Visitor.NeutralDialogImage);
		_dialogBox.SetName(mission.Visitor.name);

		_goodEnding = goodEnding;
		_fabre = mission.ReputationBased;

		List<string> ending = goodEnding ? mission.GoodRemedyText : mission.BadRemedyText;
		string remedyComposition = remedy.GetCompositionString();

		_dialogBox.ClearTextBlocks();

		foreach (string endingBlock in ending)
		{
			string endingBlockFormatted = endingBlock.Replace("$", remedyComposition);
			_dialogBox.AddSimpleTextBlock(endingBlockFormatted);
		}

		_currentDialogIndex = 0;
	}

	public void Open()
	{
		_transition.onTransitionEnd += OnTransitionFinished;
		_transition.DoFromTransparent(1.5f, "DayEnd");
		_canSkip = false;
	}

	public void Close()
	{
		_transition.DoFromTransparent(2.5f, "CloseFinalCheckup");
		onStartClose?.Invoke(this, EventArgs.Empty);
	}
	
	public void OnTransitionFinished(object sender, string name)
	{
		if (name == "DayEnd")
		{
			gameObject.SetActive(true);

			onOpen?.Invoke(this, new FinalCheckupData() { IsGoodEnding = _goodEnding, IsFabre = _fabre });
			_transition.DoFromBlack(1f, "OpenFinalCheckup", 2.6f);
		}
		else if (name == "OpenFinalCheckup")
		{
			_dialogBox.SetDialogIndex(0);
			_canSkip = true;
		}
		else if (name == "CloseFinalCheckup")
		{
			_transition.onTransitionEnd -= OnTransitionFinished;
			gameObject.SetActive(false);

			onClose?.Invoke(this, EventArgs.Empty);
		}
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!_canSkip) return;
		if (!_dialogBox.TextTransitionLock.IsFree()) return;

		_currentDialogIndex++;

		if (_currentDialogIndex >= _dialogBox.TextBlocksCount)
		{
			Close();
		}

		_dialogBox.SetDialogIndex(_currentDialogIndex);
	}
}

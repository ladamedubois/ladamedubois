using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICrafting : MonoBehaviour, IDailyComponent
{
    public event EventHandler onPrepareClicked;
    public event EventHandler onCancelClicked;

    [BoxGroup("References")] [SerializeField] private GameButton _prepareButton;
    [BoxGroup("References")] [SerializeField] private GameButton _cancelButton;
	[BoxGroup("References")] [SerializeField] private UIDialogBox _dialogBox;
	[BoxGroup("References")] [SerializeField] private SimpleTextBlock _textBlock;

	public UIDialogBox DialogBox { get => _dialogBox; }

	private void Awake()
	{
		_prepareButton.onClicked += (sender, data) =>
		{
			onPrepareClicked?.Invoke(this, EventArgs.Empty);
		};

		_cancelButton.onClicked += (sender, data) =>
		{
			onCancelClicked?.Invoke(this, EventArgs.Empty);
		};

		_dialogBox.AddSimpleTextBlock(_textBlock);

		_dialogBox.onCloseAnimationFinished += (sender, data) =>
		{
			gameObject.SetActive(false);
		};
	}

	public void Hide(bool animate = true)
	{
		_dialogBox.Hide(animate);
		_cancelButton.gameObject.SetActive(false);
		_prepareButton.gameObject.SetActive(false);
	}

	public void Show()
	{
		gameObject.SetActive(true);
		_cancelButton.gameObject.SetActive(true);
		_prepareButton.gameObject.SetActive(true);
		_dialogBox.Show();
		_dialogBox.SetDialogIndex(0);
	}

	public void Reset()
	{
	}

	public void LoadDay(DaysManager.DayData dayData)
	{
		Hide(false);
	}
}

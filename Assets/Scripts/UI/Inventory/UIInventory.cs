using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInventory : MonoBehaviour, IDailyComponent
{
	public event EventHandler<InventoryManager.ItemEventData> onGhostItemArrived;
	public event EventHandler onItemRemoved;
	public event EventHandler<int> onItemClicked;

	[BoxGroup("References")] [SerializeField] List<UIInventorySlot> _slots;
	[BoxGroup("References")] [SerializeField] Image _ghostItem;
	[BoxGroup("References")] [SerializeField] Camera _camera;

	public float TransitionTime = 0.3f;

	public float GhostSlotTime = 0.5f;

	public bool TutorialMode { get; private set; }

	private float _startXPos;

	private InventoryManager.State _currentState;

	private void Awake()
	{
		_startXPos = transform.localPosition.x;
		_ghostItem.gameObject.SetActive(false);

		for (int i = 0; i < _slots.Count; i++)
		{
			_slots[i].Index = i;
			_slots[i].onClicked += (sender, filled) => OnItemClicked(sender as UIInventorySlot, filled);
		}

		onGhostItemArrived += OnItemArriveInInventory;
	}

	public void OnAddItem(InventoryManager.ItemEventData data)
	{
		if (data.WorldSourcePosition == null) onGhostItemArrived?.Invoke(this, data);

		int slotIndex = _currentState == InventoryManager.State.Remedy ? _slots.Count - 1 : data.Slot;
		Vector2 slotPosition = transform.InverseTransformPoint(_slots[slotIndex].Position);
		Vector2 sourceCanvasPosition = RectTransformUtility.WorldToScreenPoint(_camera, data.WorldSourcePosition.GetValueOrDefault());

		sourceCanvasPosition = transform.InverseTransformPoint(sourceCanvasPosition);

		_ghostItem.gameObject.SetActive(true);
		_ghostItem.sprite = data.Item.GetImage();

		AnimateGhostItem(sourceCanvasPosition, slotPosition, data);
	}

	public void OnItemArriveInInventory(object sender, InventoryManager.ItemEventData data)
	{
		_ghostItem.gameObject.SetActive(false);
		_slots[data.Slot].SetItem(data.Item);
	}

	public void OnRemoveItem(InventoryManager.ItemEventData data)
	{
		_slots[data.Slot].ClearItem();
		onItemRemoved?.Invoke(this, EventArgs.Empty);
	}

	public void OnClear()
	{
		foreach (UIInventorySlot slot in _slots)
		{
			slot.ClearItem();
		}
	}

	public void OnStateChange(InventoryManager.State state)
	{
		_currentState = state;

		if (state == InventoryManager.State.Plants) Expand();
		else if (state == InventoryManager.State.Remedy) Collapse();
	}

	private void Collapse()
	{
		Vector3 collapsePosition = _slots[_slots.Count - 1].InitialPosition;

		foreach (UIInventorySlot slot in _slots)
		{
			slot.gameObject.LeanMoveLocal(collapsePosition, 0.4f).setEaseInBack();
		}
	}

	private void Expand()
	{
		foreach (UIInventorySlot slot in _slots) slot.ResetPosition();
	}

	public void Show()
	{
		transform.DOKill();
		transform.DOLocalMoveX(_startXPos, TransitionTime).From(1920f + 530f).SetEase(Ease.OutQuad);

		_slots.ForEach(slot =>
		{
			_slots.ForEach(slot => slot.gameObject.SetActive(true));
		});
	}

	public void Hide()
	{
		transform.DOKill();
		transform.DOLocalMoveX(1920f + 530f, TransitionTime).SetEase(Ease.OutQuad).OnComplete(() =>
		{
			_slots.ForEach(slot => slot.gameObject.SetActive(false));
		});
	}

	public void AllowRemoveItems(bool allow)
	{
		foreach (UIInventorySlot slot in _slots)
		{
			slot.AllowRemoving = allow;
		}
	}

	private void AnimateGhostItem(Vector2 from, Vector2 to, InventoryManager.ItemEventData data)
	{
		DOTween.Sequence()
			.Append(
				_ghostItem.rectTransform.DOLocalMoveY(from.y, GhostSlotTime).From(from.y - 100)
					.SetEase(Ease.OutBack)
			)
			.Join(
				_ghostItem.rectTransform.DOScale(1f, GhostSlotTime).From(0f)
			)
			.Append(
				_ghostItem.rectTransform.DOLocalMove(to, GhostSlotTime).From(from)
				.OnComplete(() =>
				{
					onGhostItemArrived?.Invoke(this, data);
				})
			)
			.Join(
				_ghostItem.rectTransform.DOScale(0f, GhostSlotTime).From(1f).SetEase(Ease.InQuad)
			);
	}

	private void OnItemClicked(UIInventorySlot slot, bool filled)
	{
		if (!filled) return;
		onItemClicked?.Invoke(this, slot.Index);
	}

	public void Reset()
	{
	}

	public void LoadDay(DaysManager.DayData dayData)
	{
		TutorialMode = dayData.DailyMission.EnableTutorial;
		Hide();
		_currentState = InventoryManager.State.Plants;
	}
}

using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIInventorySlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
	public event EventHandler<bool> onClicked; // bool is true depending on if the slot is full or not
	public event EventHandler<bool> onMouseEnter; // bool is true depending on if the slot is full or not
	public event EventHandler onItemSet;
	public event EventHandler onItemCleared;

    [BoxGroup("References")] [SerializeField] private Image _contentImage;
	[BoxGroup("References")] [SerializeField] private Image _contentImageFull;
	[BoxGroup("References")] [SerializeField] private Image _clearCross;
	[BoxGroup("References")] [SerializeField] private TextMeshProUGUI _contentLabel;

	private RectTransform _rectTransform;
	public Vector3 InitialPosition { get; private set; }
	public Vector3 Position { get => _rectTransform.position; }
	public int Index { get; set; }
	public bool AllowRemoving { get; set; }

	private void Awake()
	{
		_rectTransform = GetComponent<RectTransform>();
		InitialPosition = _rectTransform.localPosition;
		_clearCross.enabled = false;
		AllowRemoving = true;
	}

	public void SetItem(IInventoryItem item)
	{
		onItemSet?.Invoke(this, EventArgs.Empty);

		// Could use substitution here :/

		Image affectedImage = _contentImage;

		Plant plant = item as Plant;
		if (plant != null) {
			_contentImage.enabled = true;
			_contentLabel.enabled = true;
			_contentImageFull.enabled = false;
			_contentLabel.text = plant.Type.CommonName;
		}
		else
		{
			Remedy remedy = item as Remedy;
			if (remedy != null)
			{
				_contentImage.enabled = true;
				_contentLabel.enabled = true;
				_contentImageFull.enabled = true;
				_contentLabel.text = remedy.GetName();
			}
		}

		affectedImage.sprite = item.GetImage();

		DoSpawnAnimation(affectedImage);
	}

    public void ClearItem()
	{
		onItemCleared?.Invoke(this, EventArgs.Empty);

		_contentImage.enabled = false;
		_contentImageFull.enabled = false;
		_clearCross.enabled = false;
		_contentLabel.enabled = false;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		onMouseEnter?.Invoke(this, _contentImage.enabled || _contentImageFull.enabled);

		if (_contentImageFull.enabled || !AllowRemoving) return;

		_clearCross.enabled = true;
		_clearCross.rectTransform.DOKill();
		_clearCross.rectTransform.DOScale(1.0f, 0.2f).SetEase(Ease.OutBack, 1.1f);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		_clearCross.rectTransform.DOKill();
		_clearCross.rectTransform.DOScale(0f, 0.2f).SetEase(Ease.OutExpo).OnComplete(() =>
		{
			_clearCross.enabled = false;
		});
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!AllowRemoving) return;

		onClicked?.Invoke(this, _contentImage.enabled || _contentImageFull.enabled);
	}

	public void ResetPosition()
	{
		_rectTransform.localPosition = InitialPosition;
	}

	private void DoSpawnAnimation(Image image)
	{
		image.gameObject.LeanScale(Vector3.one * 1.4f, 0.2f).setEaseOutQuad().setOnComplete(() =>
		{
			image.gameObject.LeanScale(Vector3.one, 0.2f).setEaseOutQuad();
		});
	}
}

using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIIndications : MonoBehaviour
{
	/*
	[DictionaryDrawerSettings(DisplayMode = DictionaryDisplayOptions.ExpandedFoldout, ValueLabel = "Bind groups")]
	public Dictionary<string, string> _bindGroups;
*/
	/*
	[Serializable]
	public class BindGroup
	{
		public string Name;
		public List<BindIndicator> Binds;
		public float OffsetDelay;

		public void Show() => Binds.ForEach(bind => bind.Show(OffsetDelay));
		public void Hide() => Binds.ForEach(bind => bind.Hide());
	}*/

	//[SerializeField] private List<BindGroup> _bindGroups;

	[BoxGroup("References")] [SerializeField] private InfoMessage _notification;
	[BoxGroup("References")] [SerializeField] private TutorialMessage _tutorialTask;

	[BoxGroup("References")] [SerializeField] private CraftingStation _crafting;
	[BoxGroup("References")] [SerializeField] private Bed _bed;
	[BoxGroup("References")] [SerializeField] private GardenManager _garden;
	[BoxGroup("References")] [SerializeField] private DialogManager _dialog;
	[BoxGroup("References")] [SerializeField] private VisitorDoor _visitorDoor;
	[BoxGroup("References")] [SerializeField] private GardenDoor _gardenDoor;
	[BoxGroup("References")] [SerializeField] private GardenDoor _houseDoor;
	[BoxGroup("References")] [SerializeField] private UIFinalCheckup _finalCheckup;

	[BoxGroup("Notifications Blocked")] [SerializeField] private InfoMessageData _alreadyCraftedNotification;
	[BoxGroup("Notifications Blocked")] [SerializeField] private InfoMessageData _alreadyGatheredNotification;
	[BoxGroup("Notifications Blocked")] [SerializeField] private InfoMessageData _cantGoOutsideNotification;
	[BoxGroup("Notifications Blocked")] [SerializeField] private InfoMessageData _cantSleepNotification;
	[BoxGroup("Notifications Blocked")] [SerializeField] private InfoMessageData _noInventorySpaceNotification;
	[BoxGroup("Notifications Blocked")] [SerializeField] private InfoMessageData _notEnoughPlantsNotification;
	[BoxGroup("Notifications Blocked")] [SerializeField] private InfoMessageData _cantCraftNoHintNotification;
	[BoxGroup("Notifications Blocked")] [SerializeField] private InfoMessageData _cantOpenNoMoreVisitor;
	[BoxGroup("Notifications Blocked")] [SerializeField] private InfoMessageData _cantOpenTooLate;

	[BoxGroup("Notifications Information")] [SerializeField] private InfoMessageData _elementsMemorizedNotification;
	[BoxGroup("Notifications Information")] [SerializeField] private InfoMessageData _remedyReadyNotification;

	[BoxGroup("Notifications Tutorial")] [SerializeField] private InfoMessageData _openVisitorDoorTask;
	[BoxGroup("Notifications Tutorial")] [SerializeField] private InfoMessageData _findImportantWordsTask;


	public InfoMessage Notification { get => _notification; }
	public InfoMessage TutorialTask { get => _tutorialTask; }

	/*
	private List<BindIndicator> _bindIndicators;
	private BindGroup _currentGroup;

	private Stack<BindGroup> _bindGroupsStack;*/
	private bool _isFirstDialogState;
	/*
	private void Awake()
	{
		_bindIndicators = new List<BindIndicator>(GetComponentsInChildren<BindIndicator>());
		_bindGroupsStack = new Stack<BindGroup>();
	}
	*/
	
	private void Start()
	{
		/*_bindIndicators.ForEach(bind =>
		{
			bind.SetVisible(false);
		});*/

		_notification.SetVisible(false);
		_tutorialTask.SetVisible(false);

		BindNotifications();
	}

	#region [Notifications]

	private void BindNotifications()
	{
		_crafting.onCantUseAlreadyCrafted += (sender, data) => Notification.Show(_alreadyCraftedNotification);
		_crafting.onCantUseNoHints += (sender, data) => Notification.Show(_cantCraftNoHintNotification);
		_crafting.onCantUseNotAllIngredients += (sender, data) => Notification.Show(_notEnoughPlantsNotification);

		_garden.onCantGatherAlreadyInInventory += (sender, data) => Notification.Show(_alreadyGatheredNotification);
		_garden.onCantGatherInventoryFull += (sender, data) => Notification.Show(_noInventorySpaceNotification);

		_bed.onCantSleep += (sender, data) => Notification.Show(_cantSleepNotification);

		_dialog.onDialogOpen += (sender, state) => { if (state == DialogManager.State.NotStarted) _isFirstDialogState = true; };

		_dialog.onDialogClose += (sender, state) =>
		{
			if (_isFirstDialogState)
			{
				Notification.Show(_elementsMemorizedNotification);
				_isFirstDialogState = false;
			}
		};

		_crafting.onRemedyCrafted += (sender, data) => Notification.Show(_remedyReadyNotification);
		_gardenDoor.onCantUseTooLate += (sender, data) => Notification.Show(_cantOpenTooLate);
		_visitorDoor.onCantUseOnOne += (sender, data) => Notification.Show(_cantOpenNoMoreVisitor);

		_dialog.onDialogOpen += (sender, data) => Notification.Hide();
		_houseDoor.onStartTravelDoor += (sender, data) => Notification.Hide();
		_gardenDoor.onStartTravelDoor += (sender, data) => Notification.Hide();
		_crafting.onCraftingOpen += (sender, data) => Notification.Hide();
		_finalCheckup.onOpen += (sender, data) => Notification.Hide();
	}

	#endregion
/*
	#region [Bind groups]

	public void PushBindGroup(string name = "")
	{
		BindGroup group = SetBindGroup(name);
		_bindGroupsStack.Push(group);
	}

	public void PopBindGroup()
	{
		_bindGroupsStack.Pop();

		if (_bindGroupsStack.Count == 0)
		{
			SetBindGroup();
			return;
		}

		BindGroup group = _bindGroupsStack.Peek();
		SetBindGroup(group);
	}

	
	public BindGroup SetBindGroup(string name)
	{
		_currentGroup?.Hide();

		if (name == "") return null;

		foreach(BindGroup group in _bindGroups)
			if (group.Name == name)
			{
				_currentGroup = group;
				break;
			}

		_currentGroup?.Show();

		return _currentGroup;
	}

	public BindGroup SetBindGroup(BindGroup group = null)
	{
		_currentGroup?.Hide();
		_currentGroup = group;
		_currentGroup?.Show();

		return _currentGroup;
	}

	#endregion */
}

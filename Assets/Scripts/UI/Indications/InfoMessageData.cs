using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InfoMessage", menuName = "ScriptableObjects/InfoMessage")]
public class InfoMessageData : ScriptableObject
{
	[TextArea(5, 10)] public string Text;
	public InfoMessage.Type Type;
	public float Delay;
	public float MaxDisplayedTime = -1; // Time before fade out (-1 to never fade out)
}
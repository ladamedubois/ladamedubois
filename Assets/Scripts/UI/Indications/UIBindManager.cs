using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBindManager : MonoBehaviour
{
    [Serializable]
    public class BindGroup
    {
        public string Name;
        public List<BindIndicator> Binds;
        public float OffsetDelay;

        public void Show() => Binds.ForEach(bind => bind.Show(OffsetDelay));
        public void Hide() => Binds.ForEach(bind => bind.Hide());
    }

    [SerializeField] private List<BindGroup> _bindGroups;

    private List<BindIndicator> _bindIndicators;
    private BindGroup _currentGroup;

    private Stack<BindGroup> _bindGroupsStack;

	private void Awake()
	{
		_bindIndicators = new List<BindIndicator>(GetComponentsInChildren<BindIndicator>());
		_bindGroupsStack = new Stack<BindGroup>();
	}

	void Start()
    {
        _bindIndicators.ForEach(bind =>
        {
            bind.SetVisible(false);
        });
    }

	public void PushBindGroup(string name = "")
	{
		BindGroup group = SetBindGroup(name);
		_bindGroupsStack.Push(group);
	}

	public void PopBindGroup()
	{
		if (_bindGroupsStack.Count == 0) return;

		_bindGroupsStack.Pop();

		if (_bindGroupsStack.Count == 0)
		{
			SetBindGroup();
			return;
		}

		BindGroup group = _bindGroupsStack.Peek();
		SetBindGroup(group);
	}

	/**
	 * Sets the current displayed group
	 * calling without parameters hides the current group
	 */
	public BindGroup SetBindGroup(string name)
	{
		_currentGroup?.Hide();

		if (name == "") return null;

		foreach (BindGroup group in _bindGroups)
			if (group.Name == name)
			{
				_currentGroup = group;
				break;
			}

		_currentGroup?.Show();

		return _currentGroup;
	}

	public BindGroup SetBindGroup(BindGroup group = null)
	{
		_currentGroup?.Hide();
		_currentGroup = group;
		_currentGroup?.Show();

		return _currentGroup;
	}
}

using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class InfoMessage : MonoBehaviour
{
	public enum Type
	{
		Blocked, Information, Task
	}

	public event EventHandler<Type> onNotificationAppear;

	[BoxGroup("References")] [SerializeField] protected TextMeshProUGUI _text;
	[BoxGroup("Parameters")] [SerializeField] protected float _appearTime;
	[BoxGroup("Parameters")] [SerializeField] protected float _fadeOutTime;

	protected CanvasGroup _canvasGroup;
	protected RectTransform _rectTransform;
	protected InfoMessageData _currentMessage;

	protected IEnumerator _hideMessageCoroutine;

	public void Show(InfoMessageData message)
	{
		if (message == null) return;

		if (_hideMessageCoroutine != null) StopCoroutine(_hideMessageCoroutine);

		gameObject.SetActive(true);
		DOTween.Kill(_canvasGroup);
		DOTween.Kill(_rectTransform);
		DoAppearAnimation(message);

		_currentMessage = message;

		if (message.MaxDisplayedTime >= 0)
		{
			_hideMessageCoroutine = HideMessageWithDelay(message);
			StartCoroutine(_hideMessageCoroutine);
		}
	}

	protected virtual void DoAppearAnimation(InfoMessageData message)
	{
		_canvasGroup.DOFade(1f, _appearTime).SetDelay(message.Delay).From(0f);

		_rectTransform.DOScaleX(1f, _appearTime / 2f).SetEase(Ease.OutBack).SetDelay(message.Delay).From(0f).OnStart(() =>
		{
			OnNotificationAppeared(message);
		});
	}

	protected void OnNotificationAppeared(InfoMessageData message)
	{
		_text.text = message.Text;
		onNotificationAppear?.Invoke(this, message.Type);
	}

	/**
	 * Hides the current message, if message != null, hides only a specific message if
	 * it's currently displayed
	 */
	public void Hide(InfoMessageData message = null)
	{
		if (message != null && message != _currentMessage) return;

		if (_hideMessageCoroutine != null) StopCoroutine(_hideMessageCoroutine);

		_currentMessage = null;
		_canvasGroup.DOFade(0f, _fadeOutTime).OnComplete(() =>
		{
			SetVisible(false);
		});
	}

	private IEnumerator HideMessageWithDelay(InfoMessageData message)
	{
		yield return new WaitForSeconds(message.MaxDisplayedTime);
		Hide(message);
	}

	private void Awake()
	{
		_canvasGroup = GetComponent<CanvasGroup>();
		_rectTransform = GetComponent<RectTransform>();
	}

	public void SetVisible(bool visible = true)
	{
		gameObject.SetActive(visible);
	}
}

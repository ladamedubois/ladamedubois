using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class BindIndicator : MonoBehaviour
{
	[BoxGroup("References")] [SerializeField] private Image _iconImage;

	[SerializeField] private Sprite _unpressedBindIcon;
	[SerializeField] private Sprite _pressedBindIcon;

	[BoxGroup("Parameters")] [SerializeField] private float _appearTime;
	[BoxGroup("Parameters")] [SerializeField] private bool _autoPress;
	[BoxGroup("Parameters")] [SerializeField] private float _pressTimeInterval;
	[BoxGroup("Parameters")] [SerializeField] private float _pressedTimeDuration;

	public bool AutoPress
	{
		get => _autoPress;
		set
		{
			_autoPress = value;
			if (_autoPress == true)
			{
				if (gameObject.activeSelf) StartPressCycle();
			}
		}
	}

	private IEnumerator _pressCoroutine;
	private CanvasGroup _canvasGroup;
	private RectTransform _rectTransform;

	private void Awake()
	{
		_canvasGroup = GetComponent<CanvasGroup>();
		_rectTransform = GetComponent<RectTransform>();
	}

	public void Show(float delay)
	{
		gameObject.SetActive(true);
		_canvasGroup.DOFade(1f, _appearTime).From(0f).SetDelay(delay);
		_rectTransform.DOScaleX(1f, _appearTime / 2f).SetEase(Ease.OutExpo).From(0f).SetDelay(delay);
		StartPressCycle();
	}

	public void StartPressCycle()
	{
		if (_pressCoroutine != null) StopCoroutine(_pressCoroutine);
		_pressCoroutine = PressCoroutine();
		StartCoroutine(_pressCoroutine);
	}

	public void Hide()
	{
		SetVisible(false);
	}

	public void SetVisible(bool visible = true)
	{
		gameObject.SetActive(visible);

		if (!visible && _pressCoroutine != null) StopCoroutine(_pressCoroutine);
		else if (visible) StartPressCycle();
	}

	private IEnumerator PressCoroutine()
	{
		while (AutoPress)
		{
			_iconImage.sprite = _unpressedBindIcon;
			yield return new WaitForSeconds(_pressTimeInterval);
			_iconImage.sprite = _pressedBindIcon;
			yield return new WaitForSeconds(_pressedTimeDuration);
		}

		_iconImage.sprite = _unpressedBindIcon;
	}
}

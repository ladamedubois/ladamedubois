using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIKeywordsPanel : MonoBehaviour
{
	[BoxGroup("References")] [SerializeField] private CanvasGroup _canvasGroup;
	[BoxGroup("References")] [SerializeField] private List<TextMeshProUGUI> _keywordsSlots;
	[BoxGroup("References")] [SerializeField] private RectTransform _rectTransform;

	private void Awake()
	{
		gameObject.SetActive(false);
		_rectTransform = transform as RectTransform;
	}

	public void Show()
	{
		gameObject.SetActive(true);
		_rectTransform.DOKill();
		_canvasGroup.DOKill();

		_canvasGroup.DOFade(1f, 1f).From(0f).SetDelay(1f);
		_rectTransform.DOPivotY(0.5f, 0.5f).From(Vector2.up).SetDelay(0.8f);
	}

	public void Hide()
	{
		_rectTransform.DOKill();
		_canvasGroup.DOKill();

		_canvasGroup.DOFade(0f, 0.4f).OnComplete(() =>
		{
			gameObject.SetActive(false);
		});

		_rectTransform.DOPivotY(1f, 0.4f).SetEase(Ease.OutQuad);
	}

	public void UpdateData(List<string> hints)
	{
		for (int i = 0; i < _keywordsSlots.Count && i < hints.Count; i++)
		{
			_keywordsSlots[i].text = hints[i];
		}
	}
}

using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHerbarium : MonoBehaviour, IDailyComponent
{
    [BoxGroup("References")] [SerializeField] private UIKeywordsPanel _keywordsPanel;

	private bool _keywordsUpdated = false;

	public void Show()
	{
		if (_keywordsUpdated)
			_keywordsPanel.Show();
	}

	public void Hide()
	{
		_keywordsPanel.Hide();
	}

	public void UpdateData(List<string> hints)
	{
		_keywordsUpdated = true;
		_keywordsPanel.UpdateData(hints);
	}

	public void Reset()
	{
	}

	public void LoadDay(DaysManager.DayData dayData)
	{
		_keywordsUpdated = false;
		Hide();
	}
}

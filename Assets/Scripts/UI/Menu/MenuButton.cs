using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace UI.Menu
{
    public class MenuButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public List<Sprite> flowerSprites;
        public Image image;
        public Button button;
        
        [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _hover;
        [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _click;

        [BoxGroup("References")] [SerializeField] private MenuCallback _menu;

        private void OnValidate()
        {
            if (image == null)
            {
                image = GetComponentInChildren<Image>();
            }

            if (button == null)
            {
                button = GetComponentInChildren<Button>();
            }
        }

        private void Start()
        {
            button.onClick.AddListener(OnButtonClick);

            _menu.onPanelSwitched += (sender, data) =>
            {
                image.gameObject.SetActive(false);
            };
        }

        private void OnButtonClick()
        {
            if(!_click.IsNull)
                FMODUnity.RuntimeManager.PlayOneShot(_click);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if(!_hover.IsNull && button.interactable)
                FMODUnity.RuntimeManager.PlayOneShot(_hover);

            if(flowerSprites.Count == 0 || !button.interactable) return;
            image.sprite = flowerSprites[Random.Range(0, flowerSprites.Count)];
            image.gameObject.SetActive(true);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            image.gameObject.SetActive(false);
        }
    }
}

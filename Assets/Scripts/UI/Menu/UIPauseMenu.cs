using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static UnityEngine.InputSystem.InputAction;

public class UIPauseMenu : MonoBehaviour
{
    public event EventHandler onOpen;
    public event EventHandler onClose;
    public event EventHandler onPanelTransition;
    public event EventHandler onReturnToMenuClicked;

    [SerializeField] private Image _vignette;
    [SerializeField] private GameObject _menuCircle;
    [SerializeField] private GameObject _ringsBackground;
    [SerializeField] private Image _overlay;

    [SerializeField] private OptionPanel _optionsPanelUI;

    [BoxGroup("Panels")] [SerializeField] private RectTransform _pausePanel;
    [BoxGroup("Panels")] [SerializeField] private RectTransform _optionsPanel;

    [BoxGroup("Buttons")] [SerializeField] private GameButton _continueButton;
    [BoxGroup("Buttons")] [SerializeField] private GameButton _optionsButton;
    [BoxGroup("Buttons")] [SerializeField] private GameButton _menuButton;
    [BoxGroup("Buttons")] [SerializeField] private GameButton _exitButton;

    [SerializeField] private string MainMenuScene = "Menu";

    [SerializeField] private float _openAnimationTime = 0.5f;

    public MultiLock Lock { get; private set; }
    private MultiLock _transitionLock;

    private PlayerControls _controls;
    private bool _isOpened;

    public UIPauseMenu()
	{
        _isOpened = false;
        Lock = new MultiLock();
        _transitionLock = new MultiLock();
    }

	private void Awake()
	{
        _controls = new PlayerControls();
       
        _continueButton.onClicked += (sender, data) => OnContinueButtonClicked();
        _optionsButton.onClicked += (sender, data) => OnOptionsButtonClicked();
        _menuButton.onClicked += (sender, data) => OnMenuButtonClicked();
        _exitButton.onClicked += (sender, data) => OnExitButtonClicked();
        _controls.Menus.TogglePause.performed += OnPausePressed;

        _optionsPanelUI.onReturnClicked += (sender, data) => OnOptionsReturnClicked();
    }

    private void OnEnable()
    {
        _controls.Enable();
    }

    private void OnDisable()
    {
        _controls.Disable();
    }

    public void Open()
	{
        if (!Lock.IsFree()) return;

        _transitionLock.Set(this);

        _ringsBackground.transform.DOKill();

        _vignette.gameObject.SetActive(true);
        _menuCircle.SetActive(true);

        _pausePanel.anchorMin = Vector2.zero;
        _optionsPanel.anchorMin = Vector2.right;
        _overlay.raycastTarget = true;
        onOpen?.Invoke(this, EventArgs.Empty);

        _overlay.DOFade(0.9f, _openAnimationTime * 1.5f);
        _ringsBackground.transform.DORotate(Vector3.zero, _openAnimationTime * 0.75f).From(Vector3.forward * 135f)
            .OnComplete(() =>
            {
                _ringsBackground.transform.DOLocalRotate(Vector3.forward * 0.8f, 1f, RotateMode.Fast)
                    .SetLoops(-1, LoopType.Incremental).SetRelative(true).SetEase(Ease.Linear);
            });
        _menuCircle.transform.DOScale(Vector3.one, _openAnimationTime * 0.75f).From(Vector3.zero).SetEase(Ease.OutBack, 1.05f);
        _vignette.DOFade(1f, _openAnimationTime)
            .OnComplete(() => 
            {
                _transitionLock.Unset(this); 
            });
    }

    public void Close()
	{
        _transitionLock.Set(this);

        _ringsBackground.transform.DOKill();

        _overlay.DOFade(0f, _openAnimationTime * 0.75f);
        _ringsBackground.transform.DORotate(Vector3.forward * 135f, _openAnimationTime * 0.75f).From(Vector3.zero);
        _menuCircle.transform.DOScale(Vector3.zero, _openAnimationTime * 0.75f).From(Vector3.one).SetEase(Ease.InBack, 1.05f);
        _vignette.DOFade(0f, _openAnimationTime)
            .OnComplete(() =>
            {
                _menuCircle.SetActive(false);
                _vignette.gameObject.SetActive(false);
                _overlay.raycastTarget = false;
                onClose?.Invoke(this, EventArgs.Empty);

                _transitionLock.Unset(this);
            });
	}

    public void OnContinueButtonClicked()
	{
        if (!_transitionLock.IsFree()) return;

        _isOpened = false;
        Close();
	}

    public void OnMenuButtonClicked()
	{
        onReturnToMenuClicked?.Invoke(this, EventArgs.Empty);
        SceneManager.LoadScene(MainMenuScene, LoadSceneMode.Single);
	}

    public void OnExitButtonClicked()
	{
        Application.Quit();
	}

    public void OnPausePressed(CallbackContext ctx)
	{
        if (!_transitionLock.IsFree()) return;

        _isOpened = !_isOpened;
        if (_isOpened) Open();
        else Close();
	}

    public void OnOptionsButtonClicked()
    {
        _pausePanel.DOAnchorMin(Vector2.left, 0.5f).SetEase(Ease.OutQuad);
        _optionsPanel.DOAnchorMin(Vector2.zero, 0.5f).SetEase(Ease.OutQuad);
        onPanelTransition?.Invoke(this, EventArgs.Empty);
    }

    public void OnOptionsReturnClicked()
	{
        _pausePanel.DOAnchorMin(Vector2.zero, 0.5f).SetEase(Ease.OutQuad);
        _optionsPanel.DOAnchorMin(Vector2.right, 0.5f).SetEase(Ease.OutQuad);
        onPanelTransition?.Invoke(this, EventArgs.Empty);
    }
}

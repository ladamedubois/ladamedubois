using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI.Menu
{
    public class MenuCallback : SoundManager
    {
        public event EventHandler onPanelSwitched;

        [BoxGroup("Sound")] [SerializeField] private FMODUnity.EventReference _ambEvent;
        [BoxGroup("Sound")] [SerializeField] private FMODUnity.EventReference _musicEvent;
        [BoxGroup("Sound")] [SerializeField] private FMODUnity.EventReference _panelSwapSound;
        [BoxGroup("Sound")] [SerializeField] private GameObject _cuicuiLesNoizosPosition;

        private FMOD.Studio.EventInstance _amb;
        private FMOD.Studio.EventInstance _music;

        [BoxGroup("Panels")] [SerializeField] private RectTransform _mainPanel;
        [BoxGroup("Panels")] [SerializeField] private RectTransform _optionsPanel;
        [BoxGroup("Panels")] [SerializeField] private RectTransform _creditsPanel;

        [BoxGroup("Panels")] [SerializeField] private GameButton _optionsReturn;
        [BoxGroup("Panels")] [SerializeField] private GameButton _creditsReturn;

        [BoxGroup("Layouts")] [SerializeField] private RectTransform _optionsLayout;
        [BoxGroup("Layouts")] [SerializeField] private RectTransform _creditsLayout;

        [BoxGroup("References")] [SerializeField] private TransitionFader _transition;

        public float PanelTransitionTime = 0.4f;

        private MultiLock TransitionLock;

        public MenuCallback()
		{
            TransitionLock = new MultiLock();
		}

		private void Awake()
		{
            _optionsReturn.onClicked += (sender, data) => OnReturnClicked();
            _creditsReturn.onClicked += (sender, data) => OnReturnClicked();
            onPanelSwitched += (sender, data) => PlaySound(_panelSwapSound);
            _transition.onTransitionEnd += OnTransitionEnd;
        }

		private void Start()
		{
            _optionsPanel.gameObject.SetActive(false);
            _creditsPanel.gameObject.SetActive(false);

            _amb = PlaySoundStoppable(_ambEvent, true, _cuicuiLesNoizosPosition);
            _music = PlaySoundStoppable(_musicEvent);
            
            TransitionLock.Unlock();
            TransitionLock.Set(_transition);
            _transition.DoFromBlack(0.5f, "OpenMenu", 1f);
        }

        public void QuitGame()
        {
            Application.Quit();
        } 

        public void ChangeScene(string name)
        {
            if (name == "FabreIntroduction")
			{
                StopSound(_amb);
                StopSound(_music);

                TransitionLock.Set(_transition);
                _transition.DoFromTransparent(1f, "StartGame", 0.5f);
                return;
            }

            SceneManager.LoadScene(name, LoadSceneMode.Single);
        }

        public void OpenOptions()
		{
            if (!TransitionLock.IsFree()) return;

            TransitionLock.Set(this);

            _optionsPanel.gameObject.SetActive(true);
            LayoutRebuilder.ForceRebuildLayoutImmediate(_optionsLayout);

            DOTween.Sequence()
            .Append(
                _mainPanel.DOLocalMoveX(700, PanelTransitionTime)
            )
            .Append(
                _optionsPanel.DOLocalMoveX(0, PanelTransitionTime)
                .OnComplete(() =>
                {
                    _mainPanel.gameObject.SetActive(false);

                    TransitionLock.Unset(this);
                })
            );

            onPanelSwitched?.Invoke(this, EventArgs.Empty);
        }

        public void OpenCredits()
		{
            if (!TransitionLock.IsFree()) return;

            TransitionLock.Set(this);

            _creditsPanel.gameObject.SetActive(true);
            LayoutRebuilder.ForceRebuildLayoutImmediate(_creditsLayout);

            DOTween.Sequence()
            .Append(
                _mainPanel.DOLocalMoveX(700, PanelTransitionTime)
            )
            .Append(
                _creditsPanel.DOLocalMoveX(0, PanelTransitionTime)
                .OnComplete(() =>
                {
                    _mainPanel.gameObject.SetActive(false);

                    TransitionLock.Unset(this);
                })
            );

            onPanelSwitched?.Invoke(this, EventArgs.Empty);
        }

        public void OnReturnClicked()
		{
            if (!TransitionLock.IsFree()) return;

            TransitionLock.Set(this);

            _mainPanel.gameObject.SetActive(true);

            DOTween.Sequence()
            .Append(
                _creditsPanel.DOLocalMoveX(700, PanelTransitionTime)
            )
            .Join(
                _optionsPanel.DOLocalMoveX(700, PanelTransitionTime)
            )
            .Append(
                _mainPanel.DOLocalMoveX(0, PanelTransitionTime)
                .OnComplete(() =>
                {
                    _creditsPanel.gameObject.SetActive(false);
                    _optionsPanel.gameObject.SetActive(false);

                    TransitionLock.Unset(this);
                })
            );

            onPanelSwitched?.Invoke(this, EventArgs.Empty);
        }

        private void OnTransitionEnd(object sender, string name)
		{
            if (name == "OpenMenu")
			{
                TransitionLock.Unset(_transition);
            }
            else if (name == "StartGame")
			{
                SceneManager.LoadScene("FabreIntroduction", LoadSceneMode.Single);
            }
		}
    }
}

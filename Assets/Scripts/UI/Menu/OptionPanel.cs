using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionPanel : SoundManager
{
    public event EventHandler onReturnClicked;

    [BoxGroup("Video")] [SerializeField] private Toggle _fullScreen;

    [BoxGroup("Audio")] [SerializeField] private Slider _generalVolume;
    [BoxGroup("Audio")] [SerializeField] private Slider _musicVolume;
    [BoxGroup("Audio")] [SerializeField] private Slider _dialogsVolume;
    [BoxGroup("Audio")] [SerializeField] private Slider _sfxVolume;
    [BoxGroup("Audio")] [SerializeField] private Slider _ambVolume;
    [BoxGroup("Audio")] [SerializeField] private Toggle _monoSound;

    [SerializeField] GameButton _resetButton;
    [SerializeField] GameButton _returnButton;

    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _monoStart;
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _monoStop;

    public string GeneralVCAPath = "vca:/master_vol_vca";
    public string MusicVCAPath = "vca:/mus_vol_vca";
    public string DialogsVCAPath = "vca:/vo_vol_vca";
    public string SfxVCAPath = "vca:/sfx_vol_vca";
    public string AmbVCAPath = "vca:/amb_vol_vca";

    private FMOD.Studio.VCA _generalVCA;
    private FMOD.Studio.VCA _musicVCA;
    private FMOD.Studio.VCA _dialogsVCA;
    private FMOD.Studio.VCA _sfxVCA;
    private FMOD.Studio.VCA _ambVCA;
    
	private void Awake()
    {
        _generalVCA = FMODUnity.RuntimeManager.GetVCA(GeneralVCAPath);
        _musicVCA = FMODUnity.RuntimeManager.GetVCA(MusicVCAPath);
        _dialogsVCA = FMODUnity.RuntimeManager.GetVCA(DialogsVCAPath);
        _sfxVCA = FMODUnity.RuntimeManager.GetVCA(SfxVCAPath);
        _ambVCA = FMODUnity.RuntimeManager.GetVCA(AmbVCAPath);

        LoadOptions();
    }

    private void Start()
	{        
        _resetButton.onClicked += (sender, data) => ResetOptions();
        _returnButton.onClicked += (sender, data) => onReturnClicked?.Invoke(this, EventArgs.Empty);
    }

	public void SetFullscreen(bool value)
	{
        PlayerPrefs.SetInt("Fullscreen", value ? 1 : 0);
        Screen.fullScreen = value;
        if (value == false)
		{
            Screen.SetResolution(1280, 720, false);
		}
    }

    public void SetGeneralVolume(float value)
	{
        PlayerPrefs.SetFloat("GeneralVolume", value);
        _generalVCA.setVolume(value);
    }

    public void SetMusicVolume(float value)
    {
        PlayerPrefs.SetFloat("MusicVolume", value);
        _musicVCA.setVolume(value);
    }

    public void SetDialogsVolume(float value)
    {
        PlayerPrefs.SetFloat("DialogsVolume", value);
        _dialogsVCA.setVolume(value);
    }

    public void SetSfxVolume(float value)
    {
        PlayerPrefs.SetFloat("SfxVolume", value);
        _sfxVCA.setVolume(value);
    }

    public void SetAmbVolume(float value)
    {
        PlayerPrefs.SetFloat("AmbVolume", value);
        _ambVCA.setVolume(value);
    }

    public void SetMono(bool value)
    {
        PlayerPrefs.SetInt("SoundMono", value ? 1 : 0);

        if (value) PlaySound(_monoStart);
		else PlaySound(_monoStop);
    }

    public void LoadOptions()
    {
        _fullScreen.isOn = PlayerPrefs.GetInt("Fullscreen", 1) == 0 ? false : true;
        _generalVolume.value = PlayerPrefs.GetFloat("GeneralVolume", 1f);
        _musicVolume.value = PlayerPrefs.GetFloat("MusicVolume", 1f);
        _dialogsVolume.value = PlayerPrefs.GetFloat("DialogsVolume", 1f);
        _sfxVolume.value = PlayerPrefs.GetFloat("SfxVolume", 1f);
        _ambVolume.value = PlayerPrefs.GetFloat("AmbVolume", 1f);
        _monoSound.isOn = PlayerPrefs.GetInt("SoundMono", 0) == 0 ? false : true;

        Screen.fullScreen = _fullScreen.isOn;
        _generalVCA.setVolume(_generalVolume.value);
        _musicVCA.setVolume(_musicVolume.value);
        _dialogsVCA.setVolume(_dialogsVolume.value);
        _sfxVCA.setVolume(_sfxVolume.value);
        _ambVCA.setVolume(_ambVolume.value);
        if (_monoSound.isOn) PlaySound(_monoStart);
        else PlaySound(_monoStop);
    }

    public void ResetOptions()
	{
        _fullScreen.isOn = true;
        _generalVolume.value = 1f;
        _musicVolume.value = 1f;
        _dialogsVolume.value = 1f;
        _sfxVolume.value = 1f;
        _ambVolume.value = 1f;
        _monoSound.isOn = false;
    }
}

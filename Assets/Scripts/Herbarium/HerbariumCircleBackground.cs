using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class HerbariumCircleBackground : MonoBehaviour
{
	[BoxGroup("References")] [SerializeField] private Image _image;
	
	[SerializeField] private float _maxOpacity = 1.0f;

	[SerializeField] private float _transitionTime;
	[SerializeField] private Vector3 _zoomOffset = new Vector3(-226f, 76f, 0f);
	[SerializeField] private bool _rotating;
	[SerializeField] private float _rotationSpeed;


	private void Awake()
	{
		_image.color = Color.white - Color.black;
	}

	public void Show()
	{
		transform.DOKill();

		transform.DOLocalRotate(Vector3.zero, _transitionTime).From(Vector3.forward * -40)
			.OnComplete(() =>
			{
				transform.DOLocalRotate(Vector3.forward * _rotationSpeed, 1f, RotateMode.Fast)
					.SetLoops(-1, LoopType.Incremental).SetRelative(true).SetEase(Ease.Linear);
			});
		transform.DOScale(Vector3.one, _transitionTime).From(Vector3.one * 0.8f);

		_image.DOFade(_maxOpacity, _transitionTime).From(0f);
	}

	public void Hide()
	{
		transform.DOKill();
		//transform.DOLocalRotate(Vector3.forward * 180f, _transitionTime * 0.8f);
		transform.DOScale(Vector3.one * 1.8f, _transitionTime * 0.8f);

		_image.DOFade(0f, _transitionTime * 0.6f).From(1f);
	}

	public void ZoomIn()
	{
		transform.DOScale(1.75f, 1.1f);
		transform.DOLocalMoveY(_zoomOffset.y, 1.1f).SetEase(Ease.InOutSine);
	}

	public void ZoomOut()
	{
		transform.DOScale(1f, 0.5f);
		transform.DOLocalMoveY(0f, 0.5f);
	}
}

using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HerbariumPlantLink : MonoBehaviour
{
	[BoxGroup("References")] [SerializeField] private LineRenderer _lineRenderer;

    public void Place(HerbariumPlant origin, HerbariumPlant destination)
	{
		_lineRenderer.SetPosition(0, origin.transform.position);
		_lineRenderer.SetPosition(1, destination.transform.position);
	}
}

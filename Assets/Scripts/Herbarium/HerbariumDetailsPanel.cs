using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class HerbariumDetailsPanel : MonoBehaviour
{
	[BoxGroup("References")] [SerializeField] private TextMeshProUGUI _nameLabel;
	[BoxGroup("References")] [SerializeField] private TextMeshProUGUI _descriptionLabel;

	public float DeployInitialDelay = 0.2f;
	public float DeployDelay = 0.1f;
	public float DeployTime = 0.4f;
	
	private CanvasGroup _canvasGroup;

	private LTDescr _alphaTween, _rotateTween;

	private void Awake()
	{
		_canvasGroup = GetComponent<CanvasGroup>();
		_canvasGroup.alpha = 0;
	}

	public void UpdateEffects(PlantEffectData data)
	{
		_nameLabel.text = data.DisplayName;
		_descriptionLabel.text = data.Description;
	}

	public void Show(int index)
	{
		float delay = DeployInitialDelay + DeployDelay * index;
		
		_canvasGroup.DOKill();
		_canvasGroup.DOFade(1f, DeployTime).From(0f).SetDelay(delay);
	}

	public void Hide()
	{
		_canvasGroup.DOKill();
		_canvasGroup.DOFade(0f, DeployTime / 2);
	}
}

using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class HerbariumCursor : MonoBehaviour
{
    [BoxGroup("References")] [SerializeField] private Herbarium _herbarium;
	[BoxGroup("References")] [SerializeField] private Image _image;
	[BoxGroup("References")] [SerializeField] private Canvas _canvas;
	[BoxGroup("References")] [SerializeField] private UIPauseMenu _pause;

	private bool _snapped;
	private bool _hiding;
	private bool _hidden;

	private void Awake()
	{
		_herbarium.onPlantHover += OnCursorEnterPlant;
		_herbarium.onCursorExitPlant += OnCursorExitPlant;
		_herbarium.onFinishOpen += OnOpenHerbarium;
		_herbarium.onClose += OnCloseHerbarium;
/*
		_pause.onOpen += (sender, data) => { if (!_hidden) Hide(false); };
		_pause.onClose += (sender, data) => { if (!_hidden) Show(false); };
*/
		_herbarium.onPlantSelected += (sender, data) => Hide();
		_herbarium.onPlantFinishUnselect += (sender, data) => Show();

		gameObject.SetActive(false);
		_hidden = false;
	}

	private void Update()
	{
		if (_snapped) return;

		Vector2 mousePos = Mouse.current.position.ReadValue();

		transform.DOMove(GetCanvasPos(mousePos), 0.1f);
	}

	private void OnOpenHerbarium(object sender, EventArgs args)
	{
		_snapped = false;
		_hiding = false;
		_image.material.SetFloat("_Select", 0.0f);
		
		Show();
	}

	private void OnCloseHerbarium(object sender, EventArgs args)
	{
		Hide();
	}

	private void OnCursorEnterPlant(object sender, HerbariumPlant plant)
	{
		if (_hiding) return;

		_snapped = true;

		Vector3 screenPos = _canvas.worldCamera.WorldToScreenPoint(plant.transform.position);
		transform.DOMove(GetCanvasPos(screenPos), 0.1f).OnComplete(() =>
		{
			transform.DOScale(1.1f, 0.2f).SetEase(Ease.OutFlash, 2, 0).From(1.0f);
		});

		_image.material.DOFloat(1.0f, "_Select", 0.4f).SetEase(Ease.OutBack);
	}

	private void OnCursorExitPlant(object sender, HerbariumPlant plant)
	{
		if (_hiding) return;

		_snapped = false;
		transform.DOKill();
		_image.material.DOKill();

		_image.material.DOFloat(0.0f, "_Select", 0.4f);
	}

	private Vector3 GetCanvasPos(Vector2 pos)
	{
		Vector2 rectPos;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(_canvas.transform as RectTransform, pos, _canvas.worldCamera, out rectPos);
		return _canvas.transform.TransformPoint(rectPos);
	}

	private void Show(bool markHidden = true)
	{
		Vector2 mousePos = Mouse.current.position.ReadValue();

		gameObject.SetActive(true);
		transform.position = GetCanvasPos(mousePos);
		transform.DOKill();
		transform.DOScale(1f, 0.2f).SetEase(Ease.OutBack).From(0f);
		Cursor.visible = false;
		if (markHidden) _hidden = false;
	}

	private void Hide(bool markHidden = true)
	{
		_hiding = true;
		transform.DOKill();
		transform.DOScale(0f, 0.2f).OnComplete(() =>
		{
			gameObject.SetActive(false);
			_hiding = false;
			if (markHidden) _hidden = true;
		});

		Cursor.visible = true;
	}
}

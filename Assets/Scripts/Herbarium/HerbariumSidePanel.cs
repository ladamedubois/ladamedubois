using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class HerbariumSidePanel : MonoBehaviour
{
	public event EventHandler onStartDeploy; 

	[BoxGroup("References")] [SerializeField] private TextMeshProUGUI _effectLabel;

	public bool DeployToLeft = false;
	public float DeployDelay = 0.5f;
	public float DeployTime = 0.4f;
	public float DeployAnimationDistance = 0.5f;
	
	private RectTransform _rectTransform;
	private Vector3 _initialPosition;
	private Vector3 _deployStartPosition;
	private CanvasGroup _canvasGroup;

	private bool _deployed;
	private IEnumerator _deployCoroutine;

	private void Awake()
	{
		_rectTransform = GetComponent<RectTransform>();
		_canvasGroup = GetComponent<CanvasGroup>();

		_initialPosition = _rectTransform.localPosition;
		_deployStartPosition = _initialPosition + Vector3.left * DeployAnimationDistance;
		_canvasGroup.alpha = 0;
		_deployed = false;
	}

	public void UpdateEffects(PlantData data)
	{
		_effectLabel.text = "";

		for (int i = 0; i < data.Effects.Count; i++)
		{
			_effectLabel.text += data.Effects[i].DisplayName;
			if (i < data.Effects.Count - 1)
				_effectLabel.text += "\n�\n";
		}
		/*
		for (int i = 0; i < _effectLabels.Count; i++)
		{
			TextMeshProUGUI label = _effectLabels[i];

			if (i < data.Effects.Count)
			{
				label.text = data.Effects[i].DisplayName;
			}
			else
			{
				label.text = "";
			}
		}*/
	}

	public void Show(bool delayed = true)
	{
		if (_deployed) return;

		if (_deployCoroutine != null) StopCoroutine(_deployCoroutine);
		_deployCoroutine = ShowWithDelay(delayed ? DeployDelay : 0);
		StartCoroutine(_deployCoroutine);
	}

	public void Hide()
	{
		if (_deployCoroutine != null) StopCoroutine(_deployCoroutine);

		if (!_deployed) return;
		_deployed = false;
		
		_canvasGroup.DOKill();
		transform.DOKill();

		_canvasGroup.DOFade(0f, DeployTime * 0.5f);
		transform.DOLocalMove(_deployStartPosition, DeployTime * 0.5f);
	}

	private IEnumerator ShowWithDelay(float delay)
	{
		yield return new WaitForSeconds(delay);

		onStartDeploy?.Invoke(this, EventArgs.Empty);
		_deployed = true;

		_canvasGroup.DOKill();
		transform.DOKill();

		_canvasGroup.DOFade(1f, DeployTime);
		transform.DOLocalMove(_initialPosition, DeployTime).From(_deployStartPosition);
	}
}

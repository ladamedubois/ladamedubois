using Cinemachine;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using static UnityEngine.InputSystem.InputAction;

public class Herbarium : MonoBehaviour, IDailyComponent, ITutorialLock
{
	public event EventHandler onOpen;
	public event EventHandler onClose;
	public event EventHandler onFinishOpen;
	public event EventHandler onFinishClose;

	public event EventHandler<HerbariumPlant> onPlantSelected;
	public event EventHandler<HerbariumPlant> onPlantUnselected;
	public event EventHandler onPlantFinishUnselect;
	public event EventHandler<HerbariumPlant> onPlantHover;

	public event EventHandler<HerbariumPlant> onCursorEnterPlant;
	public event EventHandler<HerbariumPlant> onCursorExitPlant;

	[BoxGroup("References")] [SerializeField] private Camera _camera;
	[BoxGroup("References")] [SerializeField] private GameObject _closeCMCamera;
	[BoxGroup("References")] [SerializeField] private GameObject _startViewCMCamera;
	[BoxGroup("References")] [SerializeField] private GameObject _globalCMCamera;
	[BoxGroup("References")] [SerializeField] private List<HerbariumPlant> _plants;
	[BoxGroup("References")] [SerializeField] private List<HerbariumCircleBackground> _circleBackgrounds;
	[BoxGroup("References")] [SerializeField] private TransitionFader _transition;

	[SerializeField] private LayerMask _plantLayer;
	[SerializeField] private Vector3  _closeCameraOffset;

	private HerbariumPlant _currentHoverTarget;
	private HerbariumPlant _currentSelected;

	private PlayerControls _controls;

	private bool _zoomLocked = false;
	private MultiLock _transitionLock;
	private bool _selecting;

	public MultiLock Lock { get; private set; }
	public bool DailyUsed { get; private set; }
	public bool IsOpened { get => _globalCMCamera.activeSelf; }

	private void Awake()
	{
		Lock = new MultiLock();
		_transitionLock = new MultiLock();
		DailyUsed = false;

		_controls = new PlayerControls();
		_controls.Herbarium.TogglePlantDetails.performed += OnTogglePlantDetails;

		foreach(HerbariumPlant plant in _plants)
		{
			plant.Canvas.worldCamera = _camera;
			onPlantSelected += plant.OnHerbariumSelected;
			onPlantUnselected += plant.OnHerbariumUnselected;
			plant.onHover += OnPlantHoverWithDelay;
		}
		/*
		foreach(HerbariumCircleBackground circle in _circleBackgrounds)
		{
			circle.FillCameraSprite.Camera = _camera;
		}*/

		CinemachineBlendNotifier _blendNotifier = _camera.GetComponent<CinemachineBlendNotifier>();
		_blendNotifier.onStartBlend += (sender, data) => _zoomLocked = true;
		_blendNotifier.onFinishBlend += (sender, data) => _zoomLocked = false;

		_blendNotifier.onFinishBlend += (sender, data) =>
		{
			if (!_startViewCMCamera.activeSelf && !_closeCMCamera.activeSelf && _globalCMCamera.active && _selecting)
			{
				_selecting = false;
				onPlantFinishUnselect?.Invoke(this, EventArgs.Empty);
			}
		};

		_transition.onTransitionStill += OnMidFaderTransition;
		_transition.onTransitionEnd += OnFaderTransitionEnd;

	}

	private void OnEnable()
	{
		_controls.Enable();
	}

	private void OnDisable()
	{
		_controls.Disable();
	}

	private void Update()
	{
		if (!Lock.IsFree()) return;

		Ray _mouseRay = _camera.ScreenPointToRay(Mouse.current.position.ReadValue());

		RaycastHit hit;

		HerbariumPlant target = null;

		if (Physics.Raycast(_mouseRay, out hit, 100, _plantLayer))
		{
			target = hit.collider.GetComponent<HerbariumPlant>();
		}

		if (target != _currentHoverTarget)
		{
			if (_currentHoverTarget != null) {
				_currentHoverTarget.CursorExit();
				onCursorExitPlant?.Invoke(this, _currentHoverTarget);
			}

			_currentHoverTarget = target;

			if (_currentHoverTarget != null)
			{
				_currentHoverTarget.CursorEnter();
				onCursorEnterPlant?.Invoke(this, _currentHoverTarget);
			}
		}
	}

	private void OnPlantHoverWithDelay(object sender, EventArgs data)
	{
		if (_zoomLocked || !Lock.IsFree()) return;

		HerbariumPlant plant = sender as HerbariumPlant;
		if (plant == null) return;

		onPlantHover?.Invoke(this, _currentHoverTarget);
	}

	private void OnTogglePlantDetails(CallbackContext ctx)
	{
		if (_zoomLocked) return;
		if (!_transitionLock.IsFree()) return;
		if (!Lock.IsFree()) return;

		if (_currentSelected == null) // Zoom on plant
		{
			if (_currentHoverTarget == null) return;

			_currentHoverTarget.Click();

			ZoomOnSelection();
		}
		else // Zoom out
		{
			ResetView(true);
		}
	}

	private void ZoomOnSelection()
	{
		Vector3 selectedPosition = _currentHoverTarget.transform.position + _closeCameraOffset;

		_closeCMCamera.transform.position = selectedPosition;
		_closeCMCamera.SetActive(true);

		_currentSelected = _currentHoverTarget;
		_selecting = true;
		onPlantSelected?.Invoke(this, _currentSelected);

		_circleBackgrounds.ForEach(c => c.ZoomIn());
	}

	private void ResetView(bool animate = false)
	{
		_closeCMCamera.SetActive(false);
		if (_currentSelected != null) onPlantUnselected?.Invoke(this, _currentSelected);
		_currentSelected = null;

		_circleBackgrounds.ForEach(c => c.ZoomOut());
	}

	public void LoadDay(DaysManager.DayData dayData)
	{
		DailyUsed = false;

		foreach (HerbariumPlant plant in _plants)
		{
			plant.SetAvailable(dayData.DailyMission.PlantsAvailable.Contains(plant.Data));
		}
	}

	public void Reset()
	{
	}

	public void Toggle()
	{
		if (_zoomLocked) return;
		if (!_transitionLock.IsFree()) return;
		if (!Lock.IsFree()) return;

		bool newState = !_globalCMCamera.activeSelf;
		
		if (newState)
		{
			_selecting = false;
			_transition.DoTransition(0.5f, 0f, "HerbariumOpen");
			_transitionLock.Set(this);

			onOpen?.Invoke(this, EventArgs.Empty);
			_circleBackgrounds.ForEach(c => c.Show());
		}
		else
		{
			_circleBackgrounds.ForEach(c => c.Hide());
			Close();
			Cursor.visible = true; // For safety
		}
	}

	public void Close()
	{
		ResetView();

		_startViewCMCamera.SetActive(true);
		_transition.StopTransition();
		_transition.DoTransition(0.5f, 0f, "HerbariumClose");
		_transitionLock.Set(this);

		onClose?.Invoke(this, EventArgs.Empty);
		DailyUsed = true;
	}

	private void OnMidFaderTransition(object sender, string name)
	{
		if (name == "HerbariumOpen")
		{
			_camera.orthographic = false;
			_startViewCMCamera.SetActive(true);
			_globalCMCamera.SetActive(true);
			StartCoroutine(MoveCameraDown());
		}
		else if (name == "HerbariumClose")
		{
			_camera.orthographic = true;
			_startViewCMCamera.SetActive(false);
			_globalCMCamera.SetActive(false);
		}
	}

	private void OnFaderTransitionEnd(object sender, string name)
	{
		if (name == "HerbariumOpen" || name == "HerbariumClose")
		{
			_transitionLock.Unset(this);
			if (name == "HerbariumOpen") onFinishOpen?.Invoke(this, EventArgs.Empty);
			if (name == "HerbariumClose") onFinishClose?.Invoke(this, EventArgs.Empty);
		}
	}

	private IEnumerator MoveCameraDown()
	{
		yield return null;
		_startViewCMCamera.SetActive(false);
	}

	public void TutorialLock(TutorialManager tutorial)
	{
		Lock.Set(tutorial);
	}

	public void TutorialUnlock(TutorialManager tutorial)
	{
		Lock.Unset(tutorial);
	}
}

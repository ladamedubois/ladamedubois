using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HerbariumPlant : MonoBehaviour
{
	public event EventHandler onCursorEnter;
	public event EventHandler onCursorExit;
	public event EventHandler onClick;
	public event EventHandler onHover; // Takes the delay in account

	/** 
     * Visual links between plants in the herbarium
     * Only need to register the link in one direction
     */
	[BoxGroup("References")] [SerializeField] private List<HerbariumPlant> _visualLinks;
    [BoxGroup("References")] [SerializeField] private GameObject _plantLinkPrefab;

	[BoxGroup("References")] [SerializeField] private Image _plantImage;
	[BoxGroup("References")] [SerializeField] private Image _backgroundImage;
	[BoxGroup("References")] [SerializeField] private TextMeshProUGUI _plantNameLabel;
	[BoxGroup("References")] [SerializeField] private HerbariumSidePanel _sidePanel;
	[BoxGroup("References")] [SerializeField] private List<HerbariumDetailsPanel> _detailsPanels;
	[BoxGroup("References")] [SerializeField] private CanvasGroup _detailsPanel;
	[BoxGroup("References")] [SerializeField] public Canvas Canvas;

	[SerializeField] [OnValueChanged("UpdatePlant")] public PlantData Data;

	private bool _selected;
	private bool _anyPlantSelected;

	public bool Available { get; private set; }

	private void Start()
	{
		UpdatePlant();

		foreach (HerbariumPlant plant in _visualLinks)
		{
			HerbariumPlantLink link = Instantiate(_plantLinkPrefab, transform).GetComponent<HerbariumPlantLink>();
			link.Place(this, plant);
		}

		_sidePanel.onStartDeploy += (sender, data) => onHover?.Invoke(this, EventArgs.Empty);
		_detailsPanel.alpha = 0f;
	}

	private void UpdatePlant()
	{
		_plantImage.sprite = Data.HerbariumImage;
		_plantNameLabel.text = Data.CommonName;

		_sidePanel.UpdateEffects(Data);
		for (int i = 0; i < _detailsPanels.Count; i++)
		{
			bool valid = i < Data.Effects.Count;
			_detailsPanels[i].gameObject.SetActive(valid);
			if (valid) _detailsPanels[i].UpdateEffects(Data.Effects[i]);
		}
	}

	public void Click()
	{
		onClick?.Invoke(this, EventArgs.Empty);
	}

	public void CursorEnter()
	{
		if (_selected || _anyPlantSelected) return;

		_sidePanel.Show();
		onCursorEnter?.Invoke(this, EventArgs.Empty);
	}

	public void CursorExit()
	{
		_sidePanel.Hide();
		onCursorExit?.Invoke(this, EventArgs.Empty);
	}

	public void SetAvailable(bool available)
	{
		Available = available;
		Color color = Color.white;
		color.a = available ? 1f : 0.5f;
		_plantImage.color = color;
		color.a = available ? 1f : 0.1f;
		_backgroundImage.color = color;
	}

	private void ShowDetails()
	{
		LayoutRebuilder.ForceRebuildLayoutImmediate(_detailsPanel.transform as RectTransform);

		_detailsPanel.DOKill();
		_detailsPanel.DOFade(1f, 1.0f).From(0f).SetDelay(0.6f);
		_detailsPanel.transform.DORotate(Vector3.zero, 0.6f).From(Vector3.up * 60f).SetDelay(0.6f);

		for (int i = 0; i < _detailsPanels.Count; i++)
		{
			if (!_detailsPanels[i].gameObject.activeSelf) continue;
			_detailsPanels[i].Show(i);
		}
	}

	private void HideDetails()
	{
		_detailsPanel.DOKill();
		_detailsPanel.DOFade(0f, 0.35f);

		_detailsPanels.ForEach(panel =>
		{
			if (!panel.gameObject.activeSelf) return;
			panel.Hide();
		});
	}

	public void OnHerbariumSelected(object sender, HerbariumPlant plant)
	{
		_anyPlantSelected = true;
		_sidePanel.Hide();
		if (plant == this) {
			_selected = true;
			ShowDetails();
		}
	}

	public void OnHerbariumUnselected(object sender, HerbariumPlant plant)
	{
		_anyPlantSelected = false;
		_selected = false;
		if (plant == this) {
			HideDetails();
		}
	}
}

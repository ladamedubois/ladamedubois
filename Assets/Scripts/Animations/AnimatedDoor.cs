using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedDoor : MonoBehaviour
{
	public event EventHandler onOpen;
	public event EventHandler onClose;

	[BoxGroup("Animated Door")] [SerializeField] private GameObject _rotatingPart;

	[BoxGroup("Animated Door")] [SerializeField] private Vector3 _rotationOpened;
	[BoxGroup("Animated Door")] [SerializeField] private Vector3 _rotationClosed;
	[BoxGroup("Animated Door")] [SerializeField] private float _transitionTime;

	public void Open()
	{
		_rotatingPart.transform.DOLocalRotateQuaternion(Quaternion.Euler(_rotationOpened), _transitionTime);
		onOpen?.Invoke(this, EventArgs.Empty);
	}

	public void Close()
	{
		_rotatingPart.transform.DOLocalRotateQuaternion(Quaternion.Euler(_rotationClosed), _transitionTime)
			.SetEase(Ease.InQuad);
		onClose?.Invoke(this, EventArgs.Empty);
	}

	public void SetOpen(bool opened = true)
	{
		_rotatingPart.transform.localRotation = Quaternion.Euler(opened ? _rotationOpened : _rotationClosed);
	}
}

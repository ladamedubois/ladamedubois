using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.VFX;

public class CreditsEffectsManager : MonoBehaviour
{
    [SerializeField] private Image _endingIllustration;
    [SerializeField] private VisualEffect _embers;

    private void Start()
	{
        _endingIllustration.material.DOFloat(1f, "_FadeAmount", 3f).From(0f);
    }

	public void OnShowTitle()
    {
        _endingIllustration.material.DOFloat(0.7f, "_FadeAmount", 5f);
    }

    public void OnShowCredits()
	{
        _endingIllustration.material.DOFloat(0.3f, "_FadeAmount", 10f);
    }

    public void OnCreditsFinished()
	{
        _endingIllustration.material.DOFloat(0.0f, "_FadeAmount", 8f);
        _embers.Stop();
    }

    public void ReturnToMenu()
	{
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
	}
}

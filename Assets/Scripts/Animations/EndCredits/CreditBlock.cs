using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Timeline;

[RequireComponent(typeof(CanvasGroup))]
public class CreditBlock : MonoBehaviour, ITimeControl
{
	public UnityEvent onShow;
	public UnityEvent onHide;

	[SerializeField] private float _showTime = 0.5f;
	[SerializeField] private float _hideTime = 1f;

	[SerializeField] private CanvasGroup _canvasGroup;

	protected void Start()
	{
		gameObject.SetActive(true);
		_canvasGroup.alpha = 0f;
	}

	void ITimeControl.OnControlTimeStart()
	{
		onShow?.Invoke();
	}

	void ITimeControl.OnControlTimeStop()
	{
		onHide?.Invoke();
	}

	void ITimeControl.SetTime(double time) { }

	public virtual void OnShow()
	{
		_canvasGroup.DOFade(1f, _showTime).From(0f);
	}

	public virtual void OnHide()
	{
		_canvasGroup.DOFade(0f, _hideTime).OnComplete(() =>
		{
		});
	}
}

using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Timeline;

[RequireComponent(typeof(RectTransform))]
public class ScrollingCreditBlock : CreditBlock
{
	[SerializeField] private float _scrollTime = 8f;

	[SerializeField] private RectTransform _rectTransform;

	public override void OnShow()
	{
		base.OnShow();
		_rectTransform.DOAnchorMax(Vector2.one, _scrollTime)
			.From(Vector2.right)
			.SetEase(Ease.Linear);

		_rectTransform.DOPivotY(-0.1f, _scrollTime)
			.SetEase(Ease.Linear);
	}
}

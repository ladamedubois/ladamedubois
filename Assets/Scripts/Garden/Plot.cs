using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(InteractiveElement))]
public class Plot : MonoBehaviour
{
	public class PlantGatheredData
	{
		public PlantData PlantData;
		public Vector3 Position;
	}

	public event EventHandler<PlantGatheredData> onPlantGathered;
	public event EventHandler<PlantData> onPlantHovered;
	public event EventHandler<PlantData> onPlantHoveredGathered;
	public event EventHandler onAlreadyGathered;
	public event EventHandler onNoInventoryPlace;

	[BoxGroup("References")] [SerializeField] private Transform _plantTransform;
	[BoxGroup("References")] [SerializeField] private TextMeshProUGUI _plantNameLabel;
	[BoxGroup("References")] [SerializeField] public Canvas Canvas;
	[BoxGroup("References")] [SerializeField] private InteractiveElement _interativeElement;

	private GameObject _plantModel;
	private PlantData _currentPlant;
	public PlantData Plant
	{
		get { return _currentPlant; }
	}

	public InteractiveElement Interactive { get => _interativeElement; }

	public MultiLock GatheredLock { get; private set; }
	public MultiLock FullInventoryLock { get; private set; }

	public Plot()
	{
		GatheredLock = new MultiLock();
		FullInventoryLock = new MultiLock();
	}

	private void Awake()
	{	
		_interativeElement.onInteract += (sender, data) =>
		{
			OnTryGather();
		};

		_interativeElement.onCursorEnter += (sender, data) =>
		{
			if (GatheredLock.IsFree())
				onPlantHovered?.Invoke(this, Plant);
			else
				onPlantHoveredGathered?.Invoke(this, Plant);
		};

		// @TODO : Label refactor
		_interativeElement.onCursorEnter += (sender, data) =>
		{
			_plantNameLabel.DOKill();
			_plantNameLabel.gameObject.SetActive(true);
			_plantNameLabel.DOFade(1.0f, 0.4f);
		};

		_interativeElement.onCursorExit += (sender, data) =>
		{
			_plantNameLabel.DOKill();
			_plantNameLabel.DOFade(0.0f, 0.4f).OnComplete(() =>
			{
				_plantNameLabel.gameObject.SetActive(false);
			});
		};
	}

	public void OnTryGather()
	{
		if (!FullInventoryLock.IsFree())
		{
			onNoInventoryPlace?.Invoke(this, EventArgs.Empty);
			return;
		}

		if (!GatheredLock.IsFree())
		{
			onAlreadyGathered?.Invoke(this, EventArgs.Empty);
			return;
		}

		onPlantGathered?.Invoke(this, new PlantGatheredData { PlantData = Plant, Position = _plantTransform.position });
	}

	/**
	 * Binds and initializes the plot with a specific plant
	 */
	public void Bind(PlantData plant)
	{
		_currentPlant = plant;
		GatheredLock.Unlock();
		FullInventoryLock.Unlock();

		if (_plantModel != null) Destroy(_plantModel);
		_plantModel = Instantiate(plant.GardenModel, _plantTransform);

		_plantNameLabel.text = plant.CommonName;
	}
}

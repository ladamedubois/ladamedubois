using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plant : IInventoryItem
{
    public PlantData Type { get; private set; }
    
	public Plant(PlantData type)
	{
		Type = type;
	}

	public Sprite GetImage()
	{
		return Type.HerbariumImage;
	}

	public bool CanRemove() => true;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassRandomizer : MonoBehaviour
{
    [SerializeField] private List<Texture> _texturePool;
	[SerializeField] private List<Renderer> _grassPlanes;

	private void Awake()
	{

		foreach (Renderer renderer in _grassPlanes) {
			int index = Random.Range(0, _texturePool.Count);
			renderer.material.SetTexture("_PlantImage", _texturePool[index]);
		}
	}
}

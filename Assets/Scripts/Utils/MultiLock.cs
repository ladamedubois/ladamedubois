using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Simple lock system
 */
public class MultiLock
{
	public event EventHandler onUnlocked;
	public event EventHandler onLocked;

    private List<object> _locks;

	public MultiLock()
	{
		_locks = new List<object>();
	}

    public void Set(object locker)
	{
		if (_locks.Contains(locker)) return;

		_locks.Add(locker);
		if (_locks.Count == 1)
		{
			onLocked?.Invoke(this, EventArgs.Empty);
		}
	}

	public void Unset(object locker)
	{
		_locks.Remove(locker);
		if (_locks.Count == 0)
		{
			onUnlocked?.Invoke(this, EventArgs.Empty);
		}
	}

	/**
	 * Fully unlocks the object
	 */
	public void Unlock()
	{
		_locks.Clear();
		onUnlocked?.Invoke(this, EventArgs.Empty);
	}
		
	public bool IsFree()
	{
		return _locks.Count == 0;
	}

	public bool IsLockedBy(object locker)
	{
		return _locks.Contains(locker);
	}
}

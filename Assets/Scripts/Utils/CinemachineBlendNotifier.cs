using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CinemachineBrain))]
public class CinemachineBlendNotifier : MonoBehaviour
{
    public event EventHandler onStartBlend;
    public event EventHandler onFinishBlend;

    private CinemachineBrain _brain;
	private bool _lastBlending;

	private void Awake()
	{
		_brain = GetComponent<CinemachineBrain>();
		_lastBlending = false;
	}

	private void Update()
	{
		if (_brain.IsBlending)
		{
			if (!_lastBlending) onStartBlend?.Invoke(this, EventArgs.Empty);
			_lastBlending = true;
		}
		else
		{
			if (_lastBlending) onFinishBlend?.Invoke(this, EventArgs.Empty);
			_lastBlending = false;
		}
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class FillCameraSprite : MonoBehaviour
{
    [SerializeField] private float _distance = 20f;
    [SerializeField] private float _borderOffset = 1f;
    [SerializeField] private SpriteRenderer _sprite;
    
    public Camera Camera { get; set; }

    private float position;
    private RectTransform _rectTransform;

    public float ScaleMultiplier {get; set;}

	private void Awake()
	{
        ScaleMultiplier = 1f;
        _rectTransform = gameObject.GetComponent<RectTransform>();
    }

	private void Start()
	{
        position = (Camera.nearClipPlane + _distance);

        transform.position = Camera.transform.position + Camera.transform.forward * position;
    }

	private void Update()
    {
        float h = Mathf.Tan(Camera.fieldOfView * Mathf.Deg2Rad * 0.5f) * position * 2f * _borderOffset;

        float maxSize = Mathf.Max(h * Camera.aspect, h);

        transform.localScale = (new Vector3(maxSize, maxSize, 0f) / _sprite.sprite.bounds.size.x) * ScaleMultiplier;
    }
}

using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class CraftingStation : MonoBehaviour, IDailyComponent
{
    public event EventHandler onCraftingOpen;
    public event EventHandler onCraftingClose;
    public event EventHandler<Remedy> onRemedyCrafted;

    public event EventHandler onCantUseAlreadyCrafted;
    public event EventHandler onCantUseNotAllIngredients;
    public event EventHandler onCantUseNoHints;

    [BoxGroup("References")] [SerializeField] private InventoryManager _inventory;
    [BoxGroup("References")] [SerializeField] private VisualEffect _feedbackVfx;

    public MultiLock AlreadyCraftedLock { get; private set; }
    public MultiLock AllIngredientsLock { get; private set; }
    public MultiLock NoHintsLock { get; private set; }

    private MissionData _dailyMission;

	public void Reset()
	{
	}

	private void Awake()
	{
        _inventory.onAddItem += (sender, data) =>
        {
            if (_inventory.IsFull()) {
                AllIngredientsLock.Unset(_inventory);
            }
        };

        _inventory.onRemoveItem += (sender, data) =>
        {
            if (!_inventory.IsFull() && !AllIngredientsLock.IsLockedBy(_inventory))
            {
                AllIngredientsLock.Set(_inventory);
            }
        };

        onRemedyCrafted += (sender, data) =>
        {
            _feedbackVfx.Play();
        };
    }

	public void LoadDay(DaysManager.DayData dayData)
	{
        _dailyMission = dayData.DailyMission;

        AlreadyCraftedLock = new MultiLock();
        AllIngredientsLock = new MultiLock();
        NoHintsLock = new MultiLock();

        AllIngredientsLock.Set(_inventory);
    }

    public void OnPrepareClicked()
    {
        Remedy remedy = new Remedy(_dailyMission);

        List<Plant> plants = _inventory.GetPlants();
        foreach(Plant plant in plants)
		{
            remedy.AddComponent(plant.Type);
		}

        _inventory.AddItem(remedy, transform.position);

        onRemedyCrafted?.Invoke(this, remedy);

        AlreadyCraftedLock.Set(this);
        CloseCrafting();
    }

    public void OnCancelClicked()
    {
        CloseCrafting();
    }

	public void OpenCrafting()
	{
        if (!AlreadyCraftedLock.IsFree())
		{
            onCantUseAlreadyCrafted?.Invoke(this, EventArgs.Empty);
            return;
		}

        if (!NoHintsLock.IsFree())
		{
            onCantUseNoHints?.Invoke(this, EventArgs.Empty);
            return;
		}

        if (!AllIngredientsLock.IsFree())
		{
            onCantUseNotAllIngredients?.Invoke(this, EventArgs.Empty);
            return;
		}

        onCraftingOpen?.Invoke(this, EventArgs.Empty);
	}

	public void CloseCrafting()
	{
        onCraftingClose?.Invoke(this, EventArgs.Empty);
    }
}

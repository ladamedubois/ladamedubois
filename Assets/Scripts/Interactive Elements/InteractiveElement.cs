using Sirenix.OdinInspector;
using System;
using UnityEngine;

/**
 * The base component for an interactive element on the scene
 * 
 * Don't forget to include [RequireComponent(typeof(InteractiveElement))] on top of all
 * the other specific interactive elements components
 */
public class InteractiveElement : MonoBehaviour, ITutorialLock
{
	public event EventHandler onInteract;
	public event EventHandler onInteractionBlocked; // Triggered when the interactive element is locked
	public event EventHandler<bool> onCursorEnter; // parameter = false if the object is locked
	public event EventHandler onCursorExit;
	public event EventHandler onClick;

	[BoxGroup("References")] [SerializeField] private Transform _useSpot;

	[Tooltip("Indicates that the player does not needs to reach the element to trigger it's action")]
	public bool InstantReach = false;

	public MultiLock Lock { get; private set; }

	public InteractiveElement()
	{
		Lock = new MultiLock();
	}

	public void CursorEnter()
	{
		onCursorEnter?.Invoke(this, !Lock.IsFree());
	}

	public void CursorExit()
	{
		onCursorExit?.Invoke(this, EventArgs.Empty);
	}

	public void Click()
	{
		onClick?.Invoke(this, EventArgs.Empty);
		if (!InstantReach) return;
		(!Lock.IsFree() ? onInteractionBlocked : onInteract)?.Invoke(this, EventArgs.Empty);
	}

	public void Reached()
	{
		if (InstantReach) return; // Additional security to avoid double interactions
		(!Lock.IsFree() ? onInteractionBlocked : onInteract)?.Invoke(this, EventArgs.Empty);
	}

	public Vector3 GetUseSpot()
	{
		Vector3 position = (_useSpot != null ? _useSpot : gameObject.transform).position;
		position.y = 0; // align the position to ground
		return position;
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.yellow;
		Vector3 position = GetUseSpot();

		Gizmos.DrawWireSphere(position, 0.5f);
		if (_useSpot == null) Gizmos.DrawLine(position, gameObject.transform.position);
	}

	public void TutorialLock(TutorialManager tutorial)
	{
		Lock.Set(tutorial);
	}

	public void TutorialUnlock(TutorialManager tutorial)
	{
		Lock.Unset(tutorial);
	}
}

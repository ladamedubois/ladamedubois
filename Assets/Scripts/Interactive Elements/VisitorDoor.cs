using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InteractiveElement))]
public class VisitorDoor : AnimatedDoor, IDailyComponent
{
	public event EventHandler onVisitorArrived;
	public event EventHandler onCantUseOnOne;
	public event EventHandler onKnock;

	private InteractiveElement _interactiveElement;

	[BoxGroup("References")] [SerializeField] private Renderer _visitorRenderer;
	[BoxGroup("Parameter")] [SerializeField] private AnimationCurve _visitorWalkAnimation;

	public MultiLock VisitorLock { get; private set; }
	[SerializeField] private float _knockDelay;

	private IEnumerator _knockCoroutine;

	private bool _opening;
	public bool Opening { get => _opening; }

	public VisitorDoor()
	{
		VisitorLock = new MultiLock();
	}

	public void LoadDay(DaysManager.DayData dayData)
	{
		_visitorRenderer.material.SetTexture("_VisitorTexture", dayData.DailyMission.Visitor.NeutralDialogImage.texture);
		VisitorLock.Unlock();
		
		_knockCoroutine = KnockDelay();
		StartCoroutine(_knockCoroutine);
	}

	public void Reset()
	{
	}

	private IEnumerator KnockDelay()
	{
		yield return new WaitForSeconds(_knockDelay);
		Knock();
	}

	private void Awake()
	{
		_interactiveElement = GetComponent<InteractiveElement>();
		onOpen += (sender, data) =>
		{
			_opening = true;
			if (_knockCoroutine != null) StopCoroutine(_knockCoroutine);
			DoVisitorArriveAnimation();
		};

		_interactiveElement.onInteract += (sender, data) =>
		{
			if (!VisitorLock.IsFree())
				onCantUseOnOne?.Invoke(this, EventArgs.Empty);
		};

		SetOpen(false);
	}

	private void DoVisitorArriveAnimation()
	{
		_visitorRenderer.gameObject.transform.DOLocalMoveX(-1.1f, 1.0f).From(0f);
		_visitorRenderer.gameObject.transform.DOLocalMoveY(1.2f, 1.0f).From(1.1f).SetEase(Ease.OutFlash, 6);
		_visitorRenderer.material.DOFloat(1.0f, "_Alpha", 1.5f).SetEase(Ease.OutQuad).From(0.0f).OnComplete(() =>
		{
			onVisitorArrived?.Invoke(this, EventArgs.Empty);
			_opening = false;
		});
	}

	public void Knock()
	{
		onKnock?.Invoke(this, EventArgs.Empty);
	}
}

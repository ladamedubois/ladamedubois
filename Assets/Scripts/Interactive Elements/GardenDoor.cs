using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InteractiveElement))]
public class GardenDoor : AnimatedDoor, IDailyComponent
{
	public event EventHandler onStartTravelDoor;
	public event EventHandler onDoorTraveled;
	public event EventHandler onCantUseTooLate;

    [BoxGroup("References")] [SerializeField] private Transform _destinationTransform;
	[BoxGroup("References")] [SerializeField] private GameObject _associatedRoom;
    [BoxGroup("References")] [SerializeField] private GardenDoor _otherSideDoor;
    [BoxGroup("References")] [SerializeField] private Character _character;
	[BoxGroup("References")] [SerializeField] private TransitionFader _transition;

	private InteractiveElement _interactiveElement;

	public MultiLock LateLock { get; private set; }

	public GardenDoor()
	{
		LateLock = new MultiLock();
	}

	private void Awake()
	{
		_interactiveElement = GetComponent<InteractiveElement>();
		_interactiveElement.onInteract += (sender, data) => TravelDoor();
	}

	public Transform GetDestinationTransform() { 
        return _destinationTransform;
    }

	public GameObject GetDestinationRoom()
	{
		return _associatedRoom;
	}

	private void TravelDoor()
	{
		
		if (!LateLock.IsFree())
		{
			onCantUseTooLate?.Invoke(this, EventArgs.Empty);
			return;
		}

		onStartTravelDoor?.Invoke(this, EventArgs.Empty);
		Open();
		
		bool ret = _transition.DoTransition(0.3f, 0.9f, "Garden");
		_transition.onTransitionStill += OnMidTransition;
		_transition.onTransitionStillEnd += OnTransitionStillEnd;
	}

	private void OnMidTransition(object sender, string name)
	{
		if (name != "Garden") return;

		onDoorTraveled?.Invoke(this, EventArgs.Empty);

		_otherSideDoor.GetDestinationRoom().SetActive(true);
		_associatedRoom.SetActive(false);

		_character.Movement.Warp(_otherSideDoor.GetDestinationTransform().position);
		_character.transform.rotation = _otherSideDoor.GetDestinationTransform().rotation;

		_transition.onTransitionStill -= OnMidTransition;
	}

	private void OnTransitionStillEnd(object sender, string name)
	{
		if (name != "Garden") return;

		_otherSideDoor.SetOpen(true);
		_otherSideDoor.Close();

		_transition.onTransitionStillEnd -= OnTransitionStillEnd;
	}

	private void OnDrawGizmos()
	{
		if (_destinationTransform == null) return;

		Gizmos.color = Color.green;
		Vector3 position = _destinationTransform.position;
		Vector3 direction = _destinationTransform.rotation * Vector3.forward;

		Gizmos.DrawWireSphere(position, 0.5f);
		Gizmos.DrawLine(position, position + direction);
	}

	public void Reset()
	{
	}

	public void LoadDay(DaysManager.DayData dayData)
	{
		LateLock.Unlock();
	}
}

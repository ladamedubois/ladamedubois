using DG.Tweening;
using EPOOutline;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InteractiveElement))]
[RequireComponent(typeof(Outlinable))]
public class InteractiveOutline : MonoBehaviour
{
	private InteractiveElement _interactiveElement;
	private Outlinable _outline;

	public float HoverFadeSpeed = 0.2f;
	public float HighlightFadeDuration = 1.5f;
	[ColorUsage(true, true)] public Color HoverColor;
	[ColorUsage(true, true)] public Color HighlightColor;

	private bool _highlighted;
	private bool _hovered;
	private bool _disabled;

	private Tween _highlightAnimation;

	public bool Highlighted { 
		get => _highlighted;
		set {
			_highlighted = value;

			if (_highlighted)
				StartHighlight();
			else
				StopHighlight();
		}
	}

	public bool Disabled
	{
		get => _disabled;
		set
		{
			_disabled = value;
			if (_disabled)
			{
				StopHover();
				Highlighted = false;
				return;
			}
		}
	}

	private void Awake()
	{
		_interactiveElement = GetComponent<InteractiveElement>();
		_outline = GetComponent<Outlinable>();
		
		_interactiveElement.onCursorEnter += OnCursorEnter;
		_interactiveElement.onCursorExit += OnCursorExit;
		_interactiveElement.onClick += OnCursorExit;

		_interactiveElement.Lock.onLocked += (sender, data) => Disabled = true;
		_interactiveElement.Lock.onUnlocked += (sender, data) => Disabled = false;

		_outline.enabled = false;

		Disabled = false;
	}
	
	private void OnEnable()
	{
		CursorController.onTargetingBlocked += OnTargetingBlocked;
	}

	private void OnDisable()
	{
		CursorController.onTargetingBlocked -= OnTargetingBlocked;
	}
	
	private void OnCursorEnter(object sender, bool blocked)
	{
		_hovered = true;

		if (Disabled) return;
		StartHover();
	}

	private void OnCursorExit(object sender, EventArgs data)
	{
		_hovered = false;
		StopHover();
	}
	
	private void OnTargetingBlocked(object sender, EventArgs data) => StopHover();
	
	private void StartHover()
	{
		DOTween.Kill(_outline.FrontParameters);

		_outline.FrontParameters.DOColor(HoverColor, HoverFadeSpeed);
		_outline.FrontParameters.DOFade(1f, HoverFadeSpeed);
	}

	private void StopHover()
	{
		DOTween.Kill(_outline.FrontParameters);

		if (Highlighted)
		{
			StartHighlight();
			return;
		}

		_outline.FrontParameters.DOFade(0f, HoverFadeSpeed);
	}

	private void StartHighlight()
	{
		DOTween.Kill(_outline.FrontParameters);

		_outline.FrontParameters.DOColor(HighlightColor, HoverFadeSpeed);
		_outline.FrontParameters.DOFade(1f, HighlightFadeDuration).OnComplete(() =>
		{
			_highlightAnimation = _outline.FrontParameters.DOFade(0.2f, HighlightFadeDuration)
				.From(1f)
				.SetEase(Ease.InOutSine)
				.SetLoops(-1, LoopType.Yoyo);
		});
	}

	private void StopHighlight()
	{
		DOTween.Kill(_outline.FrontParameters);

		if (_hovered)
		{
			StartHover();
			return;
		}

		_outline.FrontParameters.DOFade(0f, HighlightFadeDuration);
	}
}

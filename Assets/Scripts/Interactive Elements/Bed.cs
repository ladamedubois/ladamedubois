using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InteractiveElement))]
public class Bed : MonoBehaviour, IDailyComponent
{
	public event EventHandler onSleep;
	public event EventHandler onCantSleep;
	public event EventHandler onWakeUp;
	public event EventHandler onGetIn;

	[BoxGroup("References")] [SerializeField] private Transform _wakeTransform;
	[BoxGroup("Parameters")] [SerializeField] private float _wakeUpDelay;
	[BoxGroup("Parameters")] [SerializeField] private float _getInDelay = 1f;

	public MultiLock ClientSatisfyLock { get; private set; }

	private void Awake()
	{
		ClientSatisfyLock = new MultiLock();
		
		GetComponent<InteractiveElement>().onInteract += (sender, data) =>
		{
			if (!CanSleep())
			{
				onCantSleep?.Invoke(this, EventArgs.Empty);
				return;
			}
			onSleep?.Invoke(this, EventArgs.Empty);

			StartCoroutine(GetInDelayed(_getInDelay));
		};
	}

	public bool CanSleep()
	{
		return ClientSatisfyLock.IsFree();
	}

	public Transform GetWakeTransform()
	{
		return _wakeTransform;
	}

	private void OnDrawGizmos()
	{
		if (_wakeTransform == null) return;

		Gizmos.color = Color.blue;
		Vector3 position = _wakeTransform.position;
		Vector3 direction = _wakeTransform.rotation * Vector3.forward;

		Gizmos.DrawWireSphere(position, 0.5f);
		Gizmos.DrawLine(position, position + direction);
	}

	public void Reset()
	{
	}

	public void LoadDay(DaysManager.DayData dayData)
	{
		StartCoroutine(WakeUpDelayed(_wakeUpDelay));
	}

	private IEnumerator GetInDelayed(float delay)
	{
		yield return new WaitForSeconds(delay);
		onGetIn?.Invoke(this, EventArgs.Empty);
	}

	private IEnumerator WakeUpDelayed(float delay)
	{
		yield return new WaitForSeconds(delay);
		onWakeUp?.Invoke(this, EventArgs.Empty);
	}
}

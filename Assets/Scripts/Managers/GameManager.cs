using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/**
 * The goal of the game manager is to link the other managers together without coupling, and to link them
 * to game elements using events
 */
public class GameManager : MonoBehaviour
{
    [BoxGroup("Managers")] [SerializeField] private DaysManager _daysManager;
    [BoxGroup("Managers")] [SerializeField] private DialogManager _dialogManager;
    [BoxGroup("Managers")] [SerializeField] private KeywordManager _keywordManager;
    [BoxGroup("Managers")] [SerializeField] private GardenManager _gardenManager;
    [BoxGroup("Managers")] [SerializeField] private InventoryManager _inventoryManager;
    [BoxGroup("Managers")] [SerializeField] private Herbarium _herbarium;

    [BoxGroup("Elements")] [SerializeField] private Bed _bed;
    [BoxGroup("Elements")] [SerializeField] private VisitorDoor _visitorDoor;
    [BoxGroup("Elements")] [SerializeField] private GardenDoor _gardenDoor;
    [BoxGroup("Elements")] [SerializeField] private InteractiveElement _houseDoor;
    [BoxGroup("Elements")] [SerializeField] private CraftingStation _craftingStation;

    [BoxGroup("UI")] [SerializeField] private UIInventory _inventoryUI;
    [BoxGroup("UI")] [SerializeField] private UIDialog _dialogUI;
    [BoxGroup("UI")] [SerializeField] private UIHerbarium _herbariumUI;
    [BoxGroup("UI")] [SerializeField] private UICrafting _craftingUI;
    [BoxGroup("UI")] [SerializeField] private UIFinalCheckup _finalCheckupUI;
    [BoxGroup("UI")] [SerializeField] private UIIndications _indicatorsUI;
    [BoxGroup("UI")] [SerializeField] private UIBindManager _bindGroupsUI;
    [BoxGroup("UI")] [SerializeField] private UIPauseMenu _pauseMenuUI;

    [SerializeField] private CursorController _cursor;
    [SerializeField] private Character _character;
    [SerializeField] private GameObject _CMGardenCamera;
    [SerializeField] private TransitionFader _transitionFader;

    public bool DebugSpeedUp = false;

	private void Awake()
	{
        if (DebugSpeedUp)
            Time.timeScale = 4.0f; // TODO REMOVE !
        /**
         * Bind all events
         */

        // Day cycle
        _bed.onSleep += (sender, data) => _finalCheckupUI.Open();
        _finalCheckupUI.onClose += (sender, data) => _daysManager.NextDay();
        _daysManager.onNoDayLeft += (sender, data) => SceneManager.LoadScene("Ending", LoadSceneMode.Single);

        //        _daysManager.onDayChange += (sender, data) => _gardenManager.LoadDay(data);

        _daysManager.onDayChange += (sender, data) => _character.InitDay();
        _daysManager.onDayChange += (sender, data) => _transitionFader.DoFromBlack(1.2f, "DayStart", 2.2f);
        _daysManager.onDayChange += (sender, data) => _bed.ClientSatisfyLock.Set(_dialogManager);
        _daysManager.onDayChange += (sender, data) => _gardenManager.LockGathering(_dialogManager);

        // Spawn reward
        _daysManager.onDayEnd += (sender, mission) =>
        {
            GameObject reward = mission.DailyMission.RewardModel;
            if (reward == null) return;
            Instantiate(reward, _dialogManager.transform); // Dialog manager is on the house object
        };

        // Movements
        _cursor.onGroundClick += (sender, point) => _character.Movement.SetTargetPoint(point);
        _cursor.onInteractiveElementClick += (sender, element) => _character.Movement.SetTargetElement(element);
        _cursor.onTargetCancel += (sender, data) => _character.Movement.CancelTarget();
        _cursor.onToggleHerbariumAction += (sender, data) => _herbarium.Toggle();

        // Prevent targetting
        _transitionFader.onTransitionStart += (sender, data) => _cursor.TargetingLock.Set(_transitionFader);
        _transitionFader.onTransitionEnd += (sender, data) => _cursor.TargetingLock.Unset(_transitionFader);
        _visitorDoor.onOpen += (sender, data) => _cursor.TargetingLock.Set(_dialogManager);
        _dialogManager.onDialogOpen += (sender, data) => _cursor.TargetingLock.Set(_dialogManager);
        _dialogManager.onDialogClose += (sender, data) => _cursor.TargetingLock.Unset(_dialogManager);
        _herbarium.onOpen += (sender, data) => _cursor.TargetingLock.Set(_herbarium);
        _herbarium.onClose += (sender, data) => _cursor.TargetingLock.Unset(_herbarium);
        _craftingStation.onCraftingOpen += (sender, data) => _cursor.TargetingLock.Set(_craftingStation);
        _craftingStation.onCraftingClose += (sender, data) => _cursor.TargetingLock.Unset(_craftingStation);
        _finalCheckupUI.onOpen += (sender, data) => _cursor.TargetingLock.Set(_finalCheckupUI);
        _finalCheckupUI.onClose += (sender, data) => _cursor.TargetingLock.Unset(_finalCheckupUI);
        _pauseMenuUI.onOpen += (sender, data) => _cursor.TargetingLock.Set(_pauseMenuUI);
        _pauseMenuUI.onClose += (sender, data) => _cursor.TargetingLock.Unset(_pauseMenuUI);

        // Herbarium
        _visitorDoor.onOpen += (sender, state) => _herbarium.Lock.Set(_dialogManager);
        _dialogManager.onDialogOpen += (sender, state) => _herbarium.Lock.Set(_dialogManager);
        _dialogManager.onDialogClose += (sender, data) => _herbarium.Lock.Unset(_dialogManager);
        _transitionFader.onTransitionStart += (sender, data) => _herbarium.Lock.Set(_transitionFader);
        _transitionFader.onTransitionEnd += (sender, data) => _herbarium.Lock.Unset(_transitionFader);
        _finalCheckupUI.onOpen += (sender, data) => _herbarium.Lock.Set(_finalCheckupUI);
        _finalCheckupUI.onClose += (sender, data) => _herbarium.Lock.Unset(_finalCheckupUI);
        _pauseMenuUI.onOpen += (sender, data) => _herbarium.Lock.Set(_pauseMenuUI);
        _pauseMenuUI.onClose += (sender, data) => _herbarium.Lock.Unset(_pauseMenuUI);
        _craftingStation.onCraftingOpen += (sender, data) => _herbarium.Lock.Set(_craftingStation);
        _craftingStation.onCraftingClose += (sender, data) => _herbarium.Lock.Unset(_craftingStation);
        _transitionFader.onTransitionStart += (sender, data) => _pauseMenuUI.Lock.Set(_transitionFader);
        _transitionFader.onTransitionEnd += (sender, data) => _pauseMenuUI.Lock.Unset(_transitionFader);

        // UI
        _inventoryUI.onItemClicked += (sender, slot) => _inventoryManager.OnItemClicked(slot);
        _inventoryManager.onAddItem += (sender, data) => _inventoryUI.OnAddItem(data);
        _inventoryManager.onClear += (sender, data) => _inventoryUI.OnClear();
        _inventoryManager.onRemoveItem += (sender, data) => _inventoryUI.OnRemoveItem(data);
        _inventoryManager.onStateChange += (sender, state) => _inventoryUI.OnStateChange(state);
        _inventoryManager.onStateChange += (sender, state) => { if (state == InventoryManager.State.Remedy) _dialogManager.OnGotRemedy(); } ;
        _herbarium.onOpen += (sender, data) => _herbariumUI.Show();
        _herbarium.onClose += (sender, data) => _herbariumUI.Hide();
        _inventoryManager.onRemoveItem += (sender, data) => _gardenManager.OnInventoryRemove(data.Item);

        _herbarium.onOpen += (sender, data) => _inventoryUI.Hide();
        _herbarium.onFinishClose += (sender, data) => { 
            if (_dialogManager.AlreadyTalked && _dialogManager.CurrentState != DialogManager.State.Satisfied) _inventoryUI.Show(); 
        };
        _bed.onSleep += (sender, data) => _inventoryUI.Hide();

        _visitorDoor.GetComponent<InteractiveElement>().onInteract += (sender, data) =>
        {
            if (_visitorDoor.Opening) return;

            if (_dialogManager.CurrentState == DialogManager.State.NotStarted)
                _visitorDoor.Open();
            else
                _dialogManager.OpenDialog(); // If door already opened, immediatly open dialog
        };

        _visitorDoor.onVisitorArrived += (sender, data) =>
        {
            if (_dialogManager.CurrentState == DialogManager.State.NotStarted) // If first time talk, wait for visistor to arrive
                _dialogManager.OpenDialog();
        };

        _dialogUI.onArrowPrevClicked += (sender, data) =>
        {
            if (!_dialogUI.DialogBox.TextTransitionLock.IsFree()) return;
            _dialogManager.OnArrowPrevClicked();
        };

        _dialogUI.onArrowNextClicked += (sender, data) =>
        {
            if (!_dialogUI.DialogBox.TextTransitionLock.IsFree()) return;
            _dialogManager.OnArrowNextClicked();
        };

        _dialogUI.onExitDialogClicked += (sender, data) => _dialogManager.OnExitDialogClicked();
        _dialogUI.onTakeRewardClicked += (sender, data) => _dialogManager.OnTakeRewardClicked();
        _dialogUI.onGiveRemedyClicked += (sender, data) => _dialogManager.OnGiveRemedyClicked();
        _dialogUI.onGiveRemedyClicked += (sender, data) => _inventoryManager.Clear();

        _dialogUI.onHintFound += (sender, data) => _keywordManager.AddKeyword(data.Shortcut);

        _dialogManager.onDialogOpen += (sender, data) => _inventoryUI.Hide();
        _dialogManager.onDialogClose += (sender, state) => 
        {
            if ((!_inventoryUI.TutorialMode || !_dialogManager.FirstTimeTalking) 
                && _dialogManager.CurrentState != DialogManager.State.Satisfied)
                _inventoryUI.Show();
        };

        _keywordManager.onNewKeyword += (sender, keyword) => _dialogUI.ValidateHint(keyword);
        _keywordManager.onAllHintsFound += (sender, data) => _dialogUI.AllowQuitDialog();
        _keywordManager.onAllHintsFound += (sender, keywords) => _herbariumUI.UpdateData(keywords);

        _dialogManager.onTextChange += (sender, index) => _dialogUI.DialogBox.SetDialogIndex(index);
        _dialogManager.onDialogOpen += (sender, state) => _dialogUI.Open(state);
        _dialogManager.onDialogClose += (sender, data) => _dialogUI.Close();

        _dialogManager.onDialogClose += (sender, state) => {
            if (state == DialogManager.State.Satisfied)
            {
                _visitorDoor.VisitorLock.Set(_dialogManager);
                _bed.ClientSatisfyLock.Unset(_dialogManager); // Allow to go to bed
                //_gardenDoor.LateLock.Set(_dialogManager);
                _gardenManager.OnEvening();
            }

            if (_dialogManager.FirstTimeTalking) _gardenManager.UnlockGathering(_dialogManager);
        }; 

        _craftingUI.onPrepareClicked += (sender, data) => _craftingStation.OnPrepareClicked();
        _craftingUI.onCancelClicked += (sender, data) => _craftingStation.OnCancelClicked();
        _craftingStation.onCraftingOpen += (sender, data) => _craftingUI.Show();
        _craftingStation.onCraftingClose += (sender, data) => _craftingUI.Hide();
        _craftingStation.GetComponent<InteractiveElement>().onInteract += (sender, data) => _craftingStation.OpenCrafting();
        _craftingStation.onCraftingOpen += (sender, data) => _inventoryUI.AllowRemoveItems(false);
        _craftingStation.onCraftingClose += (sender, data) => _inventoryUI.AllowRemoveItems(true);
        _daysManager.onDayChange += (sender, data) => _craftingStation.NoHintsLock.Set(_dialogManager);
        _dialogManager.onDialogClose += (sender, state) => { if (state == DialogManager.State.WaitForRemedy) _craftingStation.NoHintsLock.Unset(_dialogManager); };
        _craftingStation.onRemedyCrafted += (sender, remedy) =>
        {
            bool validForClient = _daysManager.EvaluateRemedy(remedy);

            MissionData currentMission = _daysManager.CurrentMission;

            bool goodEnding;
            if (currentMission.ReputationBased) goodEnding = _daysManager.Reputation > 0;
            else goodEnding = validForClient;

            _finalCheckupUI.FillData(currentMission, goodEnding, remedy);
        };

            // Garden
        _gardenDoor.onStartTravelDoor += (sender, data) => _CMGardenCamera.SetActive(true);
        _houseDoor.onInteract += (sender, data) => _CMGardenCamera.SetActive(false);

        // Animations
        _dialogManager.onDialogClose += (sender, state) => { if (state == DialogManager.State.Satisfied) _visitorDoor.Close(); };

        // @TODO : Debug, remove later
        //_inventoryUI.gameObject.SetActive(false);
        /*
        _craftingStation.GetComponent<InteractiveElement>().onInteract += (sender, data) =>
        {
            if (!_inventoryManager.IsFull()) return;
            _inventoryManager.AddItem(_craftingStation.BuildRemedy());
        };*/

        _herbarium.onOpen += (sender, data) => _bindGroupsUI.PushBindGroup("HerbariumGlobal");
        _herbarium.onClose += (sender, data) => _bindGroupsUI.PopBindGroup();
        _herbarium.onPlantSelected += (sender, data) => _bindGroupsUI.PushBindGroup("HerbariumFocus");
        _herbarium.onPlantUnselected += (sender, data) => _bindGroupsUI.PopBindGroup();

        //_finalCheckupUI.onOpen += (sender, data) => _bindGroupsUI.PushBindGroup("FinalCheckupPass");
        _finalCheckupUI.onStartClose += (sender, data) => _bindGroupsUI.PopBindGroup();
        _finalCheckupUI.DialogBox.onTextAnimationStart += (sender, data) => _bindGroupsUI.PopBindGroup();
        _finalCheckupUI.DialogBox.onTextAnimationEnd += (sender, data) => _bindGroupsUI.PushBindGroup("FinalCheckupPass");
    }

    private void Start()
	{
        Initialize();
    }

	private void Initialize()
	{
        _daysManager.NextDay();
	}
}

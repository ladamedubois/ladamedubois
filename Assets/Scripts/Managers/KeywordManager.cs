using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeywordManager : MonoBehaviour, IDailyComponent
{
	public event EventHandler<string> onNewKeyword;
	public event EventHandler<List<string>> onAllHintsFound;

	private List<string> _keywords;

	public int MaxKeywordCapacity = 3;

	private void Awake()
	{
		_keywords = new List<string>();
	}

	public void LoadDay(DaysManager.DayData dailyMission)
	{
	}

	public void Reset()
	{
		_keywords.Clear();
	}

	public void AddKeyword(string keyword)
	{
		if (_keywords.Count >= MaxKeywordCapacity) return;

		if (!_keywords.Contains(keyword)) // Can't add the same keyword twice
		{
			onNewKeyword?.Invoke(this, keyword);
			_keywords.Add(keyword);
		}

		if (_keywords.Count >= MaxKeywordCapacity) onAllHintsFound?.Invoke(this, _keywords);
	}
}

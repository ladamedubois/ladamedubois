using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class IntroductionManager : SoundManager, IPointerClickHandler
{
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _garden;
	[BoxGroup("References")] [SerializeField] private GameObject _gardenSource;

	[SerializeField] private TransitionFader _transitionFader;
    [SerializeField] private UIBindManager _bindGroups;
    [SerializeField] private UIDialogBox _dialogBox;
	[SerializeField] private List<SimpleTextBlock> _dialogTexts;
	[SerializeField] private UIPauseMenu _pauseMenu;

	[SerializeField] private string _nextSceneName;

	public int CurrentDialogIndex { get; private set; }

	public UIDialogBox DialogBox { get => _dialogBox; }

	private FMOD.Studio.EventInstance _currentAmb;

	private void Awake()
	{
		_transitionFader.onTransitionEnd += (sender, name) => OnFaderTransitionEnd(name);
		_dialogBox.onTextAnimationEnd += (sender, data) => _bindGroups.SetBindGroup("FabreDialogPass");
		_dialogBox.onTextAnimationStart += (sender, data) => _bindGroups.SetBindGroup();

		_dialogTexts.ForEach(text =>
		{
			_dialogBox.AddSimpleTextBlock(text);
		});

		_pauseMenu.onReturnToMenuClicked += (sender, data) =>
		{
			StopSound(_currentAmb);
		};
	}

	private void Start()
	{
		_dialogBox.Hide(false);
		_transitionFader.DoFromBlack(2.0f, "ShowGarden", 2.0f);

		_currentAmb = PlaySoundStoppable(_garden, true, _gardenSource);
	}

	private void OnFaderTransitionEnd(string name)
	{
		if (name == "ShowGarden")
		{
			_dialogBox.Show();
			_dialogBox.SetDialogIndex(0);
		}
		else if (name == "EndIntroduction")
		{
			SceneManager.LoadScene(_nextSceneName, LoadSceneMode.Single);
		}
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!_dialogBox.IsOpen) return;
		if (!_dialogBox.TextTransitionLock.IsFree()) return;

		CurrentDialogIndex++;

		if (CurrentDialogIndex >= _dialogBox.TextBlocksCount)
		{
			FinishIntro();
		}

		_dialogBox.SetDialogIndex(CurrentDialogIndex);
	}

	private void FinishIntro()
	{
		StopSound(_currentAmb);
		_transitionFader.DoFromTransparent(2.0f, "EndIntroduction");
		_bindGroups.SetBindGroup();
	}
}

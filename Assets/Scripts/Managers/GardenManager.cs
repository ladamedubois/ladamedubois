using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GardenManager : MonoBehaviour, IDailyComponent, ITutorialLock
{
	public event EventHandler onPlantGathered;

	public event EventHandler onCantGatherInventoryFull;
	public event EventHandler onCantGatherAlreadyInInventory;

	[BoxGroup("References")] [SerializeField] private Camera _camera;
	[BoxGroup("References")] [SerializeField] private List<Plot> _plots;
	[BoxGroup("References")] [SerializeField] private InventoryManager _inventory;

	[BoxGroup("Lights")] [SerializeField] private GameObject _dayLights;
	[BoxGroup("Lights")] [SerializeField] private GameObject _eveningLights;

	private void Awake()
	{
		gameObject.SetActive(false);

		foreach (Plot plant in _plots)
		{
			plant.Canvas.worldCamera = _camera;
		}
	}

	public void LoadDay(DaysManager.DayData dailyMission)
	{
		List<PlantData> plants = dailyMission.DailyMission.PlantsAvailable;
		for (int i = 0; i < _plots.Count; i++)
		{
			if (i >= plants.Count) return;

			_plots[i].Bind(plants[i]);

			_plots[i].onPlantGathered += OnPlantGathered;
			_plots[i].onAlreadyGathered += (sender, data) => onCantGatherAlreadyInInventory.Invoke(this, EventArgs.Empty);
			_plots[i].onNoInventoryPlace += (sender, data) => onCantGatherInventoryFull?.Invoke(this, EventArgs.Empty);
		}

		foreach (Plot plant in _plots)
		{
			plant.Interactive.Lock.Unset(this);
		}

		_dayLights.SetActive(true);
		_eveningLights.SetActive(false);
	}

	private void OnPlantGathered(object sender, Plot.PlantGatheredData data)
	{
		Plant plant = new Plant(data.PlantData);
		bool added = _inventory.AddItem(plant, data.Position);
		if (added) (sender as Plot).GatheredLock.Set(this);

		if (_inventory.IsFull())
		{
			OnInventoryFull();
		}
	}

	public void Reset()
	{
		for (int i = 0; i < _plots.Count; i++)
		{
			_plots[i].onPlantGathered -= OnPlantGathered;
		}
	}

	private void OnInventoryFull()
	{
		foreach (Plot plant in _plots)
		{
			plant.FullInventoryLock.Set(_inventory);
		}
	}

	public void OnEvening()
	{
		foreach (Plot plant in _plots)
		{
			plant.Interactive.Lock.Set(this);
		}

		SetEveningLights();
	}

	public void OnInventoryRemove(IInventoryItem item)
	{
		if (!(item is Plant plant)) return;

		Plot plot = GetCorrespondingPlot(plant.Type);
		if (plot) plot.GatheredLock.Unset(this);

		if (!_inventory.IsFull())
		{
			foreach (Plot p in _plots)
			{
				p.FullInventoryLock.Unset(_inventory);
			}
		}
	}

	public Plot GetCorrespondingPlot(PlantData data)
	{
		for (int i = 0; i < _plots.Count; i++)
		{
			if (_plots[i].Plant == data)
				return _plots[i];
		}

		return null;
	}

	public void LockGathering(object locker)
	{
		_plots.ForEach(plot => plot.Interactive.Lock.Set(locker));
	}

	public void UnlockGathering(object locker)
	{
		_plots.ForEach(plot => plot.Interactive.Lock.Unset(locker));
	}

	public void TutorialLock(TutorialManager tutorial)
	{
		_plots.ForEach(plot => plot.Interactive.TutorialLock(tutorial));
	}

	public void TutorialUnlock(TutorialManager tutorial)
	{
		_plots.ForEach(plot => plot.Interactive.TutorialUnlock(tutorial));
	}

	private void SetEveningLights()
	{
		_dayLights.SetActive(false);
		_eveningLights.SetActive(true);
	}
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * An interface for each manager in the game that resets each day
 * Also manages the reputation system
 */
interface IDailyComponent
{
    public void Reset();
    public void LoadDay(DaysManager.DayData dayData);
}

/**
 * The manager that loads each new day and updates all other managers
 */
public class DaysManager : MonoBehaviour
{
    public class DayData { 
        public MissionData DailyMission;
    }

    public event EventHandler<DayData> onDayChange;
    public event EventHandler<DayData> onDayEnd;
    public event EventHandler onNoDayLeft;

    [SerializeField] private List<MissionData> _missionQueue;
    [SerializeField] private List<GameObject> _dailyElementsList; // objects must implement IDailyComponent

    private MissionData _currentMission = null;
    public MissionData CurrentMission { get => _currentMission; }

    public int Reputation { get; private set; }

	private void Awake()
	{
        Reputation = 0;
	}

	public bool EvaluateRemedy(Remedy remedy)
	{
        bool valid = _currentMission.CheckRemedy(remedy);
        Reputation += valid ? 1 : -1;
        return valid;
	}

    /**
     * Go to the next day
     */
    public void NextDay()
	{
        if (CurrentMission != null)
            onDayEnd?.Invoke(this, new DayData { DailyMission = _currentMission });

        if (_missionQueue.Count == 0) {
            onNoDayLeft?.Invoke(this, EventArgs.Empty);
            return;
        }

        ResetDailyList();

        _currentMission = _missionQueue[0];
        _missionQueue.RemoveAt(0);
        DayData dayData = new DayData { DailyMission = _currentMission };

        LoadDailyList(dayData);

        onDayChange?.Invoke(this, dayData);
    }

    public void ResetDailyList()
	{
        foreach (GameObject obj in _dailyElementsList)
		{
            IDailyComponent[] idcs = obj.GetComponents<IDailyComponent>();
            foreach (IDailyComponent idc in idcs) idc.Reset();
		}
	}

    public void LoadDailyList(DaysManager.DayData dayData)
    {
        foreach (GameObject obj in _dailyElementsList)
        {
            IDailyComponent[] idcs = obj.GetComponents<IDailyComponent>();
            foreach (IDailyComponent idc in idcs) idc.LoadDay(dayData);
        }
    }
}

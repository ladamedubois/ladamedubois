using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager : MonoBehaviour, IDailyComponent
{
	public enum State { NotStarted, WaitForRemedy, GiveRemedy, Satisfied}

	public event EventHandler<int> onTextChange; // The passed int represents the dialog index
	public event EventHandler<State> onDialogOpen;
	public event EventHandler<State> onDialogClose;
	public event EventHandler onEnterSymptomPhase;
	public event EventHandler onRemedyGiven;
	
	public State CurrentState { get; private set; }
	public int CurrentIndex { get => _dialogIndex; }
	public bool FirstTimeTalking { get; private set; }
	public bool AlreadyTalked { get; private set; }

	private int _dialogIndex = 0;
	private int _maxMainDialogIndex = 0;
	private int _symptomPhaseIndex = 0;
	private bool _hasRemedy;
	private bool _alreadyEnteredSymptomPhase;

	private List<VisitorData.DialogText> _dialogTexts;

	private void Awake()
	{
		_dialogTexts = new List<VisitorData.DialogText>();
	}

	public void LoadDay(DaysManager.DayData dayData)
	{
		CurrentState = State.NotStarted;
		_dialogIndex = 0;
		_maxMainDialogIndex = dayData.DailyMission.Visitor.LoreText.Count + dayData.DailyMission.Visitor.SymptomsText.Count - 1;
		_symptomPhaseIndex = dayData.DailyMission.Visitor.LoreText.Count;
		_hasRemedy = false;
		_alreadyEnteredSymptomPhase = false;
		FirstTimeTalking = true;
		AlreadyTalked = false;

		_dialogTexts.AddRange(dayData.DailyMission.Visitor.LoreText);
		_dialogTexts.AddRange(dayData.DailyMission.Visitor.SymptomsText);
		_dialogTexts.Add(dayData.DailyMission.Visitor.WaitingText);
		_dialogTexts.Add(dayData.DailyMission.Visitor.RewardText);
	}

	public void Reset()
	{
		_dialogTexts.Clear();
	}

	public void OpenDialog()
	{
		switch (CurrentState)
		{
			case State.Satisfied: return;

			case State.NotStarted:
				_dialogIndex = 0;
				AlreadyTalked = true;
				break;
			case State.WaitForRemedy:
				_dialogIndex = _maxMainDialogIndex + 1;
				if (_hasRemedy) // has potion in inventory
					CurrentState = State.GiveRemedy;
				break;
		}

		onDialogOpen?.Invoke(this, CurrentState);
		onTextChange?.Invoke(this, _dialogIndex);
	}

	public void CloseDialog()
	{
		onDialogClose?.Invoke(this, CurrentState);
		FirstTimeTalking = false;
	}	 

	public void ChangeState(State newState)
	{
		CurrentState = newState;
	}

	public void OnArrowPrevClicked()
	{
		if (_dialogIndex <= 0) return;

		_dialogIndex--;
		onTextChange?.Invoke(this, _dialogIndex);
	}

	public void OnArrowNextClicked()
	{
		if (_dialogIndex >= _maxMainDialogIndex) return;

		_dialogIndex++;
		onTextChange?.Invoke(this, _dialogIndex);
		
		if (_dialogIndex >= _symptomPhaseIndex && !_alreadyEnteredSymptomPhase)
		{
			_alreadyEnteredSymptomPhase = true;
			onEnterSymptomPhase?.Invoke(this, EventArgs.Empty);
		}
	}

	public void OnExitDialogClicked()
	{
		if (CurrentState == State.NotStarted)
			CurrentState = State.WaitForRemedy;
		CloseDialog();
	}

	public void OnGiveRemedyClicked()
	{
		if (CurrentState != State.GiveRemedy) return;

		_dialogIndex++;
		onTextChange?.Invoke(this, _dialogIndex);
		onRemedyGiven?.Invoke(this, EventArgs.Empty);
	}

	public void OnTakeRewardClicked()
	{
		if (CurrentState != State.GiveRemedy) return;

		CurrentState = State.Satisfied;
		CloseDialog();
	}

	public void OnGotRemedy()
	{
		_hasRemedy = true;
	}

	public VisitorData.DialogText GetDialogByIndex(int index)
	{
		return _dialogTexts[index];
	}
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * An interface for items that can be placed in inventory
 */
public interface IInventoryItem
{
	public Sprite GetImage();
	public bool CanRemove();
}

public class InventoryManager : MonoBehaviour, IDailyComponent
{
	public enum State { Plants, Remedy }
	public class ItemEventData
	{
		public IInventoryItem Item;
		public int Slot;
		public Vector3? WorldSourcePosition;
	}

	public event EventHandler<ItemEventData> onAddItem;
	public event EventHandler onFull;
	public event EventHandler<ItemEventData> onRemoveItem;
	public event EventHandler onClear;
	public event EventHandler<State> onStateChange;

	public State CurrentState { get; private set; }

	private IInventoryItem[] _items;

	private void Awake()
	{
		_items = new IInventoryItem[3];
	}

	public void Reset()
	{
		CurrentState = State.Plants;
		onStateChange?.Invoke(this, CurrentState);

		Clear();
	}

	public void LoadDay(DaysManager.DayData dayData)
	{
		/*LeanTween.value(0, 1, 4.0f).setOnComplete(() =>
		{
			onStateChange?.Invoke(this, State.Remedy);
			LeanTween.value(0, 1, 2.0f).setOnComplete(() =>
			{
				onStateChange?.Invoke(this, State.Plants);
			});
		});*/
		/*Remedy remedy = new Remedy(dayData.DailyMission);
		AddItem(remedy);*/
	}

	/**
	 * Adds an item to the inventory.
	 * Auto manages the inventory collapse and state depending on item type
	 */
	public bool AddItem(IInventoryItem item, Vector3? worldSourcePosition = null)
	{
		if (CurrentState == State.Remedy) return false; // Can't add an item when the inventory contains a remedy

		int index = -1;

		if (item is Remedy) // If the item is a remedy
		{
			Clear();
			CurrentState = State.Remedy;
			onStateChange?.Invoke(this, CurrentState);
		}

		index = GetFirstFreeIndex();

		if (index == -1) // If the inventory is full
		{
			onFull?.Invoke(this, EventArgs.Empty);
			return false; 
		}

		_items[index] = item;
		onAddItem?.Invoke(this, new ItemEventData() { Item = item, Slot = index, WorldSourcePosition = worldSourcePosition });

		index = GetFirstFreeIndex();
		if (index == -1) // If it was the last slot available
			onFull?.Invoke(this, EventArgs.Empty);

		return true;
	}

	public void RemoveItem(int slot)
	{
		if (!ItemAt(slot)) return;

		IInventoryItem item = _items[slot];
		_items[slot] = null;

		onRemoveItem?.Invoke(this, new ItemEventData() { Item = item, Slot = slot });
	}

	/**
	 * Removes all the items in the inventory
	 */
	public void Clear()
	{
		for (int i = 0; i < _items.Length; i++)
			_items[i] = null;

		onClear?.Invoke(this, EventArgs.Empty);
	}

	public void OnItemClicked(int slot)
	{
		if (!_items[slot].CanRemove())
		{
			return;
		}

		RemoveItem(slot);
	}


	/**
	 * Returns the first empty slot of the inventory
	 * Returns -1 if the inventory does not have empty slots
	 */
	public int GetFirstFreeIndex()
	{
		for (int i = 0; i < _items.Length; i++)
			if (_items[i] == null) return i;
		return -1;
	}

	public bool IsFull()
	{
		return GetFirstFreeIndex() == -1;
	}

	public bool ItemAt(int index)
	{
		return _items[index] != null;
	}

	public List<Plant> GetPlants()
	{
		List<Plant> plants = new List<Plant>();
		foreach (IInventoryItem item in _items)
		{
			Plant plant = item as Plant;
			if (plant != null) plants.Add(plant);
		}
		return plants;
	}

	public Remedy GetRemedy()
	{
		if (CurrentState != State.Remedy) return null;
		return _items[0] as Remedy;
	}
}
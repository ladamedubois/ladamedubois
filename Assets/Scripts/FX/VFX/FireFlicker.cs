using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

/**
 * Stolen and adapted from https://gist.github.com/sinbad/4a9ded6b00cf6063c36a4837b15df969
 */
public class FireFlicker : MonoBehaviour
{
    //[SerializeField] private Light _light;
    [SerializeField] private HDAdditionalLightData _light;

    public float MinIntensity;
    public float MaxIntensity;
    public float FlickerSpeed = 1;

    public Vector3 MinPos = Vector3.zero;
    public Vector3 MaxPos = Vector3.zero;

    private float x;

    void Update()
    {
        if (_light == null)
            return;

        x += FlickerSpeed * Time.deltaTime;

        _light.intensity = MinIntensity + Mathf.PerlinNoise(x, 0.2f) * (MaxIntensity - MinIntensity);

        Vector3 newPos = Vector3.zero;
        /*newPos.x = MinPos.x + Mathf.PerlinNoise(x, 0.8f) * (MaxPos.x - MinPos.x);*/
        newPos.y = MinPos.y + Mathf.PerlinNoise(x, 0.98f) * (MaxPos.y - MinPos.y);
        /*newPos.z = MinPos.z + Mathf.PerlinNoise(x, 0.3f) * (MaxPos.z - MinPos.z);*/

        transform.localPosition = newPos;
    }
}

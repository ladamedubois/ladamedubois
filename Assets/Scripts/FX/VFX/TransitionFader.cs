using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransitionFader : MonoBehaviour
{
	public event EventHandler<string> onTransitionStart;
	public event EventHandler<string> onTransitionStill;
	public event EventHandler<string> onTransitionStillEnd;
	public event EventHandler<string> onTransitionEnd;
	public event EventHandler<string> onTransitionCancelled;

	[BoxGroup("References")] [SerializeField] private Image _overlay;

	public bool Processing { get; private set; }
	public string Name { get; private set; }

	private void Awake()
	{
		Name = "";
		Processing = false;
		onTransitionStart += (sender, data) => Processing = true;
		onTransitionEnd += (sender, data) => Processing = false;
	}

	public bool DoFromBlack(float time, string name = "", float delay = 0f)
	{
		if (Name != "") return false; // Can't chain transitions
		Name = name;

		onTransitionStart?.Invoke(this, Name);

		_overlay.DOFade(0f, time).SetDelay(delay).From(1f).SetEase(Ease.InOutCubic)
			.OnComplete(() =>
			{
				string previousName = Name;
				Name = "";
				onTransitionEnd?.Invoke(this, previousName);
			});

		return true;
	}

	public bool DoFromTransparent(float time, string name = "", float delay = 0f)
	{
		if (Name != "") return false; // Can't chain transitions
		Name = name;

		onTransitionStart?.Invoke(this, Name);

		_overlay.DOFade(1f, time).SetDelay(delay).From(0f).SetEase(Ease.Linear)
			.OnComplete(() =>
			{
				string previousName = Name;
				Name = "";
				onTransitionEnd?.Invoke(this, previousName);
			});

		return true;
	}

	public bool DoTransition(float fadeTime, float stillTime, string name = "", float fadeOutTime = -1f)
	{
		if (Name != "") return false; // Can't chain transitions
		Name = name;

		onTransitionStart?.Invoke(this, Name);

		DOTween.Sequence()
			.Append(
				_overlay.DOFade(1f, fadeTime).From(0f).SetEase(Ease.Linear)
					.OnComplete(() =>
					{
						onTransitionStill?.Invoke(this, Name);
					})
			)
			.AppendInterval(stillTime)
			.Append(
				_overlay.DOFade(0f, fadeOutTime > 0 ? fadeOutTime : fadeTime).From(1f).SetEase(Ease.Linear)
					.OnStart(() =>
					{
						onTransitionStillEnd?.Invoke(this, Name);
					})
					.OnComplete(() =>
					{
						string previousName = Name;
						Name = "";
						onTransitionEnd?.Invoke(this, previousName);
					})
			);

		return true;
	}

	public void StopTransition(float targetAlpha = 0f)
	{
		if (Name != "") onTransitionCancelled?.Invoke(this, Name);
		Name = "";
		_overlay.color = Color.black * targetAlpha;
		DOTween.Kill(_overlay);
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SoundManager : MonoBehaviour
{
	protected void PlaySoundParam(FMODUnity.EventReference sound, string name, float value)
	{
		FMOD.Studio.EventInstance soundInstance = FMODUnity.RuntimeManager.CreateInstance(sound);
		soundInstance.setParameterByName(name, value);
		soundInstance.start();
		soundInstance.release();
	}

	protected FMOD.Studio.EventInstance CreateSoundParam(FMODUnity.EventReference sound, string name, float value)
	{
		FMOD.Studio.EventInstance soundInstance = FMODUnity.RuntimeManager.CreateInstance(sound);
		soundInstance.setParameterByName(name, value);
		return soundInstance;
	}

	protected void SetParam(FMOD.Studio.EventInstance soundInstance, string name, float value)
	{
		soundInstance.setParameterByName(name, value);
	}

	protected void PlaySound(FMODUnity.EventReference sound)
	{
		FMOD.Studio.EventInstance soundInstance = FMODUnity.RuntimeManager.CreateInstance(sound);
		soundInstance.start();
		soundInstance.release();
	}

	protected void PlaySoundStoppable(FMOD.Studio.EventInstance sound)
	{
		sound.start();
	}

	protected FMOD.Studio.EventInstance PlaySoundStoppable(FMODUnity.EventReference sound, bool is3d = false, GameObject source = null)
	{
		FMOD.Studio.EventInstance soundInstance = FMODUnity.RuntimeManager.CreateInstance(sound);
		
		if (is3d)
		{
			FMOD.ATTRIBUTES_3D attr3d = FMODUnity.RuntimeUtils.To3DAttributes(source);
			soundInstance.set3DAttributes(attr3d);
		}

		soundInstance.start();
		return soundInstance;
	}

	protected FMOD.Studio.EventInstance PlaySoundStoppable(FMODUnity.EventReference sound)
	{
		FMOD.Studio.EventInstance soundInstance = FMODUnity.RuntimeManager.CreateInstance(sound);
		soundInstance.start();
		return soundInstance;
	}

	protected void StopSound(FMOD.Studio.EventInstance soundInstance)
	{
		soundInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
		soundInstance.release();
	}
}

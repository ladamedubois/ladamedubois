using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogSound : SoundManager, IDailyComponent
{
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _open;
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _close;
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _arrowClick;
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _keywordHover;
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _keywordRight;
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _keywordWrong;
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _keywordStartSelect;
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _giveRemedy;
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _receiveReward;


    [BoxGroup("References")] [SerializeField] private DialogManager _dialog;
    [BoxGroup("References")] [SerializeField] private UIDialog _dialogUI;

    private VisitorData _dailyVisitor;
    private int _minVoiceIndex;
    private FMOD.Studio.EventInstance _selectSoundInstance;

	public void LoadDay(DaysManager.DayData dayData)
	{
        _dailyVisitor = dayData.DailyMission.Visitor;
        _minVoiceIndex = -1;
    }

	public void Reset()
	{
	}

	private void Awake()
    {
        _dialog.onDialogOpen += (sender, data) => PlaySound(_open);
        _dialog.onDialogClose += (sender, data) => PlaySound(_close);
        _dialogUI.onGiveRemedyClicked += (sender, data) => PlaySound(_giveRemedy);
        _dialogUI.onTakeRewardClicked += (sender, data) => PlaySound(_receiveReward);
        _dialogUI.DialogBox.onSymptomBlockHovered += (sender, data) => PlaySound(_keywordHover);
        _dialogUI.DialogBox.onSymptomBlockSelected += (sender, data) =>
        {
            FMODUnity.EventReference sound = data.Validity ? _keywordRight : _keywordWrong;
            PlaySound(sound);
        };

        _dialogUI.DialogBox.onSymptomBlockSelectionStarted += (sender, data) =>
        {
            _selectSoundInstance = PlaySoundStoppable(_keywordStartSelect);
        };

        _dialogUI.DialogBox.onSymptomBlockSelectionCancelled += (sender, data) =>
        {
            StopSound(_selectSoundInstance);
        };

        /*_dialog.onTextChange += (sender, index) =>
        {
            if (index >= _minVoiceIndex)
                PlaySound(_dialog.GetDialogByIndex(index).Voice);
            _minVoiceIndex = Mathf.Max(_minVoiceIndex, index);
        };*/

        _dialogUI.DialogBox.onTextAnimationStartAppear += (sender, data) =>
        {
            int index = _dialog.CurrentIndex;
            if (index >= _minVoiceIndex)
                PlaySound(_dialog.GetDialogByIndex(index).Voice);
            _minVoiceIndex = Mathf.Max(_minVoiceIndex, index);
        };

        _dialogUI.onArrowNextClicked += (sender, data) => PlaySound(_arrowClick);
        _dialogUI.onArrowPrevClicked += (sender, data) => PlaySound(_arrowClick);
    }
}

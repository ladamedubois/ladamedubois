using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CauldronSound : SoundManager
{
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _hover;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _dameVoice;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _loadCrafting;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _craftingRemedySound;

	[BoxGroup("References")] [SerializeField] private InteractiveElement _interactive;
	[BoxGroup("References")] [SerializeField] private CraftingStation _crafting;
	[BoxGroup("References")] [SerializeField] private UICrafting _craftingUI;

	private void Awake()
	{
		_interactive.onCursorEnter += (sender, blocked) => { if (!blocked) PlaySound(_hover); };
		_crafting.onRemedyCrafted += (sender, data) => PlaySound(_craftingRemedySound);
		_craftingUI.DialogBox.onTextAnimationStartAppear += (sender, data) =>
		{
			PlaySound(_dameVoice);
		};
	}
}

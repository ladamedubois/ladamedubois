using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BedSound : SoundManager
{
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _hover;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _getIn;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _getOut;

	[BoxGroup("References")] [SerializeField] private InteractiveElement _interactive;
	[BoxGroup("References")] [SerializeField] private Bed _bed;

	private void Awake()
	{
		_interactive.onCursorEnter += (sender, blocked) => { if (!blocked) PlaySound(_hover); };
		_bed.onGetIn += (sender, data) => PlaySound(_getIn);
		_bed.onWakeUp += (sender, data) => PlaySound(_getOut);
	}
}

using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySound : SoundManager
{
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _plantCollect;
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _removeItem;

    [BoxGroup("References")] [SerializeField] private UIInventory _inventoryUI;

	private void Awake()
	{
		_inventoryUI.onGhostItemArrived += (data, item) =>
		{
			if (item.Item is Plant)
				PlaySound(_plantCollect);
		};

		_inventoryUI.onItemRemoved += (data, item) => PlaySound(_removeItem);
	}
}

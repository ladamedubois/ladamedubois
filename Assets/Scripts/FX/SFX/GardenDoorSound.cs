using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GardenDoorSound : SoundManager
{
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _hover;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _use;

	[BoxGroup("References")] [SerializeField] private GardenDoor _door;

	private void Awake()
	{
		_door.onStartTravelDoor += (sender, data) => PlaySound(_use);
		_door.GetComponent<InteractiveElement>().onCursorEnter += (sender, blocked) => { if (!blocked) PlaySound(_hover); };
	}
}

using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSound : SoundManager
{
    [SerializeField] private CharacterAnimationController _animations;
	[SerializeField] private AmbSound _ambSound;

	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _footstep;

	[BoxGroup("Params")] [SerializeField] private string _material;

	private void Awake()
	{
		_animations.onFootstep += (sender, data) => PlaySoundParam(_footstep, _material, _ambSound.InGarden ? 1 : 0);
	}
}

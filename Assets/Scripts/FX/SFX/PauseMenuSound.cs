using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuSound : SoundManager
{
    [SerializeField] private UIPauseMenu _pauseMenu;

	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _open;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _close;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _transition;

	private void Awake()
	{
		_pauseMenu.onOpen += (sender, data) => PlaySound(_open);
		_pauseMenu.onOpen += (sender, data) => PlaySound(_close);
		_pauseMenu.onPanelTransition += (sender, data) => PlaySound(_transition);
	}
}

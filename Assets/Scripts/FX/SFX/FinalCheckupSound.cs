using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalCheckupSound : SoundManager
{
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _amb;

	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _crowdWhispers;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _chordEvent;

	[BoxGroup("References")] [SerializeField] private UIFinalCheckup _finalCheckup;

	[SerializeField] private string _recapResultParam;

	private bool _resolved;
	private FMOD.Studio.EventInstance _chord;
	private FMOD.Studio.EventInstance _currentAmb;

	private UIFinalCheckup.FinalCheckupData _data;

	private void Awake()
	{
		_finalCheckup.onOpen += (sender, data) =>
		{
			_currentAmb = PlaySoundStoppable(_amb);

			_data = data;
			_resolved = false;
			_chord = CreateSoundParam(_chordEvent, _recapResultParam, 0);

			if (data.IsFabre)
			{
				SetParam(_chord, _recapResultParam, data.IsGoodEnding ? 1 : 2);
			}
		};

		_finalCheckup.onClose += (sender, data) =>
		{
			StopSound(_chord);
			StopSound(_currentAmb);
		};

		_finalCheckup.DialogBox.onTextAnimationStart += (sender, data) =>
		{
			if (!_resolved)
			{
				PlaySoundStoppable(_chord);
			}
			else if (!_data.IsFabre)
			{
				SetParam(_chord, _recapResultParam, _data.IsGoodEnding ? 1 : 2);
			}

			_resolved = true;
		};

		_finalCheckup.DialogBox.onTextAnimationStartAppear += (sender, data) =>
		{
			PlaySound(_crowdWhispers);
		};
	}
}

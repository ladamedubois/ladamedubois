using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlotSound : SoundManager
{
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _hover;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _cut;

	[BoxGroup("Params")] [SerializeField] private string _plantType;

	[BoxGroup("References")] [SerializeField] private Plot _plot;

	private InteractiveElement _interactive;

	private void Awake()
	{
		_interactive = _plot.GetComponent<InteractiveElement>();

		_plot.onPlantGathered += (sender, data) => PlaySound(_cut);
		_interactive.onCursorEnter += (sender, blocked) =>
		{
			if (_plot.Plant == null || blocked)
			{
				return;
			}
			PlaySoundParam(_hover, _plantType, _plot.Plant.Type);
		};
	}
}

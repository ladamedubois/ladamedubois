using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GameButton))]
public class ButtonSound : SoundManager
{
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _hover;
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _click;

	private GameButton _button;

	private void Awake()
	{
		_button = GetComponent<GameButton>();

		_button.onClicked += (sender, data) => PlaySound(_click);
		_button.onCursorEnter += (sender, data) => PlaySound(_hover);
	}
}

using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbSound : SoundManager
{
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _music;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _house;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _garden;

	[BoxGroup("References")] [SerializeField] private TransitionFader _transition;
	[BoxGroup("References")] [SerializeField] private GameObject _gardenSource;
	[BoxGroup("References")] [SerializeField] private GameObject _houseSource;
	[BoxGroup("References")] [SerializeField] private DialogManager _dialogManager;
	[BoxGroup("References")] [SerializeField] private VisitorDoor _visitorDoor;
	[BoxGroup("References")] [SerializeField] private Bed _bed;
	[BoxGroup("References")] [SerializeField] private UIPauseMenu _pauseMenu;

	[BoxGroup("Params")] [SerializeField] private string _eveningParam;
	[BoxGroup("Params")] [SerializeField] private string _windowOpenParam;
	[BoxGroup("Params")] [SerializeField] private string _musicLocationParam;

	private bool _inGarden;
	private FMOD.Studio.EventInstance _currentAmb;
	private FMOD.Studio.EventInstance _currentMusic;

	private float _eveningParamValue;
	private float _windowParamValue;

	public bool InGarden { get => _inGarden; }

	private void Awake()
	{
		_inGarden = false;
		_transition.onTransitionStillEnd += OnMidTransition;
		_transition.onTransitionEnd += OnTransitionEnd;

		_dialogManager.onRemedyGiven += (sender, data) =>
		{
			_eveningParamValue = 1.0f;
			SetParam(_currentAmb, _eveningParam, _eveningParamValue);
		};

		_visitorDoor.onOpen += (sender, data) =>
		{
			_windowParamValue = 1.0f;
			SetParam(_currentAmb, _windowOpenParam, _windowParamValue);
		};

		_visitorDoor.onClose += (sender, data) =>
		{
			_windowParamValue = 0.0f;
			SetParam(_currentAmb, _windowOpenParam, _windowParamValue);
		};

		_bed.onGetIn += (sender, data) =>
		{
			StopSound(_currentAmb);
			StopSound(_currentMusic);
		};

		_pauseMenu.onReturnToMenuClicked += (sender, data) =>
		{
			StopSound(_currentAmb);
			StopSound(_currentMusic);
		};
	}

	private void OnTransitionEnd(object sender, string name)
	{
		if (name != "DayStart") return;

		_currentAmb = PlaySoundStoppable(_house, true, _houseSource);

		_eveningParamValue = 0.0f;
		SetParam(_currentAmb, _eveningParam, _eveningParamValue);

		_currentMusic = PlaySoundStoppable(_music);
		SetParam(_currentMusic, _musicLocationParam, InGarden ? 1 : 0);
	}

	private void OnMidTransition(object sender, string name)
	{
		if (name != "Garden") return;

		StopSound(_currentAmb);

		_inGarden = !_inGarden;
		FMODUnity.EventReference amb = _inGarden ? _garden : _house;

		_currentAmb = PlaySoundStoppable(amb, true, InGarden ? _gardenSource : _houseSource);
		SetParam(_currentAmb, _windowOpenParam, _windowParamValue);
		SetParam(_currentAmb, _eveningParam, _eveningParamValue);

		SetParam(_currentMusic, _musicLocationParam, InGarden ? 1 : 0);
	}
}

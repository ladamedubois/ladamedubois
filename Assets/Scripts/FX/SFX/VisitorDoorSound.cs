using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisitorDoorSound : SoundManager
{
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _hover;
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _open;
    [BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _close;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _knock;

	[BoxGroup("References")] [SerializeField] private VisitorDoor _door;
	private InteractiveElement _interactive;

	private void Awake()
	{
		_interactive = _door.GetComponent<InteractiveElement>();

		_interactive.onCursorEnter += (sender, blocked) => { if (!blocked) PlaySound(_hover); };
		_door.onOpen += (sender, data) => PlaySound(_open);
		_door.onClose += (sender, data) => PlaySound(_close);
		_door.onKnock += (sender, data) => PlaySound(_knock);
	}
}

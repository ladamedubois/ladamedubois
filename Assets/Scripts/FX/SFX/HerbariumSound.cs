using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HerbariumSound : SoundManager
{
	[SerializeField] private Herbarium _herbarium;

	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _amb;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _open;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _close;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _plantHover;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _plantHoverUnavailable;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _plantUnselect;

	[BoxGroup("Params")] [SerializeField] private string _dailyOpened;
	[BoxGroup("Params")] [SerializeField] private string _dailyClosed;
	[BoxGroup("Params")] [SerializeField] private string _plantType;

	private FMOD.Studio.EventInstance _currentAmb;

	private void Awake()
	{
		_herbarium.onOpen += (sender, data) =>
		{
			PlaySoundParam(_open, _dailyOpened, _herbarium.DailyUsed ? 1f : 0f);
			_currentAmb = PlaySoundStoppable(_amb);
		};

		_herbarium.onClose += (sender, data) =>
		{
			PlaySoundParam(_close, _dailyClosed, _herbarium.DailyUsed ? 1f : 0f);
			StopSound(_currentAmb);
		};

		_herbarium.onPlantUnselected += (sender, data) => PlaySound(_plantUnselect);

		_herbarium.onPlantSelected += (sender, plant) => PlaySound(plant.Data.HerbariumClickEvent);
		_herbarium.onPlantHover += (sender, plant) =>
		{
			FMODUnity.EventReference hoverSound = plant.Available ? _plantHover : _plantHoverUnavailable;
			PlaySoundParam(hoverSound, _plantType, plant.Data.Type);
		};
	}
}
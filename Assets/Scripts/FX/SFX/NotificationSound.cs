using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationSound : SoundManager
{
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _informationSound; // Same sound for tasks
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _blockedSound;

	[BoxGroup("References")] [SerializeField] private InfoMessage _notification;

	private void Awake()
	{
		_notification.onNotificationAppear += (sender, type) =>
		{
			FMODUnity.EventReference sound = type == InfoMessage.Type.Blocked ? _blockedSound : _informationSound;
			PlaySound(sound);
		};
	}
}

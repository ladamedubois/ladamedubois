using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(IntroductionManager))]
public class IntroductionSound : SoundManager
{
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _open;
	[BoxGroup("Events")] [SerializeField] private FMODUnity.EventReference _close;

	[BoxGroup("Events")] [SerializeField] private List<FMODUnity.EventReference> _fabreVoices;

	private IntroductionManager _introManager;

	private void Awake()
	{
		_introManager = GetComponent<IntroductionManager>();
		_introManager.DialogBox.onTextAnimationStartAppear += (sender, data) =>
		{
			PlaySound(_fabreVoices[_introManager.CurrentDialogIndex]);
		};

		_introManager.DialogBox.onOpenAnimationStarted += (sender, data) => PlaySound(_open);
		_introManager.DialogBox.onCloseAnimationStarted += (sender, data) => PlaySound(_close);
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Mission", menuName = "ScriptableObjects/Mission")]
public class MissionData : ScriptableObject
{
	public bool EnableTutorial;
	public VisitorData Visitor;

	[Header("Needs")]
	public List<PlantEffectData> RequiredEffects;
	public List<PlantEffectData> BadEffects;
	public Sprite RemedyImage;
	public string RemedyName;

	[Header("Mission Summary (insert $ for remedy composition)")]
	public bool ReputationBased; // only used for last mission
	[TextArea(5, 10)] public List<string> GoodRemedyText;
	[TextArea(5, 10)] public List<string> BadRemedyText;

	[Header("Garden")]
	public List<PlantData> PlantsAvailable;

	[Header("Garden")]
	public Sprite RewardImage;
	public GameObject RewardModel;

	/**
	 * Checks if a remedy if efficient for the mission
	 */
	public bool CheckRemedy(Remedy remedy)
	{
		List<PlantEffectData> needsToCheck = new List<PlantEffectData>();
		needsToCheck.AddRange(RequiredEffects);

		foreach(PlantData plant in remedy.Components)
		{
			foreach(PlantEffectData effect in plant.Effects)
			{
				needsToCheck.Remove(effect);
				if (BadEffects.Contains(effect)) return false;
			}
		}

		return needsToCheck.Count == 0;
	}
}

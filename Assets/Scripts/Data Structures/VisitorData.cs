using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Visitor", menuName = "ScriptableObjects/Visitor")]
public class VisitorData : ScriptableObject
{
	public class KeywordData
	{
		public string Text;
		public bool Validity;
		public string Shortcut; // may be null if keyword is invalid
	}

	[Serializable]
	public struct DialogText
	{
		[TextArea(5, 10)]
		public string Text;
		public FMODUnity.EventReference Voice;
	}

	[Header("General information")]
	public string Name;
	[BoxGroup("Images")] public Sprite NeutralDialogImage;
	[BoxGroup("Images")] public Sprite HappyDialogImage;
	[BoxGroup("Images")] public Sprite AngryDialogImage;

	[Header("Dialogs")]
	public List<DialogText> LoreText;
	public List<DialogText> SymptomsText;

	public DialogText WaitingText;
	public DialogText RewardText;

	[Header("FinalCheckup")]
	public Sprite CheckupBackgroundImage;
	public Color CheckupColor;

	/**
	 * Parses the symptoms of a hints panel into a map of keyword -> validity
	 */
	public List<KeywordData> ParseSymptoms(int index)
	{
		List<KeywordData> keywords = new List<KeywordData>();

		string symptoms = SymptomsText[index].Text;

		foreach (string text in symptoms.Split('|'))
		{
			string block = text;

			bool valid = block.StartsWith("#");
			bool hasShortcut = block.StartsWith("#[");
			string shortcut = null;

			if (hasShortcut)
			{
				int shortcutEnd = block.IndexOf("]");
				if (shortcutEnd != -1)
				{
					shortcut = block.Substring(2, shortcutEnd - 2);
					block = block.Remove(0, shortcutEnd + 1);
				}
				else
				{
					block = block.Remove(0, 1);
				}
			}
			else if (valid)
			{
				block = block.Remove(0, 1);
			}

			KeywordData keyword = new KeywordData { Text = block.Trim(), Validity = valid, Shortcut = shortcut };
			keywords.Add(keyword);
		}

		return keywords;
	}
}

using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Plant", menuName = "ScriptableObjects/Plant")]
public class PlantData : ScriptableObject
{
	[Header("Herbarium")]
	public string CommonName;
	public Sprite HerbariumImage;
	public GameObject HerbariumModel;

	[Header("Garden")]
	public GameObject GardenModel;

	[Header("Remedy")]
	[AssetSelector] public List<PlantEffectData> Effects;

	[Header("FMOD Params")]
	public float Type;
	public FMODUnity.EventReference HerbariumClickEvent;
}

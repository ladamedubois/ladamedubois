using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlantEffect", menuName = "ScriptableObjects/PlantEffect")]
public class PlantEffectData : ScriptableObject
{
	public string DisplayName;
	[TextArea(5, 10)] public string Description;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Remedy : IInventoryItem
{
	public static string RightColor = "<color=\"green\">";
	public static string WrongColor = "<color=\"red\">";

	public static int MaxComponentsCount = 3;

	public List<PlantData> Components { get; private set; }
	public VisitorData Client { get; private set; }

	private Sprite _image;
	private string _name;

	private MissionData _mission;

	public Remedy(MissionData mission)
	{
		Components = new List<PlantData>();
		Client = mission.Visitor;
		_image = mission.RemedyImage;
		_name = mission.RemedyName;

		_mission = mission;
	}

	public void AddComponent(PlantData component)
	{
		if (Components.Count >= MaxComponentsCount) return;
		Components.Add(component);
	}

	public Sprite GetImage()
	{
		return _image;
	}

	public string GetName()
	{
		return _name;
	}

	public string GetCompositionString()
	{
		string composition = "";

		for (int i = 0; i < Components.Count; i++)
		{
			composition += Components[i].CommonName;
			if (i < Components.Count - 1) composition += " - ";
		}

		return composition;
	}

	public string GetCompositionStringColored()
	{
		// Define invalid components
		
		List<PlantData> invalidComponents = new List<PlantData>();
		bool remedyValid = _mission.CheckRemedy(this);

		if (!remedyValid)
		{
			foreach (PlantData plant in Components)
			{
				int usefullEffects = 0;
				foreach (PlantEffectData effect in plant.Effects)
				{
					if (_mission.BadEffects.Contains(effect))
					{
						invalidComponents.Add(plant);
						break;
					}

					if (_mission.RequiredEffects.Contains(effect)) usefullEffects++;
				}

				if (usefullEffects == 0) invalidComponents.Add(plant);
			}
		}

		// Generate string

		string composition = "";

		for (int i = 0; i < Components.Count; i++)
		{
			PlantData comp = Components[i];

			composition += invalidComponents.Contains(comp) ? WrongColor : RightColor;
			composition += comp.CommonName;
			composition += "</color>";
			if (i < Components.Count - 1) composition += " - ";
		}

		return composition;
	}

	public bool CanRemove() => false;
}

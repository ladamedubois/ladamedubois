using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

[RequireComponent(typeof(CanvasGroup))]
[Serializable]
public class CreditPlayable : PlayableBehaviour, ITimeControl
{
	public event EventHandler onShow;
	public event EventHandler onHide;

	[SerializeField] private float _showTime = 0.5f;
	[SerializeField] private float _hideTime = 1f;

	private CanvasGroup _canvasGroup;

	private void Awake()
	{
		_canvasGroup = GetComponent<CanvasGroup>();

		onShow += (sender, data) => OnShow();
		onHide += (sender, data) => OnHide();
		onShow += (sender, data) => Debug.Log("Show");
	}

	private void Start()
	{
		gameObject.SetActive(false);
	}

	void ITimeControl.OnControlTimeStart()
	{
		Debug.Log("SHOFIEPH");
		onShow?.Invoke(this, EventArgs.Empty);
	}

	void ITimeControl.OnControlTimeStop()
	{
		onHide?.Invoke(this, EventArgs.Empty);
	}

	void ITimeControl.SetTime(double time) { }

	private void OnShow()
	{
		Debug.Log("Test");
		gameObject.SetActive(true);
		_canvasGroup.DOFade(1f, _showTime).From(0f);
	}

	private void OnHide()
	{
		_canvasGroup.DOFade(0f, _hideTime).OnComplete(() =>
		{
			gameObject.SetActive(false);
		});
	}
}
